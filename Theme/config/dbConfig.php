<?php

class connection{
    
    function dbConnect () {

         try {

                $dbHandler = new PDO ('mysql:host=localhost;dbname=kcmi-website-db', 'root', '');
                $dbHandler -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $time_zone='+01:00';
                $now = new DateTime();
                $mins = $now->getOffset() / 60;
                $sgn = ($mins < 0 ? -1 : 1);
                $mins = abs($mins);
                $hrs = floor($mins / 60);
                $mins -= $hrs * 60;
                $offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);
                $dbHandler->exec("SET time_zone='$offset';");
                return $dbHandler;

        } catch (PDOException $e) {
             $page = '<div style="margin:0 auto;text-align:center;"><h1>An error occured, Please try again.</h1></div>';
             echo $page;
             die();
        }
    }
}
        $connection = new connection();
        $db = $connection -> dbConnect();
?>