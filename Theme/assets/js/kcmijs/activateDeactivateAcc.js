$(document).ready(function () {
    
    $(document).on('click', "#activateAcc", function () {
        
        var adminId = $(this).val();
        // i'd hide this button
        $("#action" + adminId).empty();
        $("#status" + adminId).empty();
        
        
        $("#action" + adminId).html("<div class=\"loader\" style=\"width:20px;height:20px;\"></div>");
        $("#status" + adminId).html("<div class=\"loader\" style=\"width:20px;height:20px;\"></div>");
        
        var send = {'mainId' : adminId};
            
        $.ajax ({
                type: "POST",
                url: "../scripts/activateAccount.php",
                data: send,
                cache: false,
                success: function(html)
            {
                var result = html;
                
                if (result == true) {
                    $("#action" + adminId).empty();
                    $("#status" + adminId).empty();

                    $("#action" + adminId).html("<button class=\"btn btn-danger btn-sm\" value='"+adminId+"' id=\"deactivateAcc\"><i class=\"fa fa-trash-o \"></i> Suspend or Deactivate account</button>");
                
                    $("#status" + adminId).html('<span class="btn btn-default" style="padding:7px;font-size:12px;">ACTIVATED</span>');
                    
                } else {
                    
                    $("#action" + adminId).empty();
                    $("#status" + adminId).empty();

                    $("#action" + adminId).html("<button class=\"btn btn-success btn-sm\" value='"+adminId+"' id=\"activateAcc\"><i class=\"fa fa-ok \"></i>Activate account</button>");
                
                    $("#status" + adminId).html('<span class="btn btn-default" style="padding:7px;font-size:12px;">UNACTIVATED</span>');
                    
                }
                
            }

        });
    });
    
    $(document).on('click', "#deactivateAcc", function () {
        
        var adminId = $(this).val();
        // i'd hide this button
        $("#action" + adminId).empty();
        $("#status" + adminId).empty();
        
        
        $("#action" + adminId).html("<div class=\"loader\" style=\"width:20px;height:20px;\"></div>");
        $("#status" + adminId).html("<div class=\"loader\" style=\"width:20px;height:20px;\"></div>");
        
        var send = {'mainId' : adminId};
            
        $.ajax ({
                type: "POST",
                url: "../scripts/deactivateAccount.php",
                data: send,
                cache: false,
                success: function(html)
            {
                var result = html;
                
                if (result == false) {
                    $("#action" + adminId).empty();
                    $("#status" + adminId).empty();

                    $("#action" + adminId).html("<button class=\"btn btn-danger btn-sm\" value='"+adminId+"' id=\"deactivateAcc\"><i class=\"fa fa-trash-o \"></i> Suspend or Deactivate account</button>");
                
                    $("#status" + adminId).html('<span class="btn btn-default" style="padding:7px;font-size:12px;">ACTIVATED</span>');
                    
                } else {
                    
                    $("#action" + adminId).empty();
                    $("#status" + adminId).empty();

                    $("#action" + adminId).html("<button class=\"btn btn-success btn-sm\" value='"+adminId+"' id=\"activateAcc\"><i class=\"fa fa-ok \"></i>Activate account</button>");
                
                    $("#status" + adminId).html('<span class="btn btn-default" style="padding:7px;font-size:12px;">UNACTIVATED</span>');
                    
                }
                
            }

        });
    });
    
})