$(document).ready (function () {
   
    $(document).on('click', "#assignTask", function () {
       
        $("#ajaxModalTitle").empty();
        $('#ajaxModal').modal('show');
        
        $("#assignTaskVisuals").html("<div align=\"center\" class=\"loader\" style=\"width:50px;height:50px;margin:0 auto;margin-top:30px;margin-bottom:30px;\"></div>");
        
        var admin = $(this).val();
        
        var send = {'adminId' : admin};
        
        $.ajax
           ({
               type: "POST",
               url: "../scripts/showTask.php",
               data: send,
               cache: true,
               success: function(html)
            { 
                $("#assignTaskVisuals").empty();
                $("#assignTaskVisuals").html(html);
            }

           });
        
    });
    
    

    
    
});