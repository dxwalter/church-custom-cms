$(document).ready (function () {
    
    // this is for loading more dfr messages
    $(document).on('click', '#loadDfrBtn', function () {
       var lastDfrId = $(this).val();
        $("#dfr" + lastDfrId).empty();
        $("#dfr" + lastDfrId).html("<div class=\"loader\" style=\"width:40px;height:40px;\"></div>");
        
        var send = {'lastId' : lastDfrId};
            
        $.ajax ({
                type: "POST",
                url: "../scripts/listOfDfr.php",
                data: send,
                cache: false,
                success: function(html)
            {
                var result = html;
                $("#dfr" + lastDfrId).empty();
                $("#dfr" + lastDfrId).html(result);
                
            }
            
        });
    });
    
    
    // this is for loading more dfr messages
    $(document).on('click', '#loadMoreMessage', function () {
       var lastMessageId = $(this).val();
        $("#moreMessage" + lastMessageId).empty();
        $("#moreMessage" + lastMessageId).html("<div class=\"loader\" style=\"width:40px;height:40px;\"></div>");
        
        var send = {'lastId' : lastMessageId};
            
        $.ajax ({
                type: "POST",
                url: "../scripts/messageList.php",
                data: send,
                cache: false,
                success: function(html)
            {
                var result = html;
                $("#moreMessage" + lastMessageId).empty();
                $("#moreMessage" + lastMessageId).html(result);
                
            }
            
        });
    });
    
    
    // this is for loading more dfr messages
    $(document).on('click', '#loadMoreEvents', function () {
       var lastEventId = $(this).val();
        $("#moreEvents" + lastEventId).empty();
        $("#moreEvents" + lastEventId).html("<div class=\"loader\" style=\"width:40px;height:40px;\"></div>");
        
        var send = {'lastId' : lastEventId};
            
        $.ajax ({
                type: "POST",
                url: "../scripts/callRecentEventsClass.php",
                data: send,
                cache: false,
                success: function(html)
            {
                var result = html;
                $("#moreEvents" + lastEventId).empty();
                $("#moreEvents" + lastEventId).html(result);
                
            }
            
        });
    });
    
    
       // this is for loading more testimonies
    $(document).on('click', '#loadTestimony', function () {
       var lastId = $(this).val();
        $("#moretest" + lastId).empty();
        $("#moretest" + lastId).html("<div class=\"loader\" style=\"width:40px;height:40px;\"></div>");
        
        var send = {'lastId' : lastId};
            
        $.ajax ({
                type: "POST",
                url: "../scripts/allPublishedTestimonies.php",
                data: send,
                cache: false,
                success: function(html)
            {
                var result = html;
                $("#moretest" + lastId).empty();
                $("#moretest" + lastId).html(result);
                
            }
            
        });
    });
    
    
    // this is for loading more unpublished dfr messages
    $(document).on('click', '#unPublishedloadDfrBtn', function () {
       var lastDfrId = $(this).val();
        $("#dfr" + lastDfrId).empty();
        $("#dfr" + lastDfrId).html("<div class=\"loader\" style=\"width:40px;height:40px;\"></div>");
        
        var send = {'lastId' : lastDfrId};
            
        $.ajax ({
                type: "POST",
                url: "../scripts/publishDfr.php",
                data: send,
                cache: false,
                success: function(html)
            {
                var result = html;
                $("#dfr" + lastDfrId).empty();
                $("#dfr" + lastDfrId).html(result);
                
            }
            
        });
    });
    
    
    // this is for Publishing DFR for a day
    $(document).on('click', '#publishDfr', function () {
       var dfrId = $(this).val();
        $(this).fadeOut(1000);
        $("#dfrDiv" + dfrId).empty();
        $("#dfrDiv" + dfrId).html("<div class=\"loader\" style=\"width:20px;height:20px;\"></div>");
        
        var send = {'dfrId' : dfrId};
            
        $.ajax ({
                type: "POST",
                url: "../scripts/publishDfrForToday.php",
                data: send,
                cache: false,
                success: function(html)
            {
                var result = html;
                $("#dfrDiv" + dfrId).empty();
                $("#dfrDiv" + dfrId).html(result);
                
            }
            
        });
    });
    
    // This is to confirm an event
    $(document).on('click', '#confirmEvent', function () {
        var actionType = $(this).attr('data-action');
        var eventId = $(this).attr('data-event');
        $(this).fadeOut(1000);
        
        $("#contentDump" + eventId).html("<div align=\"center\" class=\"loader\" style=\"width:30px;height:30px;margin:0 auto;margin-top:10px;margin-bottom:10px;\"></div>");
        
        var send = {'eventId' : eventId, 'actionType':actionType};
        
        $.ajax
           ({
               type: "POST",
               url: "../scripts/eventManager.php",
               data: send,
               cache: true,
               success: function(html)
            { 
                $("#contentDump" + eventId).empty();
                $("#contentDump" + eventId).html(html);
            }

           });
        
    });
    
    
    // This is to set an event as the next event
    $(document).on('click', '#nextEvent', function () {
        var actionType = "nextEvent";
        var eventId = $(this).val();
        var actionValue = $(this).attr('data-event');
        $(this).fadeOut(1000);
        
        $("#contentDump" + eventId).html("<div align=\"center\" class=\"loader\" style=\"width:30px;height:30px;margin:0 auto;margin-top:10px;margin-bottom:10px;\"></div>");
        
        var send = {'eventId' : eventId, 'actionType':actionType, 'actionValue':actionValue};
        
        $.ajax
           ({
               type: "POST",
               url: "../scripts/eventManager.php",
               data: send,
               cache: true,
               success: function(html)
            { 
                $("#contentDump" + eventId).empty();
                $("#contentDump" + eventId).html(html);
            }

           });
        
    });
    
});