$(document).ready(function () {
   
    function disableButton (name) {
        $(name).attr("disabled", "disabled");
    }
    
    $(document).on("click", "#mainButton", function () {
      
        var name = "#mainButton";
        disableButton(name);
        
    });
    
});