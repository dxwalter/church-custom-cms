<?php

    require_once ('config/carrier.php');
    require_once ('scripts/mainQueryFile.php');
    require_once ('scripts/mainFunctionFile.php');
    require_once ('scripts/signUp.php');

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="robots" content="noindex, nofollow">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>REHOBOTH -- CREATE AN ADMIN ACCOUNT</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
            
            <?php 
            
                if (isset($_POST["signUp"])) {
                    
                    echo '<div class="form-login" style="margin-top:20px;margin-bottom:-70px;">';
                        
                        if (isset($createAcc -> err[1])) {
                            echo '<div class="alert alert-danger"><b>Oh snap!</b> '.$createAcc -> err["1"].'.</div>';
                        } else if (isset($createAcc -> err["2"])) {
                            echo '<div class="alert alert-success"><b>Well done!</b> '.$createAcc -> err["2"].'.</div>';
                        }
                    
                    echo '</div>';
                }
            
            ?>
            
            
	  	        
            
		      <form class="form-login" action="signUp.php" method="post">
                  
		        <h2 class="form-login-heading">CREATE AN ACCOUNT</h2>
		        <div class="login-wrap">
		            <input type="text" class="form-control" placeholder="First name" name="fname" value="<?php if (isset($fname)) {echo $fname;} ?>">
		            <br>
                    <input type="text" class="form-control" placeholder="Last name" name="lname" value="<?php if (isset($lname)) {echo $lname;} ?>">
		            <br>
                    <input type="text" class="form-control" placeholder="Email Address" name="email"  value="<?php if (isset($email)) {echo $email;} ?>">
		            <br>
                    <input type="password" class="form-control" placeholder="Password" name="pword">
		            <br>
		            <input type="password" class="form-control" placeholder="Confirm password" name="conPword">
                    <br>
                    
		            <button class="btn btn-theme btn-block" type="submit" name="signUp"><i class="fa fa-lock"></i> CREATE ACCOUNT</button>
                    
		            <hr>
		            
		            
		            <div class="registration">
		                I have an account <br/>
		                <a class="" href="index.php">
		                    Sign in to your account
		                </a>
		            </div>
		
		        </div>
                  
                </form>
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/minorJs.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/signUpBackground.jpg", {speed: 500});
    </script>


  </body>
</html>
