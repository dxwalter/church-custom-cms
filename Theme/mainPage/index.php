<?php require_once ('../inc/header.php') ?>
      <section id="main-content">
          <section class="wrapper">
              <!-- This is where everything will fall in -->
              <?php
                        
                if (isset($_GET["pageLet"])) {
                    
                    $page = $_GET["pageLet"];
                    
                    if (!empty($page)) {
                        
                        switch ($page) {
            
                            case "dashboard":
                            require_once ('../contentPage/dashboard.php');
                            break;
                            
                            case "wordMonth":
                            require_once ('../contentPage/wordForTheMonth.php');
                            break;

                            case "livestream":
                            require_once ('../contentPage/livestream.php');
                            break;
                            
                            case "event":
                            require_once ('../contentPage/upcomingEvent.php');
                            break;
                            
                            case "editEvent":
                            require_once ('../contentPage/event/editEvent.php');
                            break;
                            
                            case "testimony":
                            require_once ('../contentPage/testimony.php');
                            break;
							
							case "contactList":
							require_once ('../contentPage/mailMessage/messageContactListings.php');
							break;
                            
                            case "dfr":
                            require_once ('../contentPage/dailyFaithRecharge.php');
                            break;
                                
                            case "convo":
                            require_once ('../contentPage/mailMessage/convo.php');
                            break;
                            
                            case "msg":
                            require_once ('../contentPage/message.php');
                            break;
                            
                            case "recentActivies":
                            require_once ('../contentPage/recentActivies.php');
                            break;

                            case "gallery":
                            require_once ('../contentPage/gallery.php');
                            break;
                            
                            case "cellCenter":
                            require_once ('../contentPage/cellCenter.php');
                            break;
                            
                            case "adminQl":
                            require_once ('../contentPage/adminList.php');
                            break;
                            
                            case "fromPulpit":
                            require_once ('../contentPage/fromPulpit/preachedMessage.php');
                            break;

                            
                            default: require_once ('../contentPage/error.php');


                    }
                        
                    } else {
                        // include dashboard
                        require_once ('../contentPage/error.php');
                    }
                    
                } else {
                    // include dashboard
                    require_once ('../contentPage/dashboard.php');
                }
    
    
    
                ?>
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
<?php require_once ('../inc/footer.php') ?>