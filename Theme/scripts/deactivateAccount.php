<?php 

require_once ('../config/carrier.php');
require_once ('../scripts/mainFunctionFile.php');

class deactivateAccount extends mainFunctionFile {
    
    function __construct ($adminId, $userId, $db) {
     
        $checkStatus = $this -> adminBio ($adminId, $db)["status"];
        
        if ($checkStatus == true) {
            
            $query = $db -> prepare("UPDATE admins SET status = ? WHERE mainid = ? LIMIT 1");
            $query -> execute(array(0, $adminId));
            
            if ($query) {
                // send this admin a message ans set activity log
                
                $mainAdminNames = $this -> adminBio($userId, $db)["firstname"]." ".$this -> adminBio($userId, $db)["lastname"];
                
                $activatedAdmin = $this -> adminBio($adminId, $db)["firstname"]." ".$this -> adminBio($adminId, $db)["lastname"];
                    
                $message = $mainAdminNames." deactivated $activatedAdmin's account.";
                
                $setActivityLog = $this -> setActivityLog ($userId, $message, $db);
                
                if ($setActivityLog) {
                    echo "1";
                } else {
                    echo "";
                }
                    
            }
            
        }
        
    }
    
}


if (isset($_POST["mainId"])) {
    $adminId = $_POST["mainId"];
    $activateAccount = new deactivateAccount($adminId, $userId, $db);
}

?>