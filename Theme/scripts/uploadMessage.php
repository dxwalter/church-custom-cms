<?php

require_once ('../scripts/emailUiClass.php');


class newMessage extends emailUiClass {
    
    function sendSermonMail($title, $preacher, $id, $db) {
        
        $query = $db -> prepare("SELECT email FROM subscription WHERE sermon = ?");
        $query -> execute(array(1));
        
        if ($query -> rowCount()) {
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                $emailAdress = $row -> email;
                
                $month = getdate()["month"];
                $messageUi = '
                    <div class="kcmi-last-message">
                        <p class="kcmi-new-message" style="text-align:center;">
                        <h2 align="center">A new sermon has been uploaded</h2>
                        <br>
                            <h4 style="color:#999" align="center">Title: '.$title.'</h3>
                            <h4 align="center" style="color:#999">Speaker: '.$preacher.'</h3>

                        </p>
                        <div class="kcmi-reply-div" align="center">
                            <a href="http://www.kcmi-rcc.com/sermonSingle.php?ijn='.$id.'" class="kcmi-button">Click to read message</a>
                        </div>
                    </div>';
                
                $uiMessageCreation = $this -> buildEmailUi ($messageUi);
                $subject = "$title by $preacher";
                $from = 'Kingdom Covenant Ministries Int\'l ';
                $sendMessage = $this -> sendEmailMessage($subject, $uiMessageCreation, $emailAdress, $from);
                
                echo $uiMessageCreation;
                
            }
        }
    }
    
    function insertMessage($title, $datePreached, $preacher, $bibleVerse, $bibleText, $word, $db, $userId) {

        $query = $db -> prepare("INSERT INTO sermon (id, title, bibleverse, bibletext, message, preacher, datepreached, uploader, dateadded, timeadded) VALUES (?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())");
        
        $query -> execute(array("", $title, $bibleVerse, $bibleText, $word, $preacher, $datePreached, $userId));

        if ($query) {
            $messageId = $db -> lastInsertId();
            $this -> sendSermonMail($title, $preacher, $messageId, $db);
            
            $siteUrl = "http://www.kcmi-rcc.com/pages/sermonSingle.php?ijn=$messageId";
            $type = "message";
            $this -> sitemapFunction ($siteUrl, $type);
            
            echo '<div class="alert alert-success">"'.$title.'" has successfully been uploaded</div>';
            
            
            unset($title);
            unset($bibleVerse);
            unset($word);
            unset($datePreached);
            unset($bibleText);
            unset($preacher);
        } else {
            echo '<div class="alert alert-danger">An error occured</div>';
        }

    }       
        
        
    function __construct ($title, $datePreached, $preacher, $bibleVerse, $bibleText, $word, $db, $userId) {
        
        if (empty($title) || ctype_space($title)) {
            echo '<div class="alert alert-info">The title for this message is required</div>';
        } else {

            if (empty($bibleVerse) || ctype_space($bibleVerse)) {
                echo '<div class="alert alert-info">The bible verse for this message is required</div>';
            } else {

              if (empty($bibleText) || ctype_space($bibleText)) {
                  echo '<div class="alert alert-info">The bible text for this message is required</div>';
              } else {

                if (empty($word) || ctype_space($word)) {
                    echo '<div class="alert alert-info">The word for this message is required</div>';
                } else {

                   if ((empty($datePreached) || ctype_space($datePreached)) || (empty($preacher) || ctype_space($preacher))) {
                        echo '<div class="alert alert-info">The date this message was preached and the name of the preacher is required.</div>';

                    } else {

                        $this -> insertMessage($title, $datePreached, $preacher, $bibleVerse, $bibleText, $word, $db, $userId);
                    }
                }
            }
          }
        }
        
        
        
        
    }
    
    
}


if (isset($_POST["newMessage"])) {
    
    $title = $_POST["title"];
    $datePreached = $_POST["datePreached"];
    $preacher = $_POST["author"];
    $bibleVerse = $_POST["bibleVerse"];
    $bibleText = $_POST["bibleText"];
    $word = $_POST["word"];
    $create = new newMessage ($title, $datePreached, $preacher, $bibleVerse, $bibleText, $word, $db, $userId);
    
}

?>