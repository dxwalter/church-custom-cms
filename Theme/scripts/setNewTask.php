<?php


class newTask extends mainFunctionFile{
    
    function __construct ($adminId, $wordforthemonth, $gallery, $testimony, $upcomingProgram, $dfr, $db) {
        
        if ($adminId) {

            $query = $db -> prepare("UPDATE task SET wordforthemonth = ?, upcomingprogram = ?, gallery = ?, dfr = ?, testimonies = ? WHERE adminid = ? LIMIT 1");
            $query -> execute(array($wordforthemonth, $upcomingProgram, $gallery, $dfr, $testimony, $adminId));

            if ($query) {
                
                $name = strtoupper($this -> adminBio($adminId, $db)["firstname"]." ".$this -> adminBio($adminId, $db)["lastname"]);
                
                echo '
                    <div class="alert alert-success"><b>Well done! </b>'.$name.' tasks has been updated successfully.</div>
                ';

            } else {
                echo '<div class="alert alert-danger"><b>Oh snap! </b>An error just occured try again. If program persist contact the technical team.</div>';
            }
        } else {
            echo '<div class="alert alert-warning"><b>Oops! </b>An error just occured try again. If program persist contact the technical team.</div>';
        }
    }
    
}

if (isset($_POST["submitTask"])) {
    
    $adminId = $_POST["submitTask"];
    
    if (isset($_POST["wordForMonth"])) {
        $wordforthemonth = 1;
    } else $wordforthemonth = 0;
    
    if (isset($_POST["gallery"])) {
        $gallery = 1;
    } else $gallery = 0;
    
    if (isset($_POST["testimony"])) {
        $testimony = 1;
    } else $testimony = 0;
    
    if (isset($_POST["nextProgram"])) {
        $upcomingProgram = 1;
    } else $upcomingProgram = 0;
    
    if (isset($_POST["dfr"])) {
        $dfr = 1;
    } else $dfr = 0;
    
    $createTask = new newTask($adminId, $wordforthemonth, $gallery, $testimony, $upcomingProgram, $dfr, $db);
}

?>