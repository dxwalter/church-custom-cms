<?php

require_once ('../scripts/emailUiClass.php');

class updateWord extends emailUiClass{
    
    
    function __construct ($word, $bibleVerse, $bibleText, $writeup, $userId, $db) {
        
       if (empty($word) || empty($bibleVerse) || empty($writeup) || empty($bibleText)) {
           echo '<div class="alert alert-info">All fields are required.</div>';
       } else {
           
           $query = $db -> prepare("INSERT INTO wordforthemonth (id, updater, word, bibleverse, bibletext, writeup, time, date)
           VALUES(?, ?, ?, ?, ?, ?, NOW(), now())");
           $query -> execute(array("", $userId, $word, $bibleVerse, $bibleText, $writeup));
           
           
           if ($query) {
               
               $name = $this -> adminBio($userId, $db)["firstname"]." ".$this -> adminBio($userId, $db)["lastname"];
               $date = date("F j, Y, g:i a");
               
               $message = ''.$name.' updated the word for this month which is "<b>'.$word.'</b>" on this day "<b>'.$date.'</b>". The bible verse choosen was "<b>'.$bibleVerse.'</b>".';
               
               $setActivityLog = $this -> setActivityLog ($userId, $message, $db);
               
               if ($setActivityLog) {
                   echo '<div class="alert alert-success">'.$word.' has successfully been updated as the word for this month.</div>';
               } else {
                   echo '<div class="alert alert-warning"><b>Oops! </b>An error just occured try again. If program persist contact the technical team.</div>';
               }
               
           }
           
       }
        
    }
    
}

if (isset($_POST["wordMonth"])) {
    
    $word = $_POST["word"];
    $bibleVerse = $_POST["bibleVerse"];
    $bibleText = $_POST["bibletext"];
    $writeup = $_POST["writeup"];
    
    $updateWord = new updateWord ($word, $bibleVerse, $bibleText, $writeup, $userId, $db);

}

?>