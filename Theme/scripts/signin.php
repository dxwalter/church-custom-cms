<?php

class checkLogin extends mainFunctionFile{
    
    public $err = array();
    
    function __construct ($email, $password, $db) {
        
        if (empty($email) || empty($password)) {
            
            $this -> err["1"] = "Your email and password are required";
            
        } else {
            
            $query = $db -> prepare("SELECT valkey FROM admins WHERE email = ? LIMIT 1");
            $query -> execute(array($email));
            
            $salt = $query -> fetch()[0];
            
            $mainPassword = $this -> passwordHash ($password, $salt);
            
            $query = $db -> prepare("SELECT mainid, level, status FROM admins WHERE email = ? AND password = ? LIMIT 1");
            $query -> execute(array($email, $mainPassword));
            
            $result = $query -> fetch();
            $mainid = $result[0];
            $level = $result[1];
            $status = $result[2];
            
            
            if ($mainid) {
                /*
                    status here means the eligibility of an admin
                    0 means the account has not been activated or has been suspended
                    1 means the account is active
                    
                */
                
                if ($status == false) {
                    $this -> err["1"] = "You account has either been suspended or has not been activated. Contact the authorities";
                    
                } else {
                    
                    /*
                        Level here means the position an admin holds in a website and it's ranked 1 to 3
                        We're setting a cookie to track each of these levels 
                    */
                    
                    $setlevelCookie = $this -> setlevelCookiesToEat ($level);  
                    $_SESSION["lv"] = $level;
                    
                    if ($setlevelCookie) {
                        // this part we set session
                        $_SESSION["id"] = $mainid;
                        // note that mainid is the userId
                        // set another cookie with the userid
                        
                        $setCooks = $this -> setMainIdCookieToEat ($mainid);
                        
                        if ($setCooks) {
                            $mainUrl = "mainPage/index.php";
                            header('location: '.$mainUrl.'');
                        }
                    }
                }
                
            } else {
                $this -> err["1"] = "Incorrect sign in credentials";
            }
            
        }
        
    }
    
}


if (isset($_POST["login"])) {
    
    $email = $_POST["email"];
    $password = $_POST["password"];
    $checkLogin = new checkLogin ($email, $password, $db);
    
}


?>