<?php


class uploadImage extends mainFunctionFile{
    
    public $formName;
    public $eventId;
    public $db;
    
    function uploadImageDb ($formName, $id, $imgDir, $db) {
        $query = $db -> prepare("INSERT INTO eventgallery(id, eventid, imagepath, dateadded, timeadded) VALUES (?, ?, ?, NOW(), NOW())");
        $query -> execute(array("", $id, $imgDir));
        
        if ($query) {
            echo '<div class="alert alert-success">'.$formName.' uploaded successfully</div>';
        } else {
            echo '<div class="alert alert-danger">An error occured uploading '.$formName.'</div>';
        }
    }
    
    function imageType($img_ext) {
        
        if ($img_ext == "jpeg" || $img_ext == "jpg" || $img_ext == "png") {
            return true;
        } else return false;
    }
    
    function checkImageSize($image_size) {
        
        if ($image_size <= 300000) {
            return true;
        } else return false;
    }
    
    function __construct ($formName, $img_array, $id, $db) {
        $this -> formName = $formName;
        $this -> eventId = $id;
        $this -> db = $db;
        
        
        // this is for image
        // check for the image format, size and width;
        ini_set('memory_limit', '128M');
            
        $img_name = $img_array[0];
        $img_size = $img_array[1];
        $img_type = $img_array[2];
        $tmp_name = $img_array[3];
        $img_ext =  $img_array[4];
        
        $imageType = $this -> imageType($img_ext);
        
        if ($imageType) {
                
                $eventDetails = $this -> eventDetails ($id, $db);
                
                $title = $eventDetails["eventtitle"].rand(1, 20000);
                
                $newname = str_replace(" ", "_", $title);
                
                $photoDir = "../../Photouploads/gallery/";
                
                $img_name = $newname.'.'.$img_ext;
                
                $imgDir = $photoDir.$img_name;
                
                if (is_dir($photoDir) == true) { 
                    
                }else{
                    mkdir($photoDir, 0777, true); // this will create directory
                }
                
                
                if (file_exists($imgDir) == false) {
                    // do nothing 
                } else {
                    $rand = rand(200, 1000);
                    $newname = $rand.$newname;
                    $imgDir = $photoDir.$newname.'.'.$img_ext;
                }
                
                
                if (move_uploaded_file($tmp_name, $imgDir) == true){
                    unset($tmp_name);
                    unset($image_size);
                    
                    $this -> uploadImageDb ($formName, $id, $imgDir, $db);
                } else {
                    echo '<div class="alert alert-danger">Unable to upload '.$formName.'</div>';
                }
            
        } else {
            echo '<div class="alert alert-danger">'.$formName.' is not a jpeg, jpg, or png image</div>';
        }
        
    }
}


if (isset($_POST["uploadPicture"])) {
    
    $id = $_GET["eventId"];
    
    if (isset($_FILES["img1"])) {
        $img_name = $_FILES["img1"]['name'];
        $img_size = $_FILES["img1"]['size'];
        $img_type = $_FILES["img1"]['type'];      
        $tmp_name = $_FILES["img1"]['tmp_name']; 
        $img_ext = strtolower(pathinfo($img_name, PATHINFO_EXTENSION));
        $img_array = array($img_name, $img_size, $img_type, $tmp_name, $img_ext);
        if ($img_name == true && $img_type == true) {
            $uploadImage = new uploadImage ("first image", $img_array, $id, $db);
        } else {
            echo '<div class="alert alert-info">Select first photo to upload</div>';
        }
    } else {
        echo '<div class="alert alert-info">Select first photo to upload</div>';
    }
    
    if (isset($_FILES["img2"])) {
        $img_name = $_FILES["img2"]['name'];
        $img_size = $_FILES["img2"]['size'];
        $img_type = $_FILES["img2"]['type'];      
        $tmp_name = $_FILES["img2"]['tmp_name']; 
        $img_ext = strtolower(pathinfo($img_name, PATHINFO_EXTENSION));
        $img_array = array($img_name, $img_size, $img_type, $tmp_name, $img_ext);
        if ($img_name && $img_type) {
            $uploadImage = new uploadImage ("second image", $img_array, $id, $db);
        } else {
            echo '<div class="alert alert-info">Select second photo to upload</div>';
        }
    } else {
        echo '<div class="alert alert-info">Select second photo to upload</div>';
    }
    
    if (isset($_FILES["img3"])) {
        $img_name = $_FILES["img3"]['name'];
        $img_size = $_FILES["img3"]['size'];
        $img_type = $_FILES["img3"]['type'];      
        $tmp_name = $_FILES["img3"]['tmp_name']; 
        $img_ext = strtolower(pathinfo($img_name, PATHINFO_EXTENSION));
        $img_array = array($img_name, $img_size, $img_type, $tmp_name, $img_ext);
        if ($img_name && $img_type) {
            $uploadImage = new uploadImage ("third image", $img_array, $id, $db);
        } else {
            echo '<div class="alert alert-info">Select third image to upload</div>';
        }
    } else {
        echo '<div class="alert alert-info">Select third photo to upload</div>';
    }
    
}

?>