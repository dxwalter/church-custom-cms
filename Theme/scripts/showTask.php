<?php 
require_once ('../config/carrier.php');
require_once ('../scripts/mainFunctionFile.php');


class callTask extends mainFunctionFile{
    
    function __construct ($userId, $admin, $db) {
        
        $name = strtoupper($this -> adminBio($admin, $db)["firstname"]." ".$this -> adminBio($admin, $db)["lastname"]);
        
        $status = $this -> adminBio($admin, $db)["status"];
        
        if ($status) {
            
            $query = $db -> prepare("SELECT * FROM task WHERE adminid = ? LIMIT 1");
            $query -> execute(array($admin));
            
            if ($query -> rowCount() > 0) {
                
                while ($row = $query -> fetch(PDO::FETCH_ASSOC)) {
                    
                    $wordFortheMonth = $row["wordforthemonth"];
                    $upcomingprogram = $row["upcomingprogram"];
                    $gallery = $row["gallery"];
                    $dfr = $row["dfr"];
                    $testimonies = $row["testimonies"];
                    $cellcenters = $row["cellcenters"];
                    
                }
                
                if ($wordFortheMonth) {
                    $wordFortheMonth = '<span class="label label-success">Assigned</span>';
                    $wordFortheMonthCheck = "checked";
                } else {
                    $wordFortheMonth = '<span class="label label-default">Not assigned</span>';
                    $wordFortheMonthCheck = "";
                }
                
                if ($upcomingprogram) {
                    $upcomingprogram = '<span class="label label-success">Assigned</span>';
                    $upcomingprogramCheck = "checked";
                } else {
                    $upcomingprogram = '<span class="label label-default">Not assigned</span>';
                    $upcomingprogramCheck = "";
                }
                
                if ($gallery) {
                    $gallery = '<span class="label label-success">Assigned</span>';
                    $galleryCheck = "checked";
                } else {
                    $gallery = '<span class="label label-default">Not assigned</span>';
                    $galleryCheck = "";
                }
                
                
                if ($dfr) {
                    $dfr = '<span class="label label-success">Assigned</span>';
                    $dfrCheck = "checked";
                } else {
                    $dfr = '<span class="label label-default">Not assigned</span>';
                    $dfrCheck = "";
                }
                
                if ($testimonies) {
                    $testimonies = '<span class="label label-success">Assigned</span>';
                    $testimoniesCheck = "checked";
                } else {
                    $testimonies = '<span class="label label-default">Not assigned</span>';
                    $testimoniesCheck = "";
                }
                
                if ($cellcenters) {
                    $cellcenters = '<span class="label label-success">Assigned</span>';
                    $cellcentersCheck = "checked";
                } else {
                    $cellcenters = '<span class="label label-default">Not assigned</span>';
                    $cellcentersCheck = "";
                }
                
                ////////////////////////////////////////ASSIGN NEW TASK ////////////////////////////////////////////
                
                
                 echo '<div class="row mt">
                        <form action="index.php?pageLet=adminQl" method="post">
                        <div class="col-md-12">
                        <div class="white-panel pn" style="height:360px;">
                            <div class="panel-heading">
                                <div class="pull-left"><h5><i class="fa fa-tasks"></i> <b style="color:#000">CREATE NEW TASK FOR '.$name.'\'S ACCOUNT</b></h5></div>
                                <br>
                            </div>
                            <div class="custom-check goleft mt">
                                 <table id="todo" class="table table-hover custom-check">
                                  <tbody>
                                    <tr>
                                        <td>
                                        
                                            <div class="task-checkbox" style="color:#000;">
                                                &nbsp;&nbsp;
                                                <input class="list-child" value="1" type="checkbox" id="r1" name="wordForMonth" '.$wordFortheMonthCheck.'>
                                                &nbsp;&nbsp;
                                                <label for="r1" style="cursor: pointer;"><span class="task-title-sp"><b>WORD FOR THE MONTH</b></span></label>&nbsp;&nbsp&nbsp;&nbsp;'.$wordFortheMonth.'
                                            </div>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        
                                            <div class="task-checkbox" style="color:#000;">
                                                &nbsp;&nbsp;
                                                <input class="list-child" value="1" type="checkbox" id="r2" name="nextProgram" '.$upcomingprogramCheck.'>
                                                &nbsp;&nbsp;
                                                <label for="r2" style="cursor: pointer;"><span class="task-title-sp"><b>UPDATING UPCOMING PROGRAMS</b></span></label>&nbsp;&nbsp&nbsp;&nbsp;'.$upcomingprogram.'
                                            </div>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                            <div class="task-checkbox" style="color:#000;">
                                                &nbsp;&nbsp;
                                                <input class="list-child" value="1" type="checkbox" id="r3" name="dfr" '.$dfrCheck.'>
                                                &nbsp;&nbsp;
                                                <label for="r3" style="cursor: pointer;"><span class="task-title-sp"><b>DAILY FAITH RECHARGE</b></span></label>&nbsp;&nbsp&nbsp;&nbsp;'.$dfr.'
                                            </div>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        
                                        <!--<td>
                                              
                                            <div class="task-checkbox" style="color:#000;">
                                                &nbsp;&nbsp;
                                                <input class="list-child" value="1" type="checkbox" id="r4" name="gallery" '.$galleryCheck.'>
                                                &nbsp;&nbsp;
                                                <label for="r4" style="cursor: pointer;"><span class="task-title-sp"><b>GALLERY & PICTURE UPLOAD</b></span></label>&nbsp;&nbsp&nbsp;&nbsp;'.$gallery.'
                                            </div>
                                                
                                        </td>
                                     </tr>-->
                                     
                                    <tr>
                                        <td>
                                               
                                            <div class="task-checkbox" style="color:#000;">
                                                &nbsp;&nbsp;
                                                <input class="list-child" value="1" type="checkbox" id="r5" name="testimony" '.$testimoniesCheck.'>
                                                &nbsp;&nbsp;
                                                <label for="r5" style="cursor: pointer;"><span class="task-title-sp"><b>PUBLISHING TESTIMONIES</b></span></label>&nbsp;&nbsp&nbsp;&nbsp;'.$testimonies.'
                                            </div>
                                              
                                        </td>
                                     </tr>
                                  </tbody>
                              </table>
                            </div><!-- /table-responsive -->
                            <div class="add-task-row" style="padding:10px;">
                                  <button value="'.$admin.'" name="submitTask" type="submit" class="btn btn-success btn-sm btn-block" href="todo_list.html#"><b>UPDATE TASK</b></button>
                            </div>
                        </div><!--/ White-panel -->
          		    </div>
                    </form>
          		</div>
                    
                ';
                ////////////////////////////////////////END ASSIGN NEW TASK ////////////////////////////////////////////
                
                
            } else {
                $query = $db -> prepare("INSERT INTO task (adminid) VALUES(?)");
                $query -> execute(array($admin));
                
                $callTask = new callTask ($userId, $admin, $db);
            }
            
        } else {
            echo "<h4>You have to activate <b>$name's</b> account before you can assign a task to his account.</h4>";
        }
        
        
    }
    
}


if (isset($_POST["adminId"])) {
    $admin = $_POST["adminId"];
    
    $callTask = new callTask ($userId, $admin, $db);
}


?>