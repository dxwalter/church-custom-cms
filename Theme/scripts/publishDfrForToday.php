<?php

require_once ('../config/carrier.php');
require_once ('../scripts/mainFunctionFile.php');

class publishDfrForToday extends mainFunctionFile {
    
    function sendDfrMail() {
    
    }
    
    function setToFalse ($db) {
        
        $query = $db -> prepare("UPDATE dfr SET status = ? WHERE status = ?");
        $query -> execute(array(0, 1));
        
    }
    
    function __construct ($dfrId, $db) {
        
        // first old status to false
        $this -> setToFalse($db);
        
        $today = getdate();
        $month = $today["month"];// month
        $day = $today["mday"];// day
        $year = $today["year"];// year
        $dayOfWeek = $today["weekday"];
        
        if ($day == "1" || $day == "21" || $day == "31") {
            $day = $day."st";
        } else if ($day == "2" || $day == "22") {
            $day = $day."nd";
        } else if ($day == "3" || $day == "23") {
            $day = $day."rd";
        } else {
            $day = $day."th";
        }
        
        $dateFormat = $dayOfWeek." ".$day." ".$month.", ".$year;
        
        // SET new status;
        $query = $db -> prepare("UPDATE DFR SET status = ?, publish = ?, publishdate = ?, publishtime = NOW() WHERE id = ? LIMIT 1");
        $query -> execute(array(1, 1, $dateFormat, $dfrId));
        
        if ($query) {
            // SEND MAIL TO SUBSCRIBERS
            $dfrDetails = $this -> dfrDetails($dfrId, $db);
            $link = 'http://www.kcmi-rcc.com/singleDfr.php?ijn='.$dfrId.'';
            $messageTitle = $dfrDetails["title"];
            $today = date("F j, Y, g:i a");  
            $this -> sendDfrMail();
            
            $siteUrl = "http://www.kcmi-rcc.com/pages/singleDfr.php?ijn=$dfrId";
            $type = "dfr";
            $this -> sitemapFunction ($siteUrl, $type);
            
            echo '<br><div class="alert alert-success">'.$messageTitle.' has successfully been published for today '.$today.'</div>';
        } else {
            echo '<div class="btn btn-danger">An error occured publishing today\'s DFR</div>';
        }
        
    }
    
    
}


if (isset($_POST["dfrId"])) {
    $dfrId = $_POST["dfrId"];
    $set = new publishDfrForToday($dfrId, $db);
}


?>