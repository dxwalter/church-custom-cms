<?php

require_once ('generalFunction.php');

    class recoverPassword extends general {
        
        protected $db;
        public $err = array();
        
        function sendMailing ($email, $Tempass1) {
            
        $link = "http://www.schoolmade.net/referrer/mailReferrer.php?cId=$email&responseTo=passwordRecovery&sentFrom=desktop";

        $message = "
        <div align=\"center\" style=\"line-height:22px;\">
        <h2>Hello, $email </h2>This is an automated message from SchoolMade.net.<br>You indicated that you forgot your sign in password. <br>If you didn't request for this kindly ignore this message. <br> We have created a temporary password that will allow you sign in once,<br>when signed in you can change your password to a new one.<br>Before clicking the link below this will be the new temporary password that you'll use to sign in.
        <p align=\"center\"><h3>$Tempass1</h3><p>
        Please click on change password below to sign in with your temporary password.
        <p  style=\"padding:20px;\"><a align=\"center\" href=\"$link\" target=\"_blank\" style=\"width:70px;height:30px;border:1px solid #cccccc; background-color:#fff;padding: 10px 10px;color:#333333;text-decoration:none;text-align: center;cursor: pointer;vertical-align: middle;font-size:14px;\"><b>Change password</b></a></p>
        
        <p style=\"color:#5a7fab;\">If you don't click on the link above no change will be made to your account and you won't be able to sign in.</p>
        </div>
        ";
            
        
            
        $from = "From: no-reply@schoolmade.net\n";
        $message_reciever = $email;
        $message_subject = "SchoolMade Password Recovery";
            
        if ($this -> Mailing ($message_subject, $message_reciever, $message, $from)) {
            return true;
        }
            
        }
        
        function recValidate($email, $db){
            $this -> db = $db;
            
            if (!empty($email) && $email != " " && (strpos($email, '@') == true)) {
                $query = $this -> db -> prepare("SELECT id FROM admins WHERE email = ? LIMIT 1");
                $query -> bindParam(1, $email);
                $query -> execute();
                $id = $query -> fetch()["0"];
                
                $Tempass1 = rand(4000, 700000);
                $Tempass2 = sha1($Tempass1);
                
                if ($id > 0) {
                
                    $query1 = $this -> db -> prepare("UPDATE admins SET tempPass = ? WHERE id = ? AND email = ? LIMIT 1");
                    $query1 -> bindParam(1, $Tempass2);
                    $query1 -> bindParam(2, $id);
                    $query1 -> bindParam(3, $email);
                    $query1 -> execute();
                
                if ($query1) {
                    
                    $mail = $this -> sendMailing($email, $Tempass1);
                    
                    if ($mail) {
                        return $this -> err["2"] = "A recovery password and a link has been sent to <b>$email</b>";
                    } else{
                        header('location: ../ErrorPage.php?errNum=5');
                    }
                    
                } 
                }else {
                    return $this -> err["1"] = "We can't recognize <b>$email</b>. Please try again.";
                }
                
                
            } else {
                return $this -> err["1"] = "Your email address is required";
            }
        }
    }

if (isset($_POST["recover"])) {
    $email = $_POST["email"];
    $recDetails = new recoverPassword();
    $recDetails -> recValidate($email, $db);
}

?>