<?php

class createUser extends mainFunctionFile {
    
    public $err = array();
    public $password;
    public $fname;
    public $lname;
    public $email;
    public $salt;
    protected $db;
    
    function insertDbNewAccount($fname, $lname, $email, $password, $salt, $db) {
        
        $query = $db -> prepare("INSERT INTO admins (id, mainid, firstname, lastname, email, password, valkey, level, status, date, time) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())");
        
        $query -> execute(array("", "", $fname, $lname, $email, $password, $salt, 0, 0));
        
        if ($query) {
            
            $lastId = $this -> db -> lastInsertId();
            $mainId = sha1($lastId);
            
            if ($mainId) {
                
                $query = $this -> db -> prepare("UPDATE admins SET mainid = ? WHERE id = ? LIMIT 1");
                $query -> execute(array($mainId, $lastId));
                
                $query = $db -> prepare("INSERT INTO lastseen (adminid, date, time) VALUES (?, NOW(), NOW())");
                $query -> execute(array($mainId));
                
                $query = $db -> prepare("INSERT INTO task (adminid) VALUES(?)");
                $query -> execute(array($mainId));
                
                if (!$query) {
                    return false;
                } else return true;
                
            } else {
                return false;
            }
            
        } else{
            return false;
        }
        
    }
    
    
    function regMail() {
        $fname = $this -> fname;
        $lname = $this -> lname;
        
        $message = "$fname $lname just registered to the church website. His or Her account needs to be confirmed. Log on to the control panel to confirm the account.";
            
    }
    
    function checkName ($name, $type) {
            
        if (ctype_space($name) || empty($name)) {
            $this -> err["1"] = "Your $type name is required";
            return false;
            
        } else if (strlen($name) < 3 ) {
            
            $this -> err["1"] = "Your $type can't be less than 3 characters";
            return false;
            
        } else {
            
            if ($type == "first name") {
                $this -> fname = ucfirst(strtolower($name));
            } else {
                $this -> lname = ucfirst(strtolower($name));
            }
            
            return true;
            
        }
        
    }
    
    function checkEmail ($email, $db) {
        
        if (!empty($email) || ctype_space($email)) {
            
            if (strpos($email, '@') == true) {
                // check if email already exists
                $checkmail = $this -> checkMailExistence ($email, $db);
                
                if ($checkmail) {
                    $this -> err["1"] = "This email <b>".$email."</b> address already exists";
                    return false;
                    
                } else {
                    $this -> email = strtolower($email);
                    return true;
                }
            }
            
        } else {
            $this -> err["1"] = "Your email address is required";
            return false;
        }
    }
    
    function checkPassword($pword, $conPword) {
        
        if (strlen($pword) < 6 || ctype_space($pword)) {
            $this -> err["1"] = "Your password must be greater than 5 characters";
            return false;
        } else {
            
            if ($pword !== $conPword) {
                $this -> err["1"] = "Your passwords are not the same";
                return false;
            } else {
                $salt = rand(1000, 2900000);
                $this -> salt = $salt;
                $this -> password = $this -> passwordHash ($pword, $salt);
                return true;
            }
            
        }
        
    }
    
    function startUpload ()
    {
        // insert into database, send mail to the church administrator to confirm account;
        
        $fname = $this -> fname;
        $lname = $this -> lname;
        $email = $this -> email;
        $salt = $this -> salt;
        $password = $this -> password;
        
        
        $inserIntoDb = $this -> insertDbNewAccount($fname, $lname, $email, $password, $salt, $this -> db);         
                
        if ($inserIntoDb) {
            // send mail to to level three members
            $this -> regMail();
            $this -> err["2"] = "$fname your account has successfully been created but it has to be confirmed. You'll get an email once it has been confirmed.";
        } else  {
            $this -> err["1"] = "An error just occured. Try again or contact the technical department";
        }
    }
            
    function __construct ($fname, $lname, $email, $pword, $conPword, $db) {
        
        $this -> db = $db;
        
        $type = "first name";
        if ($this -> checkName ($fname, $type)) {
            
            $type = "last name";
            if ($this -> checkName ($lname, $type)) {
                
                if ($this -> checkEmail ($email, $db)) {
                    
                    if ($this -> checkPassword($pword, $conPword)) {
                     
                        if (count($this->err) == 0) {
                            $this -> startUpload();
            
                        }
                        
                        
                    }
                }
            }
            
        }
        
    }
    
}

if (isset($_POST["signUp"])) {
    $fname = $_POST["fname"];
    $lname = $_POST["lname"];
    $email = $_POST["email"];
    $pword = $_POST["pword"];
    $conPword = $_POST["conPword"];
    
    $createAcc = new createUser($fname, $lname, $email, $pword, $conPword, $db);
}

?> 