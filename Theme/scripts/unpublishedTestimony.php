<h4><i class="fa fa-angle-right"></i> Publish Testimonies</h4>
<hr>
<div class="col-md-8">




<?php
    
 if (isset($_POST["publishTestimony"])) {
     require_once ('../scripts/publishTestimony.php');
     
 } else if (isset($_POST["deleteTestimony"])) {
     require_once ('../scripts/deleteTestimony.php');
 }   
    
?>
    
<?php

$query = $db -> prepare("SELECT * FROM testimony WHERE status = ? ORDER BY id ASC LIMIT 1");
$query -> execute(array(0));

if ($query -> rowCount()) {
    
    while ($row = $query -> fetch(PDO::FETCH_ASSOC)) {
        $dbname = $row["name"];
        $dbtitle = $row["title"];
        $dbcontact = $row["contact"];
        $dblocation = $row["location"];
        $dbmessage = $row["message"];
        $id = $row["id"];
        
    }
    
} else {
    echo '<div class="alert alert-info">We don\'t have any unpublished testimony</div>';
    die();
}

?>
    
<?php

    if ($id == false) {
        echo '<div class="alert alert-danger">This testimony is not recognised, We can\'t track its ID</div>';
    }
    
?>
    
<form method="post" name="contactform" class="clearfix" action="index.php?pageLet=testimony&type=add">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" id="fname" name="names"  class="form-control input-lg" placeholder="First and Last names" value="<?php if (isset($dbname)) {echo $dbname;} ?>">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" id="contact" name="contact"  class="form-control input-lg" placeholder="Email Address or Phone number" value="<?php if (isset($dbcontact)) {echo $dbcontact;} ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <input type="text" id="email" name="location"  class="form-control input-lg" placeholder="What's your State and Country. Example Country, State" value="<?php if (isset($dblocation)) {echo $dblocation;} ?>">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <input type="text" id="email" name="title"  class="form-control input-lg" placeholder="Give Your Testimony a Title" value="<?php if (isset($dbtitle)) {echo $dbtitle;} ?>">
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <input type="text" id="email" name="tag"  class="form-control input-lg" placeholder="Tag this testimony e.g Grace, Fruitfulness, Employment" value="<?php if (isset($tag)) {echo $tag;} ?>">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <textarea cols="6" rows="10" id="comments" name="message" class="form-control input-lg" placeholder="Type Your Testimony">
                <?php if (isset($dbmessage)) {echo $dbmessage;} ?>
                </textarea>
            </div>
        </div>
    </div>
    
    <div class="row">
        
        <div class="col-md-6">
            <button class="btn btn-danger btn-lg btn-block" type="submit" name="deleteTestimony" value="<?php if (isset($id)) {echo $id;} ?>">Delete Testimony</button>
        </div>
        
        <div class="col-md-6">
            <button class="btn btn-success btn-lg btn-block" type="submit" name="publishTestimony" value="<?php if (isset($id)) {echo $id;} ?>">Save & Publish Testimony</button>
        </div>
    </div>
    
    
</form>
    
    
    
    
    
    
    
</div>