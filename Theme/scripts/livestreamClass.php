<?php

class updateIframe {

    function __construct ($iframe, $userId, $db) {

        if (empty($iframe)) {
            echo '<div class="alert alert-info">The iframe field is required.</div>';
        } else {
            $query = $db -> prepare("INSERT INTO livestreamiframe (id, uploader, iframe, timedate)
           VALUES(?, ?, ?, NOW())");

           $query -> execute(array("", $userId, $iframe));

           if ($query) {
            echo '<div class="alert alert-success">Livestream iframe uploaded successfully.</div>';
           }

        }
    }


}


if (isset($_POST["iframeBtn"])) {
    
    $iframe = $_POST["iframeText"];
    
    $updateIframe = new updateIframe ($iframe, $userId, $db);

}

?>