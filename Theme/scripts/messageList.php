<?php

if (isset($_POST["lastId"])) {
    require_once ('../config/carrier.php');
    require_once ('../scripts/mainFunctionFile.php');
}

class messageList extends mainFunctionFile {
    
    function __construct ($lastId, $db) {
        
        if ($lastId) {
            
            $query = $db -> prepare("SELECT * FROM sermon WHERE id < ? ORDER BY id DESC LIMIT 6");
            $query -> execute(array($lastId));
            
        } else {
            $query = $db -> prepare("SELECT * FROM sermon ORDER BY id DESC LIMIT 6");
            $query -> execute();
        }
        
        if ($query -> rowCount()) {
            
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                
                $id = $row -> id;
                $title = $row -> title;
                $bibleverse = $row -> bibleverse;
                $bibletext = $row -> bibletext;
                $message = $row -> message;
                $preacher = $row -> preacher;
                $datePreached = $row -> datepreached;
                $uploader = $row -> uploader;
                $uploaderDetails = $this -> adminBio ($uploader, $db);
                $uploaderName =  $uploaderDetails["firstname"]." ".$uploaderDetails["lastname"];
                
                echo '<div class="col-md-8">
                        <div class="showback">';
                        
                        echo '
                        <h4><i class="fa fa-angle-right"></i> '.$title.'</h4>
                        <h5 style="padding:5px;font-size:16px;"><b>Preached By</b>: '.$preacher.'</h5>
                        <h5 style="padding:5px;font-size:16px;"><b>Uploaded By</b>: '.$uploaderName.'</h5>
                        '.$this -> callNumberOfRead($id, 'pulpitmessage', $db).'
                        ';
                
                
                echo '<div class="collapse" id="collapseExample'.$id.'">
                          <div class="well">
                            <p style="">
                              <h3 style="font-size: 20px;text-align: center;">'.$title.'</h3>
                              <br>
                            </p>

                            <p>
                              <h4>Bible Verse</h4>
                              <div style="font-size:15px;">'.$bibleverse.'</div>
                              <br><br>
                            </p>

                            <p>
                              <h4>Bible Text</h4>
                              <div style="font-size:15px;">'.$bibletext.'</div>
                              <br><br>
                            </p>

                            <p>
                              <h4>The Word</h4>
                                <div style="font-size:15px;">'.$message.'</div>
                              <br><br>
                            </p>
                          </div>
                        </div>';
                
                echo '
                <button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#collapseExample'.$id.'" aria-expanded="false" aria-controls="collapseExample'.$id.'">
                  Read Message
                </button>
                ';
                
                echo '<a href="index.php?pageLet=fromPulpit&type=edit&id='.$id.'" class="btn btn-default btn-sm">Edit Message</a>';
                
                echo '</div>
                </div>';
            }
            
            if ($query -> rowCount() > 5) {
                
                echo '
                <div id="moreMessage'.$id.'">
                    <div class="col-lg-12">
                            <button class="btn btn-default btn-lg" id="loadMoreMessage" value="'.$id.'">Load More Messages</button>
                    </div>
                </div>
                ';
                
            } else {
                echo '<div class="col-lg-8"><div class="alert alert-info">These are all the messages for now</div></div>';
            }
            
            
        } else {
            echo '<div class="alert alert-info">No Message Has Been Uploaded</div>';
        }
        
    }
    
}

if (isset($_POST["lastId"])) {
    $lastId = $_POST["lastId"];   
    $callMore = new  messageList($lastId, $db);
}

?>