<?php

class editDFR {

    function insertDfr($dfrId, $title, $bibleVerse, $word, $blessing, $bibletext, $db) {

        $query = $db -> prepare("UPDATE dfr SET title = ?, terminal = ?, bibletext = ?, word = ?, blessing = ? WHERE id = ? LIMIT 1");
        $query -> execute(array($title, $bibleVerse, $bibletext, $word, $blessing, $dfrId));

        if ($query) {
            echo '<div class="alert alert-success">"'.$title.'" has successfully been edited successfully</div>';
            unset($title);
            unset($bibleVerse);
            unset($word);
            unset($blessing);
        } else {
            echo '<div class="alert alert-danger">An error occured</div>';
        }

    }

    function __construct ($dfrId, $title, $blessing, $word, $bibleVerse, $bibleText, $db) {

        if (empty($title) || ctype_space($title)) {
            echo '<div class="alert alert-info">The title for this DFR is required</div>';
        } else {

            if (empty($bibleVerse) || ctype_space($bibleVerse)) {
                echo '<div class="alert alert-info">The bible verse for this DFR is required</div>';
            } else {

              if (empty($bibleText) || ctype_space($bibleText)) {
                  echo '<div class="alert alert-info">The bible text for this DFR is required</div>';
              } else {

                if (empty($word) || ctype_space($word)) {
                    echo '<div class="alert alert-info">The word for this DFR is required</div>';
                } else {

                   if (empty($blessing) || ctype_space($blessing)) {
                        echo '<div class="alert alert-info">The blessing for this DFR is required</div>';

                    } else {

                        $this -> insertDfr($dfrId, $title, $bibleVerse, $word, $blessing, $bibleText, $db);
                    }
                }
            }
          }
        }

    }

}


    if (isset($_POST["editDfr"]) && isset($_GET["id"])) {
        $word = $_POST["word"];
        $bibleVerse = $_POST["bibleVerse"];
        $title = $_POST["title"];
        $bibleText = $_POST["bibleText"];
        $blessing = $_POST["blessing"];
        $dfrId = $_GET["id"];
        
        $createDfr = new editDFR ($dfrId, $title, $blessing, $word, $bibleVerse, $bibleText, $db);
    }
?>
