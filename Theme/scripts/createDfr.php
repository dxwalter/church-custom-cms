<?php

class createDfr {

    function insertDfr($title, $bibleVerse, $word, $blessing, $bibletext, $userId, $db) {

        $query = $db -> prepare("INSERT INTO dfr (id, writer, title, terminal, bibletext, word, blessing, status, publish, publishdate, publishtime, dateadded, timeadded) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())");
        $query -> execute(array("", $userId, $title, $bibleVerse, $bibletext, $word, $blessing, 0, 0, 0, 0));

        if ($query) {
            echo '<div class="alert alert-success">"'.$title.'" has successfully been created</div>';
            unset($title);
            unset($bibleVerse);
            unset($word);
            unset($blessing);
        } else {
            echo '<div class="alert alert-danger">An error occured</div>';
        }

    }

    function __construct ($title, $blessing, $word, $bibleVerse, $bibleText, $userId, $db) {

        if (empty($title) || ctype_space($title)) {
            echo '<div class="alert alert-info">The title for this DFR is required</div>';
        } else {

            if (empty($bibleVerse) || ctype_space($bibleVerse)) {
                echo '<div class="alert alert-info">The bible verse for this DFR is required</div>';
            } else {

              if (empty($bibleText) || ctype_space($bibleText)) {
                  echo '<div class="alert alert-info">The bible text for this DFR is required</div>';
              } else {

                if (empty($word) || ctype_space($word)) {
                    echo '<div class="alert alert-info">The word for this DFR is required</div>';
                } else {

                   if (empty($blessing) || ctype_space($blessing)) {
                        echo '<div class="alert alert-info">The blessing for this DFR is required</div>';

                    } else {

                        $this -> insertDfr($title, $bibleVerse, $word, $blessing, $bibleText, $userId, $db);
                    }
                }
            }
          }
        }

    }

}


    if (isset($_POST["newDfr"])) {
        $word = $_POST["word"];
        $bibleVerse = $_POST["bibleVerse"];
        $title = $_POST["title"];
        $bibleText = $_POST["bibleText"];
        $blessing = $_POST["blessing"];

        $createDfr = new createDfr ($title, $blessing, $word, $bibleVerse, $bibleText, $userId, $db);
    }
?>
