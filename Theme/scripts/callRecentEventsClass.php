<?php


if (isset($_POST["lastId"])) {
    require_once ('../config/carrier.php');
    require_once ('../scripts/mainFunctionFile.php');
}


class callAllEvents extends mainFunctionFile{
    
function __construct ($lastId, $userId, $db) {
    
    
    if ($lastId) {
         $query = $db -> prepare("SELECT * FROM event WHERE id < ? ORDER BY id DESC LIMIT 7");
        $query -> execute(array($lastId));
    } else {
        $query = $db -> prepare("SELECT * FROM event ORDER BY id DESC LIMIT 7");
        $query -> execute();    
    }
    
    
    if ($query -> rowCount()) {
        
        while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
            
            $id = $row -> id;
            $eventcreator = $row -> eventcreator;
            $eventtitle = $row -> eventtitle;
            $eventheme = $row -> eventheme;
            $bibleverse = $row -> bibleverse;
            $day = $row -> day;
            $month = $row -> month;
            $year = $row -> year;
            $image = $row -> image;
            $writeup = $row -> writeup;
            $status = $row -> status;
            $level = $this -> userLevel($userId, $db);
            $date = $row -> dateadded;
            $time = $row -> timeadded;
            $nextProgram = $row -> nextevent;
            $collapseId = "collapse$id";
            $creatorData = $this -> adminBio($eventcreator, $db);
            $name = $creatorData["firstname"]." ".$creatorData["lastname"];
            
            echo '
                <div class="showback">
                    <h4><i class="fa fa-angle-right"></i> '.$eventtitle.'</h4>
                    <h5 style="padding:5px;font-size:16px;"><b>THEME OF EVENT</b>: '.$eventheme.'</h5>
                    <h5 style="padding:5px;font-size:16px;"><b>DATE OF EVENT</b>: '.$day.' '.$month.' '.$year.'</h5>
                    <h5 style="padding:5px;font-size:16px;"><b>EVENT CREATOR</b>: '.$name.'</h5>
                    <h5 style="padding:5px;font-size:16px;"><b>DATE CREATED</b>: '.$date.' at '.$time.'</h5>
                    '.$this -> callNumberOfRead($id, 'event', $db).'
      		    
                                
                <a class="btn btn-primary btn-sm" role="button" data-toggle="collapse" href="#'.$collapseId.'" aria-expanded="false" aria-controls="'.$collapseId.'">
                  Read write up 
                </a>';
            
                echo '
                <a href="index.php?pageLet=editEvent&eventId='.$id.'" class="btn btn-default btn-sm">Edit Event</a>
                ';
            
                if ($level == 'h') {
                    
                    if ($status == false) {
                        echo '
                        <button id="confirmEvent" class="btn btn-success btn-sm" value="1" data-event="'.$id.'" data-action="confirm">Confirm Event</button>
                        ';
                    } else {
                        echo '
                        <button id="confirmEvent" class="btn btn-default btn-sm" value="1" data-event="'.$id.'" data-action="unconfirm">Unconfirm Event</button>
                        ';
                    }
                    
                    if (!$nextProgram) {
                        echo '<button id="nextEvent" data-event="1" value="'.$id.'"  class="btn btn-warning btn-sm">Set as next event</button>';
                    }
                    
                    
                    echo '
                        <button id="confirmEvent" class="btn btn-danger btn-sm" value="0" data-event="'.$id.'" data-action="delete">Delete Event</button>
                    ';
                    
                    if ($status) {
                        echo ' <a href="index.php?pageLet=event&state=addOldEventPhoto&eventId='.$id.'" class="btn btn-success btn-sm">Add Pictures</a>';
                    }
                }
                
                echo '
                <div class="collapse" id="'.$collapseId.'">
                <br>
                <div class="well">
                    '.$writeup.'
                </div>
                </div>
                
                
                <p><div id="contentDump'.$id.'"></div></p>
                
                </div>
                
            ';
        }
        
        if ($query -> rowCount() > 6) {
            
            echo '
                <div id="moreEvents'.$id.'">
                    <div class="col-lg-12">
                            <button class="btn btn-default btn-lg " id="loadMoreEvents" value="'.$id.'">Load More Events</button>
                    </div>
                </div>
                ';
            
        } else {
            echo "<div class=\"alert alert-info\">These are all the events that\'ve been uploaded</div>";   
        }
        
    } else {
        echo "<div class=\"alert alert-info\">No event has been created</div>";
    }
    
}
    
}

if (isset($_POST["lastId"])) {
    $lastId = $_POST["lastId"];
    $callEvents = new callAllEvents ($lastId, $userId, $db);
}

?>