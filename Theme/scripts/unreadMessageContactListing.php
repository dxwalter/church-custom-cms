<?php

class unreadMailMessages extends mainFunctionFile{
    
    function otherDetails($conversationId, $db) {
        $query = $db -> prepare("SELECT timeadded FROM conversationmessage WHERE sender = ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array($conversationId));
        $timeAdded = $query -> fetch()[0];
        
        $mainTime = date('l dS \o\f F Y @ h:i:s A', $timeAdded);
        
        return '<td class="numeric">'.$mainTime.'</td>';
    }
    
    function callLastMessageId($conversationId, $db) {
        $query = $db -> prepare("SELECT id FROM conversationmessage WHERE sender = ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array($conversationId));
        
        return $query -> fetch()[0];
    }
    
    function __construct ($db) {
        $query = $db -> prepare("SELECT DISTINCT(sender) FROM conversationmessage WHERE seen = ? AND sender != ?");
        $query -> execute(array(0, 'admin'));
        
        if ($query -> rowCount()) {

            echo '
                <div class="row mt">
                <div class="col-lg-12">
                      <div class="content-panel">
                      <h4><i class="fa fa-angle-right"></i> New Messages</h4>
                          <section id="unseen">
                            <table class="table table-bordered table-striped table-condensed">
                              <thead>
                              <tr>
                                  
                                  <th>Name OF Sender</th>
                                  <th class="numeric">Email Or Phone number of sender</th>
                                  <th class="numeric">Date and Time Sent</th>
                              </tr>
                              </thead>
                              <tbody>
            ';
            
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                
                $conversationId = $row -> sender;
                $id = $this -> callLastMessageId($conversationId, $db);
                $senderDetails = $this -> contactDetail($conversationId, $db);
                $contact = $senderDetails["contact"];
                $name = $senderDetails["name"];
                
                if ($conversationId != 'admin') {
                    
                
                    echo '    
                            <tr>
                              <td><a href="index.php?pageLet=convo&convoId='.$conversationId.'#cala'.$id.'">'.$name.'</a></td>
                              <td class="numeric"><a href="index.php?pageLet=convo&convoId='.$conversationId.'#cala'.$id.'">'.$contact.'</a></td>

                              '.$this -> otherDetails($conversationId, $db).'
                            </tr>
                    ';
                }
            }
            
            echo '
                            </tbody>
                          </table>
                          </section>
                  </div><!-- /content-panel -->
               </div>
               </div>
               <p style="margin-bottom:50px;"></p>
            ';
            
        } else {
            echo '<div class="alert alert-info">You don\'t have any unread message</div>';
        }
    }
}
$unreadMessage = new unreadMailMessages($db);
?>