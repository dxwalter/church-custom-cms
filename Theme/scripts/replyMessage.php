<?php

require_once ('../scripts/emailUiClass.php');

class replyMessage extends emailUiClass {

    public $id, $subject, $message;
    
    function receiverLastMessage($id, $db) {
        $query = $db -> prepare("SELECT message FROM conversationmessage WHERE sender = ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array($id));
        $result = $query -> fetch()[0];
        return $result;
    }
    
    function sendMesssageBack($db) {
        $id = $this -> id;
        $message = $this -> message;
        $subject = $this -> subject;
        
        $contactDetails = $this -> contactDetails($id, $db);
        $name = $contactDetails["name"];
        $receiver = $contactDetails["contact"];
        
        $messageUi = '<div class="kcmi-last-message">
               
                <p class="kcmi-new-message">
                    '.$message.' 
                </p>
            </div>
            <div class="kcmi-reply-div" align="center">
                <a href="http://www.kcmi-rcc.com/contact.php?email='.$receiver.'&amp;name='.$name.'#contactDiv" class="kcmi-button">Click here to reply</a>
            </div>';
        
        $receiverLastMessage = $this -> receiverLastMessage($id, $db);
        $uiCreation = $this -> buildEmailUi ($messageUi);
        $from = 'Kingdom Covenant Ministries Int\'l ';
        
        $sendMessage = $this -> sendEmailMessage($subject, $uiCreation, $receiver, $from);
        
        if ($sendMessage) {
            $currentTime = strtotime(date('H:i:s'));
        
            $query = $db -> prepare("INSERT INTO conversationmessage(id, sender, receiver, message, seen, dateadded, timeadded) VALUES(?, ?, ?, ?, ?, NOW(), ?)");
            $query -> execute(array("", 'admin', $id, $message, 1, $currentTime));
            if ($query) {
                echo '<div class="alert alert-success">Message sent successfully</div>';
                echo $uiCreation;
            } else {
                echo '<div class="alert alert-info">An error occured</div>';
            }
        } else {
            echo '<div class="alert alert-info">An error occured</div>';
        }
    }
    
    function __construct ($id, $subject, $message, $db) {
        
        if ($id) {
            $this -> id = $id;
            if ($subject && !ctype_space($subject)) {
                $this -> subject = $subject;
                
                if ($message && !ctype_space($message)) {
                    $this -> message = $message;
                    
                    $this -> sendMesssageBack($db);
                } else {
                    echo '<div class="alert alert-info">Type your message</div>'; 
                }
                
            } else {
                echo '<div class="alert alert-info">Type the subject of your message</div>';   
            }
            
        } else {
            echo '<div class="alert alert-info">An error occured</div>';
        }
    }
    
}

if (isset($_POST["replyMessage"])) {
    $subject = $_POST["subject"];
    $message = $_POST["message"];
    $id = $_GET["convoId"];
    $replyMessage = new replyMessage ($id, $subject, $message, $db);
}

?>