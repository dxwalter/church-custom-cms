<?php

if (isset($_POST["deleteTestimony"])) {
    $deleteId = $_POST["deleteTestimony"];
    
    $query = $db -> prepare("DELETE FROM testimony WHERE id = ?");
    $query -> execute(array($deleteId));
    
    if ($query) {
        echo "<div class=\"alert alert-success\">Testimony successfully deleted</div>";
    } else {
        echo '<div class="alert alert-danger">An error occured</div>';
    }
}

?>