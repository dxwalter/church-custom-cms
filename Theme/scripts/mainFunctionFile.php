<?php

class mainFunctionFile {
    
    
    function sitemapFunction ($siteUrl, $type) {
            
            $t = "\t";
            $n = "\n";
            $currentDateTime = date('c', time());
            
            
            $xml = "";
            $xml.= "$t".'<url>'."$n";
            $xml.= "$t".'<loc>'."$siteUrl".'</loc>'."$n";
            $xml.= "$t"."<lastmod>"."$currentDateTime"."</lastmod>$n";
            $xml.= "$t"."<changefreq>weekly</changefreq>$n";
            $xml.= "$t"."<priority>0.5</priority>$n";
            $xml.= "$t".'</url>'."$n";
            $xml.= '</urlset>';

           
           // openfile
            $endLine = '</urlset>';
            if ($type == "dfr") {
                $mapLocation = '../../main/dfr.xml';
            } else if ($type == "message") {
                $mapLocation = '../../main/sermon.xml';
            } else if ($type == "event") {
                $mapLocation = '../../main/event.xml';
            } else if ($type = "testimony") {
                $mapLocation = '../../main/testimony.xml';
            }
            
            file_put_contents($mapLocation, str_replace($endLine, '', file_get_contents($mapLocation)));
            $handle = fopen($mapLocation, 'a');
            fwrite($handle, $xml);            
        }
    
    function sendEmailMessage ($subject, $message, $receiver, $from) {
        
        if (strtoupper(substr(PHP_OS,0,3)=='WIN')) { 
           $eol="\r\n"; 
         } elseif (strtoupper(substr(PHP_OS,0,3)=='MAC')) { 
           $eol="\r"; 
         } else { 
           $eol="\n"; 
         }
        
        $emailAddress = $receiver;
        $emailSubject = $subject;
        $message = $message;
        
        $now = date('H:i:s');
        
        $headers = 'From: '.$from.''.$eol; 
        $headers .= 'Reply-To: '.$emailAddress.''.$eol; 
        $headers .= 'Return-Path: '.$emailAddress.''.$eol;     // these two to set reply address 
        $headers .= "Message-ID:<".$now." TheSystem@".$_SERVER['SERVER_NAME'].">".$eol; 
        $headers .= "X-Mailer: PHP v".phpversion().$eol;           // These two to help avoid spam-filters 
//      Boundry for marking the split & Multitype Headers 
        $mime_boundary=md5(time()); 
        $headers .= 'MIME-Version: 1.0'.$eol; 
        $headers .= "Content-Type: multipart/related; boundary=\"".$mime_boundary."\"".$eol; 
        $msg = "";
        $msg .= "--".$mime_boundary.$eol; 
        $msg .= "Content-Type: text/html; charset=iso-8859-1".$eol; 
        $msg .= "Content-Transfer-Encoding: 8bit".$eol; 
        $msg .= $message.$eol.$eol; 
        
        $msg .= "--".$mime_boundary."--".$eol.$eol;   // finish with two eol's for better security. see Injection.
        
        ini_set('sendmail_from', $from);  // the INI lines are to force the From Address to be used ! 
        if (mail($emailAddress, $emailSubject, $msg, $headers)) {
            return true;
        } else {
            return false;
        }
        ini_restore('sendmail_from'); 
        
    }
    
    function contactDetails($id, $db) {
        $query = $db -> prepare("SELECT * FROM conversationbinder WHERE id = ? LIMIT 1");
        $query -> execute(array($id));
        if ($query -> rowCount()) {
            return $query -> fetch(PDO::FETCH_ASSOC);
        }
    }
    
    function callNumberOfRead($id, $type, $db) {
        $query = $db -> prepare("SELECT COUNT(id) FROM contentreadcheck WHERE contentid = ? AND contenttype = ?");
        $query -> execute(array($id, $type));
        $result = $query -> fetch()[0];
        
        if ($result) {
            return '<h5 style="padding:5px;font-size:16px;"><b>TOTAL NUMBER OF READERS</b>: '.$result.' </h5>';
        }
    }
    
    function eventDetails ($eventId, $db) {
        $query = $db -> prepare("SELECT * FROM event WHERE id = ? LIMIT 1");
        $query -> execute(array($eventId));
        
        if ($query) {
            return $query -> fetch(PDO::FETCH_ASSOC);
        }
    }
    
	function unreadMessageCount($db) {
		$query = $db -> prepare("SELECT DISTINCT(sender) FROM conversationmessage WHERE seen = ? AND sender != ?");
		$query -> execute(array(0, 'admin'));
		
		if ($query -> rowCount()) {
			return $query -> rowCount();
		}
	}
	
    function contactDetail($id, $db) {
        $query = $db -> prepare("SELECT * FROM conversationbinder WHERE id = ? LIMIT 1");
        $query -> execute(array($id));
        if ($query) {
            return $query -> fetch(PDO::FETCH_ASSOC);
        }
    }
    
    function dfrDetails ($id, $db) {
        $query = $db -> prepare("SELECT * FROM dfr WHERE id = ? LIMIT 1");
        $query -> execute(array($id));
        if ($query) {
            return $query -> fetch(PDO::FETCH_ASSOC);
        }
    }
    
	function unconfirmedTestimonies ($db) {
		
		$query = $db -> prepare("SELECT * FROM testimony WHERE status = ?");
		$query -> execute (array(0));
		
		if ($query) {
			return $query -> rowCount();
		}
	}
	
    function unconfirmedEvents ($db) {
        $query = $db -> prepare("SELECT * FROM event WHERE status = ?");
        $query -> execute(array(0));
        
        if ($query ->rowCount() > 0) {
            return $query -> rowCount();
        }
    }
    
    
    function callMyTask ($adminId, $db) {
        $query = $db -> prepare("SELECT * FROM task WHERE adminid = ? LIMIT 1");
        $query -> execute(array($adminId));
        
        if ($query -> rowCount()) {
            return $query -> fetch(PDO::FETCH_ASSOC);
        } else {
            return false;
        }
    }
    
    function  setActivityLog ($initiator, $message, $db) {
        
        $query = $db -> prepare("INSERT INTO activitylog (id, adminid, message, time, date) VALUES(?, ?, ?, NOW(), NOW())");
        $query -> execute(array("", $initiator, $message));
        
        if ($query) {
            return true;
        } else return false;
        
    }
    
    function returnTime($date, $time) {
            // we want to return time in secs, mins and hours if the time the content was created was in the last 24hrs
            // but if it's more than 24hrs then we return in days and week

            $uploadTime = strtotime("$date $time"); // the time the content was uploaded in secs
            $currentTime = strtotime("".date("F j, Y, g:i a")." now"); // current time (NOW)
            $diff = $currentTime - $uploadTime;

            if ($diff < 1) { // if time is less than 0secs then we return in secs
                return "<span style=\"color:green\">Online</span>";
            } else if ($diff > 0 && $diff <= 59) { // if time is more than 0secs but less than or equal to 59secs then we return time in minutes
                return "<span style=\"color:green\">Online</span>";
            } else if ($diff > 59 && $diff <= 3599) { // if time is more than 59mins but less than or equal to 24hrs then we return time in hours

                $min = $diff / 60;
                $min = round($min, 0);
                if ($min == 1) {
                    return "1 min ago";
                } else if ($min > 1) {
                    return "$min mins ago";
                }

            } else if ($diff > 3599 && $diff <= 86399) { // this is from 1 hr to 24 hrs

                $min = $diff / 3600;
                $min = round($min, 0);
                if ($min == 1) {
                    return "1 hr ago";
                } else if ($min > 1) {
                    return "$min hrs ago";
                }

            } else if ($diff > 86399 && $diff <= 604799) {  // 24hrs to 7 days
                $min = $diff / 86400;
                $min = round($min, 0);
                if ($min == 1) {
                    return "1 day ago";
                } else if ($min > 1) {
                    return "$min days ago";
                }   

            } else if ($diff > 604799 && $diff <= 2419199) { // 7days to 4weeks
                $min = $diff / 604800;
                $min = round($min, 0);
                if ($min == 1) {
                    return "1 week ago";
                } else if ($min > 1) {
                    return "$min weeks ago";
                }   

            } else if ($diff > 2419199 && $diff <= 29030399) { // 4weeks to 12mnths
                $min = $diff / 2419200;
                $min = round($min, 0);
                if ($min == 1) {
                    return "1 month ago";
                } else if ($min > 1) {
                    return "$min months ago";
                }
            }
        }
    
    function passwordHash ($password, $salt) {
        return crypt($password, $salt);
    }
    
    function lastSeenTime($userId, $db) {
        $query = $db -> prepare("SELECT date, time FROM lastseen WHERE adminid = ? LIMIT 1");
        $query -> execute(array($userId));
        
        if ($query -> rowCount()) {
            $result = $query -> fetch();
            $date = $result[0];
            $time = $result[1];
            
            return $this -> returnTime($date, $time);
        }
    }
    
    function updatelastSeen($userId, $db) {
        
        $query = $db -> prepare("UPDATE lastseen SET date = NOW(), time = NOW() WHERE adminid = ? LIMIT 1");
        $query -> execute(array($userId));
    }
    
    function adminBio ($userId, $db) {
        $query = $db -> prepare("SELECT * FROM admins WHERE mainid = ? LIMIT 1");
        $query -> execute(array($userId));
        
        return $query -> fetch(PDO::FETCH_ASSOC);
    }
    
    function userLevel($userId, $db) {
        
        $query = $db -> prepare("SELECT level FROM admins WHERE mainid = ? LIMIT 1");
        $query -> execute(array($userId));
        
        if ($query) {
            $level = $query -> fetch()[0];
            if ($this -> setlevelCookiesToEat ($level)) {
                return $level;
            }
        }
        
    }
    
    function setlevelCookiesToEat ($level) {
        setcookie("_LV", "$level", time()+1800, "/");// expires in 30min
        return true;
    }
    
    function setMainIdCookieToEat ($mainid) {
        setcookie("_FK", "$mainid", time()+1800, "/");// expires in 30min
        return true;
    }
    
    function checkMailExistence ($email, $db) {
        $query = $db -> prepare("SELECT id FROM admins WHERE email = ? LIMIT 1");
        $query -> execute(array($email));
        
        if ($query -> rowCount() > 0) {
            return true;
        } else return false;
    }
    
}

?>