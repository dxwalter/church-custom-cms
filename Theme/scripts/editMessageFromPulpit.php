<?php


class editMessage extends mainFunctionFile {
    
    function insertMessage($messageId, $title, $datePreached, $preacher, $bibleVerse, $bibleText, $word, $db) {
        
		$query = $db -> prepare ("UPDATE sermon SET title = ?, bibleverse = ?, bibletext = ?, message = ?, preacher = ?, datepreached = ? WHERE id = ? LIMIT 1 ");
		
        $query -> execute(array($title, $bibleVerse, $bibleText, $word, $preacher, $datePreached, $messageId));

        if ($query) {
            echo '<div class="alert alert-success">"'.$title.'" has successfully been edited and saved</div>';
            unset($title);
            unset($bibleVerse);
            unset($word);
            unset($datePreached);
            unset($bibleText);
            unset($preacher);
        } else {
            echo '<div class="alert alert-danger">An error occured</div>';
        }

    }       
        
        
    function __construct ($messageId, $title, $datePreached, $preacher, $bibleVerse, $bibleText, $word, $db) {
        
        if (empty($title) || ctype_space($title)) {
            echo '<div class="alert alert-info">The title for this message is required</div>';
        } else {

            if (empty($bibleVerse) || ctype_space($bibleVerse)) {
                echo '<div class="alert alert-info">The bible verse for this message is required</div>';
            } else {

              if (empty($bibleText) || ctype_space($bibleText)) {
                  echo '<div class="alert alert-info">The bible text for this message is required</div>';
              } else {

                if (empty($word) || ctype_space($word)) {
                    echo '<div class="alert alert-info">The word for this message is required</div>';
                } else {

                   if ((empty($datePreached) || ctype_space($datePreached)) || (empty($preacher) || ctype_space($preacher))) {
                        echo '<div class="alert alert-info">The date this message was preached and the name of the preacher is required.</div>';

                    } else {

                        $this -> insertMessage($messageId, $title, $datePreached, $preacher, $bibleVerse, $bibleText, $word, $db);
                    }
                }
            }
          }
        }
        
        
        
        
    }
    
    
}


if (isset($_POST["editMessage"])) {
    
    $title = $_POST["title"];
    $datePreached = $_POST["datePreached"];
    $preacher = $_POST["author"];
    $bibleVerse = $_POST["bibleVerse"];
    $bibleText = $_POST["bibleText"];
    $word = $_POST["word"];
	$messageId = $_GET["id"];
    $create = new editMessage ($messageId, $title, $datePreached, $preacher, $bibleVerse, $bibleText, $word, $db);
    
}

?>