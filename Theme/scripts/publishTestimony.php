<?php

class publishTestimony extends mainFunctionFile{
    
    function  updateTestimony($id, $name, $contact, $location, $title, $tag, $message, $userId, $db) {
        
        $query = $db -> prepare("UPDATE testimony SET name = ?, title = ?, contact = ?, location = ?, tag = ?, message = ?, status = ?, validator = ? WHERE id = ? LIMIT 1");
        $query -> execute(array($name, $title, $contact, $location, $tag, $message, 1, $userId, $id));
        
        if ($query) {
            
            $siteUrl = "http://www.kcmi-rcc.com/pages/mainTestimony.php?ijn=$id";
            $type = "testimony";
            $this -> sitemapFunction ($siteUrl, $type);
            
            echo '<div class="alert alert-success">'.$title.' saved and published successfully</div>';
        } else {
            echo '<div class="alert alert-danger">An error occured</div>';
        }
        
    }
    
    function __construct ($id, $name, $contact, $location, $title, $tag, $message, $userId, $db) {
        
        if ($id == false) {
            
            echo '<div class="alert alert-info">This testimony is not recognised, Please delete it</div>';
            
        } else {
            
            if (empty($name) || ctype_space($name)) {
                
                echo '<div class="alert alert-info">Confirm the name of the author of this testimony</div>';
                
            } else {
                
                if (empty($contact) || ctype_space($contact)) {
                    
                    echo '<div class="alert alert-info">Confirm the contact of the author of this testimony</div>';                
                    
                } else {
                    
                    if (empty($location) || ctype_space($location)) {
                        
                        echo '<div class="alert alert-info">Confirm the location of the author of this testimony</div>';
                        
                    } else {
                        
                        if (empty($title) || ctype_space($title)) {
                            
                            echo '<div class="alert alert-info">Confirm the title of this testimony</div>';
                            
                        } else {
                            
                            if (empty($tag) || ctype_space($tag)) {
                                
                                echo '<div class="alert alert-info">Give this testimony a tag.</div>';
                                
                            } else {
                                
                                if (empty($message) || ctype_space($message)) { 
                                        
                                    echo '<div class="alert alert-info">Confirm what\'s been typed in the big box below</div>';
                                    
                                } else {
                                    
                                    $updateTestimony = $this -> updateTestimony($id, $name, $contact, $location, $title, $tag, $message, $userId, $db);
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
    }
    
}

if (isset($_POST["publishTestimony"])) {
    $id = $_POST["publishTestimony"];
    $name = $_POST["names"];
    $contact = $_POST["contact"];
    $location = $_POST["location"];
    $title = $_POST["title"];
    $tag = $_POST["tag"];
    $message = $_POST["message"];
    
    $publishTestimony = new publishTestimony($id, $name, $contact, $location, $title, $tag, $message, $userId, $db);
}

?>