<?php

class emailUiClass extends mainFunctionFile {
    
    function buildEmailUi ($message) {
        $ui = '
<!DOCTYPE HTML>
<html class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- Mobile Specific Metas
      ================================================== -->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link href="http://localhost/rest/main/css/mailUi.css" rel="stylesheet">
</head>

<body>

    <div class="kcmi-container">
        <div class="kcmi-logo-div">
            <a href="http://www.kcmi-rcc.com">
                <img src="images/logo-home.png">
                <div class="kcmi-church-name">
                    Kingdom Covenant
                    <br>
                    <b>Ministries International</b>
                </div>
            </a>
        </div>
        
        <div class="kcmi-message-body">
            '.$message.'
            <br>
            <div class="kcmi-footer-nav" align="center">
                <ul>
                    <li><a href="#">Word for the month</a></li>
                    <li><a href="#">Daily faith recharge</a></li>
                    <li><a href="#">Messages From the pulpit</a></li>
                    <li><a href="#">Testimonies</a></li>
                    <li><a href="#">Events</a></li>
                    <li><a href="#">Newsletter</a></li>
                    <li><a href="#">Donate</a></li>
                </ul>
            </div>
            <br>
            <small align="center">© 2018 KCMI. All Rights Reserved </small>    
        </div>
        
    </div>
    
</body>
    
</html>
        ';
        
        return $ui;
    }
}

?>