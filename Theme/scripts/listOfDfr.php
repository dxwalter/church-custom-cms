<?php

if (isset($_POST["lastId"])) {
    require_once ('../config/carrier.php');
    require_once ('../scripts/mainFunctionFile.php');
}

class callAllDfr extends mainFunctionFile{

    function __construct ($lastId, $db) {

        if ($lastId) {

            $query = $db -> prepare("SELECT * FROM dfr WHERE id < ? ORDER BY id DESC limit 10");
            $query -> execute(array($lastId));
            
        } else {
            $query = $db -> prepare("SELECT * FROM dfr ORDER BY id DESC limit 10");
            $query -> execute();
        }

        if ($query) {

            if ($query -> rowCount()) {

                while ($row = $query -> fetch(PDO::FETCH_OBJ)) {

                    $id = $row -> id;
                    $writer = $row -> writer;
                    $title = $row -> title;
                    $terminal  = $row -> terminal;
                    $word = $row -> word;
                    $bibleText = $row -> bibletext;
                    $blessing = $row -> blessing;
                    $status = $row -> status;
                    $publish = $row -> publish;
                    $publishdate = $row -> publishdate;
                    $publishtime = $row -> publishtime;
                    $dateadded = $row -> dateadded;
                    $timeadded = $row -> timeadded;
                    $collapseId = "collapse$id";

                    if ($publish) {
                        $publishReader = $publishdate." at ".$publishtime;
                    } else {
                        $publishReader = "Yet to be published";
                    }

                    $adminBio = $this -> adminBio ($writer, $db);

                    $writersName = $adminBio["firstname"]." ".$adminBio["lastname"];

                    echo '<div class="showback">';
                        echo '<h4><i class="fa fa-angle-right"></i> '.$title.'</h4>';
                        echo '<h5 style="padding:5px;font-size:16px;"><b>PUBLISHED DATE</b>: '.$publishReader.'</h5>';
                        echo '<h5 style="padding:5px;font-size:16px;"><b>DFR WRITER</b>: '.$writersName.'</h5>';
                        echo '<h5 style="padding:5px;font-size:16px;"><b>DATE CREATED</b>: '.$dateadded.' at '.$timeadded.'</h5>';
                        echo $this -> callNumberOfRead($id, 'dfr', $db);



                    echo '
                        <div class="collapse" id="'.$collapseId.'">
                        <br>
                        <div class="well">

                            <p style="">
                              <h3 style="font-size: 20px;text-align: center;">'.$title.'</h3>
                              <br>
                            </p>

                            <p>
                              <h4>Charging Terminal</h4>
                              <div style="font-size:15px;">'.$terminal.'</div>
                              <br><br>
                            </p>

                            <p>
                              <h4>Bible Text</h4>
                              <div style="font-size:15px;">'.$bibleText.'</div>
                              <br><br>
                            </p>

                            <p>
                              <h4>The Word</h4>
                                <div style="font-size:15px;">'.$word.'</div>
                              <br><br>
                            </p>

                            <p>
                              <h4>The Blessing</h4>
                                <div style="font-size:15px;">'.$blessing.'</div>
                              <br><br>
                            </p>

                        </div>
                        </div>
                        ';



                    echo '<a class="btn btn-primary btn-sm" role="button" data-toggle="collapse"        href="#'.$collapseId.'" aria-expanded="false" aria-controls="'.$collapseId.'">
                    Read write up
                    </a>&nbsp;&nbsp;';
                    
                    // This is the button for edit
                    echo '<a href="index.php?pageLet=dfr&type=editDfr&id='.$id.'" class="btn btn-default btn-sm">Edit Message</a>&nbsp;&nbsp;';

                    echo '</div>';


                }
                
                if ($query -> rowCount() > 9) {
                    echo '<div id="dfr'.$id.'">';
                    echo '<button type="button" value="'.$id.'" id="loadDfrBtn" class="btn btn-default btn-lg btn-block">Load More DFR Messages</button>';
                    echo '</div>';
                } else {
                    echo '<div class="alert alert-info">That is all the dfr for now</div>';
                }
                
            } else {
                echo '<div class="alert alert-info">That is all the dfr for now</div>';
            }

        } else {
            echo '<div class="alert alert-info">No DFR has been added</div>';
        }

    }


}

if (isset($_POST["lastId"])) {
    $lastId = $_POST["lastId"];
    $callDfr = new callAllDfr($lastId, $db);
}

?>
