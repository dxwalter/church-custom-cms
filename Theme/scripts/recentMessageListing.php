<?php

class readMailMessages extends mainFunctionFile{
    
    function checkPassed($receiver, $id, $db) {
        $query = $db -> prepare("SELECT id FROM conversationmessage WHERE (receiver = ? OR sender = ?) AND id > ? ORDER BY id LIMIT 1 ");
        $query -> execute(array($receiver, $receiver, $id));
        $result = $query -> fetch()[0];
        
        if ($result) {
            return true;
        } else return false;
    }
    
    function outputCheckDisplay ($receiver, $id, $db) {
        $receiverDetails = $this -> contactDetail($receiver, $db);
        $contact = $receiverDetails["contact"];
        $name = $receiverDetails["name"];   
        
        if (!$this -> checkPassed($receiver, $id, $db)) {

            echo '    
                    <tr>
                      <td><a href="index.php?pageLet=convo&convoId='.$receiver.'#cala'.$id.'">'.$name.'</a></td>
                      <td class="numeric"><a href="index.php?pageLet=convo&convoId='.$receiver.'#cala'.$id.'">'.$contact.'</a></td>

                      '.$this -> otherDetails($receiver, $db).'
                    </tr>
            ';
        }
    }
    
    function otherDetails($conversationId, $db) {
        $query = $db -> prepare("SELECT timeadded FROM conversationmessage WHERE sender = ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array($conversationId));
        $timeAdded = $query -> fetch()[0];
        
        $mainTime = date('l dS \o\f F Y @ h:i:s A', $timeAdded);
        
        return '<td class="numeric">'.$mainTime.'</td>';
    }
    
    function callLastMessageId($conversationId, $db) {
        $query = $db -> prepare("SELECT id FROM conversationmessage WHERE sender = ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array($conversationId));
        
        return $query -> fetch()[0];
    }
    
    function __construct ($db) {
        $query = $db -> prepare("SELECT * FROM conversationmessage ORDER BY id DESC");
        $query -> execute();
        
        if ($query -> rowCount()) {

            echo '
                <div class="row mt">
                <div class="col-lg-12">
                      <div class="content-panel">
                      <h4><i class="fa fa-angle-right"></i> Previous Conversations</h4>
                          <section id="unseen">
                            <table class="table table-bordered table-striped table-condensed">
                              <thead>
                              <tr>
                                  
                                  <th>Name OF Sender</th>
                                  <th class="numeric">Email Or Phone number of sender</th>
                                  <th class="numeric">Date and Time Sent</th>
                              </tr>
                              </thead>
                              <tbody>
            ';
            
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                
                $id = $row -> id;
                $conversationId = $row -> sender; // This is also the sender
                $receiver = $row -> receiver;
                
                if ($conversationId == 'admin') {
                    // i retrieve the is of receiver
                    $this -> outputCheckDisplay ($receiver, $id, $db);
                } else {
                    $this -> outputCheckDisplay ($conversationId, $id, $db);
                }
            }
            
            echo '
                            </tbody>
                          </table>
                          </section>
                  </div><!-- /content-panel -->
               </div>
               </div>
               <p style="margin-bottom:50px;"></p>
            ';
            
        } else {
            echo '<div class="alert alert-info">No new conversation has been started</div>';
        }
    }
}
$readMessages = new readMailMessages($db);
?>