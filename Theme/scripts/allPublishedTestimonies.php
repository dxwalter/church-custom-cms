<?php

if (isset($_POST["lastId"])) {
    require_once ('../config/carrier.php');
    require_once ('../scripts/mainFunctionFile.php');
}


class allTestimonies extends mainFunctionFile {
    
    
    function UIdisplay ($id, $name, $title, $contact, $location, $tag, $message, $validatorName, $dateadded, $timeadded, $db) {
        
        echo '
            <div class="showback">
                    <h4><i class="fa fa-angle-right"></i> '.$title.'</h4>
                    <h5 style="padding:5px;font-size:16px;"><b>Name Of Testifier</b>: '.$name.'</h5>
                    <h5 style="padding:5px;font-size:16px;"><b>Contact</b>: '.$contact.'</h5>
                    <h5 style="padding:5px;font-size:16px;"><b>Location</b>: '.$location.'</h5>
                    <h5 style="padding:5px;font-size:16px;"><b>Tag</b>: '.$tag.'</h5>
                    <h5 style="padding:5px;font-size:16px;"><b>Publisher</b>: '.$validatorName.'</h5>
      		        '.$this -> callNumberOfRead($id, 'testimony', $db).'
                    
                    <div class="collapse" id="collapse'.$id.'">
                      <div class="well" style="font-size:16px;">
                        '.$message.'
                      </div>
                    </div>
                                
                 <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapse'.$id.'" aria-expanded="false" aria-controls="collapse'.$id.'">
                  Read Testimony
                </a>
                
                </div>
        ';
    }
    
    function __construct ($lastId, $db) {
        
        if ($lastId) {
            $query = $db -> prepare("SELECT * FROM testimony WHERE status = ? AND id < ? ORDER BY id DESC LIMIT 5");
            $query -> execute(array(1, $lastId));
        } else {
            $query = $db -> prepare("SELECT * FROM testimony WHERE status = ? ORDER BY id DESC LIMIT 5");
            $query -> execute(array(1));
        }
        
        
        if ($query) {
            
            if ($query -> rowCount()) {
                
                while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                    $id = $row -> id;
                    $name  = $row -> name;
                    $title = $row -> title;
                    $contact = $row -> contact;
                    $location = $row -> location;
                    $tag = $row -> tag;
                    $message = $row -> message;
                    $publisher = $this -> adminBio ($row -> validator, $db);
                    $dateadded = $row -> dateadded;
                    $timeadded = $row -> timeadded;
                    
                    $validatorName = $publisher["firstname"]." ".$publisher["lastname"];
                    
                    $this -> UIdisplay($id, $name, $title, $contact, $location, $tag, $message, $validatorName, $dateadded, $timeadded, $db);
                    
                }
                
                if ($query -> rowCount() > 4) {
                    
                    echo '
                        <div id="moretest'.$id.'">
                            <button class="btn btn-default bnt-lg" id="loadTestimony" value="'.$id.'">Load More Testimonies </button>
                        </div>
                    ';
                    
                } else {
                    echo '<div class="alert alert-info">That\'s all for now</div>';
                }
                
            } else {
                echo '<div class="alert alert-info">No published testimony was found</div>';
            }
            
        } else {
            echo '<div class="alert alert-info">An error Occured</div>';
        }
        
    }
    
}
if (isset($_POST["lastId"])) {
    $lastId = $_POST['lastId'];
    $callTestimonies = new allTestimonies($lastId, $db);
}

?>