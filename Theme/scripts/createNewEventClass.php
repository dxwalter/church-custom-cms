<?php


class createNewEvent extends mainFunctionFile{
    
    public $userId;
    public $eventTitle;
    public $eventTheme;
    public $bibleverse;
    public $dayProgram;
    public $monthprogram;
    public $yearProgram;
    public $startTime;
    public $imageName;
    public $writeup;
    public $db;
    public $err = array();
    
    
    function insertIntoDbEvent() {
        
        $eventTitle = $this -> eventTitle;
        $eventTheme = $this -> eventTheme;
        $bibleverse = $this -> bibleverse;
        $dayOfProgram = $this -> dayProgram;
        $monthOfProgram = $this -> monthprogram;
        $yearOfProgram = $this -> yearProgram;
        $startTime = $this -> startTime;
        $writeup = $this -> writeup;
        $imageName = $this -> imageName;
        $userId = $this -> userId;
        
        $query = $this -> db -> prepare("INSERT INTO 
        event(id, eventcreator, eventtitle, eventheme, bibleverse, day, month, year, eventTime, image, writeup, status, dateadded, timeadded) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW()) ");
        $query -> execute(array("", $userId, $eventTitle, $eventTheme, $bibleverse, $dayOfProgram, $monthOfProgram, $yearOfProgram, $startTime, $imageName, $writeup, 0));
        
        if ($query) {
            // send mail to everyone registered to the website
            return true;
        } else {
            return false;
        }
        
    }
    
    
    function __construct ($title, $theme, $bibleverse, $dayOfProgram, $monthOfProgram, $yearOfProgram, $startTime, $img_array, $writeup, $userId, $db) {
        
        $this -> userId = $userId;
        $this -> db = $db;
        
        if (!empty($title)) {
            $this -> eventTitle = ucfirst($title);
        } else {
            $this -> err[1] = "The event title is a required field";
        }
        
        if (!empty($theme)) {
            $this -> eventTheme = ucfirst($theme);
        } else {
            $this -> err[2] = "The theme of this event is a required field";
        }
        
        if (!empty($bibleverse)) {
            $this -> bibleverse = $bibleverse;
        } else {
            $this -> bibleverse = "";
        }
        
        if (!empty($startTime)) {
            $this -> startTime = $startTime;
        } else {
            $this -> startTime = "";
        }
        
        
        if (empty($yearOfProgram) || empty($monthOfProgram) || empty($dayOfProgram)) {
            $this -> err[3] = "The date of the event is required.";
        } else {
            $this -> dayProgram = $dayOfProgram;
            $this -> monthprogram = $monthOfProgram;
            $this -> yearProgram = $yearOfProgram;
        }
        
        
        // this is for image
        // check for the image format, size and width;
        ini_set('memory_limit', '128M');
            
        $img_name = $img_array[0];
        $img_size = $img_array[1];
        $img_type = $img_array[2];
        $tmp_name = $img_array[3];
        $img_ext =  $img_array[4];
        
        if ($img_ext == "jpeg" || $img_ext == "jpg" ||  $img_ext == "png") {
            
            $image_size = getimagesize($tmp_name); // this is to get image size
            $width = $image_size[0];// this is to get image width from  $image_size
            $height = $image_size[1];// this is to get image height from  $image_size 
            
          
                
            if (!empty($title)) {
                $newname = str_replace(" ", "_", $title)."_".$dayOfProgram."_".$monthOfProgram."_".$yearOfProgram.".".$img_ext;
                
                $photoDir = "../../Photouploads/events/";
                $img_name = $newname;
                $imgDir = $photoDir.$newname;
                
                if (is_dir($photoDir) == true) {            
                }else{
                    mkdir($photoDir, 0777, true); // this will create directory
                }
                
                if (file_exists($imgDir) == false) {
                    // do nothing 
                } else {
                    $rand = rand(200, 1000);
                    $newname = $rand.$newname;
                    $imgDir = $photoDir.$newname;
                }
                
                if (move_uploaded_file($tmp_name, $imgDir) == true){
                    unset($tmp_name);
                    unset($image_size);
                    $this -> imageName = $newname;
                } else {
                    $this -> err[6] = "Unable to upload image please try again";
                }
                
                
            }
                
        
            
        } else {
            $this -> err[4] = "The image must of a jpeg, jpg, or png file type.";
        }
        
        if (!empty($writeup) && !ctype_space($writeup)) {
            $writeup = rtrim(ucfirst($writeup));
            $this -> writeup = $writeup;
        } else {
            $this -> err[7] = "The write about field is required";
        }
        
        
        if (count($this -> err) == false) {
            
            $inserRecord = $this -> insertIntoDbEvent();
            
            if (!$inserRecord) {
                $this -> err[8] = "An error occured creating new event";
            } else {
                $callName = $this -> adminBio($userId, $db);
                $fname = $callName["firstname"];
                $lname = $callName["lastname"];
                $date = date("F j, Y, g:i a");
                $message = $fname." ".$lname." created a new event ".strtoupper($title)." on this day $date. Click here to view event <a href=\"\" target=\"_blank\"></a> to see event";
                
                $this -> setActivityLog ($userId, $message, $db);
                
                $this -> err[9] = "The event for <b>$title</b> has been created successfully";
            }
        }
        
        
        
    }
    
}

    
if (isset($_POST["newEvent"])) {
    
    $title = $_POST["title"];
    $bibleverse = $_POST["bibleVerse"];
    $theme = $_POST["theme"];
    $dayOfProgram = $_POST["day"];
    $monthOfProgram = $_POST["month"];
    $yearOfProgram = $_POST["year"];
    $startTime = $_POST["startTime"];
    $writeup = $_POST["writeup"];
    
    $img_name = $_FILES["img"]['name'];
    $img_size = $_FILES["img"]['size'];
    $img_type = $_FILES["img"]['type'];      
    $tmp_name = $_FILES["img"]['tmp_name']; 
    $img_ext = strtolower(pathinfo($img_name, PATHINFO_EXTENSION));
    $img_array = array($img_name, $img_size, $img_type, $tmp_name, $img_ext);
    
    $createEvent = new createNewEvent($title, $theme, $bibleverse, $dayOfProgram, $monthOfProgram, $yearOfProgram, $startTime, $img_array, $writeup, $userId, $db);
}

?>