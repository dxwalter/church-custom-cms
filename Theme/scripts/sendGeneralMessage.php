<?php

class sendMessage extends emailUiClass {
    
    function __construct ($subject, $senderName, $email, $writeup) {
        
        if (empty($subject) || ctype_space($subject) || empty($senderName) || ctype_space($senderName) || empty($email) || ctype_space($email) ||  empty($writeup) || ctype_space($writeup)) {
            echo '<div class="alert alert-info">All message fields are required</div>';
        } else {
            $message = $writeup.'
            <p class="kcmi-new-message">
                This message was sent by '.ucfirst(strtolower($senderName)).'
            </p>';
            
            $mainUi = $this ->  buildEmailUi ("", "", $message);
            $from = "admin@kcmi-rcc.com";
            
            $checkSent = $this -> sendEmailMessage ($subject, $mainUi, $email, $from);
            
            if ($checkSent) {
                echo '<div class="alert alert-success">Your mail has been successfully sent</div>';
            } else {
                echo '<div class="alert alert-danger">An error occured</div>';
            }
            
        }
        
    }
    
}

if (isset($_POST["generalMessage"])) {
    $subject = $_POST["subject"];
    $senderName = $_POST["name"];
    $email = $_POST["email"];
    $writeup = $_POST["writeup"];
    
    $sendMessage = new sendMessage ($subject, $senderName, $email, $writeup);
}

?>