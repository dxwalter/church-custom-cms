<?php

class adminAccountSetup extends mainFunctionFile{
    
    
function assignTaskDiv($mainid, $db) {
    return "<div class=\"\">".$mainid."</div>";
}
    
  
function tabletitle () {
    return '
    <table class="table table-striped table-advance table-hover">
    <thead>
    <tr>
        <th><i class=""></i>Admin Names</th>
        <th class="hidden-phone"><i class="fa fa-clock"></i> Last Seen online</th>
        <th><i class=" fa fa-edit"></i> Status</th>
        <th>Action</th>
        
        <th><i class=" fa fa-edit"></i> Assign task</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    ';
}
    
function tableFooter () {
    echo '</tr>';
    echo '</tbody>';
    echo '</table>';
}
    
function __construct ($userId, $db) {
    
    $query = $db -> prepare("SELECT * FROM admins");
    $query -> execute();
    
    if ($query -> rowCount()) {
        
        echo $this -> tabletitle(); // the top of the table comes in it cointains the table head,
        
        while ($row = $query -> fetch(PDO::FETCH_OBJ) ) {
            
            $firstname = $row -> firstname;
            $lastname = $row -> lastname;
            $level = $row -> level;
            $status = $row -> status;
            $mainid = $row -> mainid;
            
            if ($status == false) {
                $lastseen = "NOT DEFINED";
                $position = "UNACTIVATED";
                $statusMsg =  '<td id="status'.$mainid.'"><span class="btn btn-default" style="padding:7px;font-size:12px;">'.$position.'</span></td>';
            } else {
                $lastseen = $this -> lastSeenTime($mainid, $db);
                $position = "ACTIVATED";
                $statusMsg =  '<td id="status'.$mainid.'"><span class="btn btn-default" style="padding:7px;font-size:12px;">'.$position.'</span></td>';
            }
            
            echo '<thead>';
            
            if ($mainid != $userId) {
                echo '<td><div style="font-size:16px;">'.$firstname.' '.$lastname.'</div></td>';
                echo '<td class="hidden-phone">'.$lastseen.'</td>';
                
                echo $statusMsg;
                
                if ($status == true) {
                    // This will carry the suspend or deactivate account button
                    
                   echo '<td id="action'.$mainid.'">
                          <button class="btn btn-danger btn-sm" value="'.$mainid.'" id="deactivateAcc"><i class="fa fa-trash-o "></i> Suspend or Deactivate account</button>
                        </td>';
                    
                } else {
                    // this will carry activate button
                    echo '<td id="action'.$mainid.'">
                          <button class="btn btn-success btn-sm" value="'.$mainid.'" id="activateAcc"><i class="fa fa-ok "></i>Activate account</button>
                        </td>';
                }
                
                echo '<td>
                        <span class=""><button  class="btn btn-default btn-sm" value="'.$mainid.'" id="assignTask">Assign task</button> </span>
                    </td>';
                
            }
            
            echo '</thead>';
        }
        
        echo $this -> tableFooter(); // the table footer comes in
        
    } else {
        echo '<div class="alert alert-info"><b>Oh snap!</b> No admin has been registered</div>';
    }
    
}
    
}

?>