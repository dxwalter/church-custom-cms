<?php

class callMailMessage extends mainFunctionFile {
    
    function setAllToRead($id, $db) {
        $query = $db -> prepare("UPDATE conversationmessage SET seen = ? WHERE sender = ?");
        $query -> execute(array(1, $id));
        
    }
    
    function __construct ($id, $db) {
        $query = $db -> prepare("SELECT * FROM conversationmessage WHERE (sender = ? OR receiver = ?) ORDER BY id ASC");
        $query -> execute(array($id, $id));
        
        if ($query) {
            
            $contactDetails = $this -> contactDetails($id, $db);
            
            if ($query -> rowCount()) {
                
                while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                    $id = $row -> id;
                    $sender = $row -> sender;
                    $receiver = $row -> receiver;
                    $seen = $row -> seen;
                    $message = $row -> message;
                    $timeAdded = date('l dS \o\f F Y @ h:i:s A', $row -> timeadded);
                    
                    if ($sender == "admin") {
                        echo '<div class="messageLook messageReplyer">
                                <div class="emailName">
                                    Sender: <span style="color:#000;">Administrator</span><br>
                                    Time sent: <span style="color:#000;">'.$timeAdded.'</span><br><br>
                                    <span style="color:#000;font-size:16px">'.$message.'</span>
                                </div>
                                </div>';
                    } else {
                        if ($seen == false) {
                            $seenMessage = '<span style="color:#fff;padding:4px;float:right;background-color:green;font-size:12px;border-radius:2px;">New Message</span><br>';
                        } else {
                            $seenMessage = "";
                        }
                        
                        echo '
                            <div class="messageLook messageSender" id="cala'.$id.'">
                                '.$seenMessage.'
                                Sender: <span style="color:#000;">'.$contactDetails["name"].' ('.$contactDetails["contact"].')</span><br>
                                Time sent: <span style="color:#000;">'.$timeAdded.'</span><br><br>
                                <span style="color:#000;font-size:16px">'.$message.'</span>
                            </div>
                        ';
                        
                    }
                    
                    
                    
                }
                
                
            } else {
                echo '<div class="alert alert-info">No conversation has been started with '.$contactDetails["contact"].'</div>';
            }
            
        } else {
            echo '<div class="alert alert-info">An error occured</div>';
        }
    }
}

?>