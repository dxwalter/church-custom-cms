<?php

require_once ('../config/carrier.php');
require_once ('../scripts/mainFunctionFile.php');

class manageEvent extends mainFunctionFile{
    
    function setNextEvent($eventId, $actionValue, $db) {
        $query = $db -> prepare("UPDATE event SET nextevent = ?");
        $query -> execute(array(0));
        
        if ($query) {
            $query = $db -> prepare("UPDATE event SET nextevent = ? WHERE id = ? LIMIT 1");
            $query -> execute(array($actionValue, $eventId));
            
            if ($query) {
                echo '<div class="alert alert-success">This event has successfully been set as the next event. <a href="index.php?pageLet=event">Refresh page</a></div>';
            } else {
                echo '<div class="alert alert-danger">An error occured</div>';
            }
            
            
        }
    }
    
    function deleteEvent($eventId, $db) {
        $query = $db -> prepare("DELETE FROM event WHERE id = ? LIMIT 1");
        $query -> execute(array($eventId));
        if ($query) {
            echo '
            <div class="alert alert-success">This event has successfully been deleted. <a href="index.php?pageLet=event">Refresh page</a></div>';
        } else {
            echo '<div class="alert alert-danger">An error occured</div>';
        }
        
    }
    
    
    function activateEvent($eventId, $db) {
        $query = $db -> prepare("UPDATE event SET status = ? WHERE id = ? LIMIT 1");
        $query -> execute(array(1, $eventId));
        if ($query) {
            $siteUrl = "http://www.kcmi-rcc.com/pages/singleEvent.php?ijn=$eventId";
            $type = "event";
            $this -> sitemapFunction ($siteUrl, $type);
            
            
            echo '
            <div class="alert alert-success">This event has successfully been activated. <a href="index.php?pageLet=event">Refresh page</a></div>';
        } else {
            echo '
            <div class="alert alert-danger">An error occured</div>';
        }
    }
    
    function deactivate($eventId, $db) {
        $query = $db -> prepare("UPDATE event SET status = ? WHERE id = ? LIMIT 1");
        $query -> execute(array(0, $eventId));
        if ($query) {
            echo '
            <div class="alert alert-success">This event has deactivated been activated. <a href="index.php?pageLet=event">Refresh page</a></div>';
        } else {
            echo '
            <div class="alert alert-danger">An error occured</div>';
        }
    }
    
    
    function __construct($actionType, $eventId, $db) {
        
        if ($actionType && $eventId) {
            
            if ($actionType == "delete") {
                
                $this -> deleteEvent($eventId, $db);
                
            } else if ($actionType == "confirm") {
                
                $this -> activateEvent($eventId, $db);
                
            } else if ($actionType == "unconfirm") {
                
                $this -> deactivate($eventId, $db);
                
            } else if ($actionType == "nextEvent") {
                
            }
            
        } else {
            echo '
            <div class="alert alert-info">An error occured. Contact the technical team or try to refresh this page</div>';
        }
        
    }
}

if (isset($_POST["eventId"]) && isset($_POST["actionType"])) {
    $actionType = $_POST["actionType"];
    $eventId = $_POST["eventId"];
    
    if ($actionType == "nextEvent") {
        $actionType = 1;
        $actionValue = $_POST["actionValue"];
        $eventManager = new manageEvent($actionType, $eventId, $db);
        $eventManager -> setNextEvent($eventId, $actionValue, $db);
        
    } else {
        $eventManager = new manageEvent($actionType, $eventId, $db);   
    }
    
}


?>