<div class="row">
    
    
    <div class="col-lg-12 main-chart">
        <h3><i class="fa fa-angle-right"></i> Shared Testimonies</h3>
       <aside class="col-lg-12 mt">
          <div id="external-events">
              <div class="btn-group btn-group-justified">
                  <div class="btn-group">
                    <a href="index.php?pageLet=testimony&amp;type=view" class="btn btn-default">List Of Published Testimonies</a>
                  </div>
                  <div class="btn-group">
                    <a href="index.php?pageLet=testimony&amp;type=add" class="btn btn-default">Unpublished Testimonies</a>
                  </div>
    <!--
                  <div class="btn-group">
                    <a href="index.php?pageLet=fromPulpit&amp;type=""" class="btn btn-default">Right</a>
                  </div>
    -->
            </div>
          </div>
           
           <!-- This part holds all the contents -->
           <div>
               <br>
            <?php
                if (isset($_GET["type"])) {
                    $action = $_GET["type"];
                    
                    if ($action == "view") {
						require_once ('../scripts/allPublishedTestimonies.php');
                        $lastId = 0;
                        $callTestimonies = new allTestimonies($lastId, $db);
					} else if ($action == "add") {
						
                        require_once ('../scripts/unpublishedTestimony.php');
						
					} else {
						require_once ('../scripts/allPublishedTestimonies.php');
                        $lastId = 0;
                        $callTestimonies = new allTestimonies($lastId, $db);
					}
                } else {
					require_once ('../scripts/allPublishedTestimonies.php');
                    $lastId = 0;
                    
                    $callTestimonies = new allTestimonies($lastId, $db);
				}				
            ?>
           </div>
           
      </aside> 
    </div>
    
    
</div>