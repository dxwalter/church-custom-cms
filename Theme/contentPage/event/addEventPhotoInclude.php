<?php
    if (isset($_GET["eventId"])) {
        
        $id = $_GET["eventId"];
        if ($id) {
            $eventDetails = $mainFunc -> eventDetails ($id, $db);
        } else {
            header('location: index.php?pageLet=event');
        }
    } else {
        header('location: index.php?pageLet=event');
    }
?>

<div class="col-lg-12 main-chart">
       <!-- This is where the main content falls in -->
        <h4><i class="fa fa-angle-right"></i> Upload pictures of <?php echo $eventDetails["eventtitle"];  ?></h4>
        <hr>
        
        <form action="index.php?pageLet=event&state=addOldEventPhoto&eventId=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
            
        <?php require_once ('../scripts/eventPictureUpload.php'); ?>
       <!-- This is where the main content falls in -->
        <div class="showback" >
            <div class="form-group">
                
                <h4>Note: The image must be less than 300kb, it must be only in JPEG OR JPG format, the width must be 500px wide and the height can be any size</h4>
                
                <br>
                <small><b>UPLOAD FIRST IMAGE</b></small><br>
                <div id="InsertImage1"></div>
                <input class="form-control" type="file" name="img1"  onchange="PreviewImg1(event)">
            </div>
            <br>
            
            <div class="form-group">
                <small><b>UPLOAD SECOND IMAGE</b></small><br>
                <div id="InsertImage2"></div>
                <input class="form-control" type="file" name="img2"  onchange="PreviewImg2(event)">
            </div>
            <br>
            
            <div class="form-group">
                <small><b>UPLOAD THIRD IMAGE</b></small><br>
                <div id="InsertImage3"></div>
                <input class="form-control" type="file" name="img3"  onchange="PreviewImg3(event)">
            </div>
            <br>
            
            
            <div class="form-group">
                <small>Note: Let the pictures carry watermarks of the church logo and name.</small>
                <button type="submit" class="btn btn-info btn-lg btn-block" name="uploadPicture">Upload pictures</button>
            </div>
        </div>
    </form>
    
</div>