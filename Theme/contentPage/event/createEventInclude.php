<div class="col-lg-12 main-chart">
       <!-- This is where the main content falls in -->
        <h4><i class="fa fa-angle-right"></i> Create a new event</h4>
        <hr>
    
        <form action="index.php?pageLet=event&state=addNewEvent" method="post" enctype="multipart/form-data">
       <!-- This is where the main content falls in -->
        <div class="showback" >
            <div class="form-group">
                <small><b>TITLE OF EVENT</b></small>
                <input class="form-control" type="text" name="title" value="<?php if (isset($title)) {echo $title;} ?>">
            </div>
            
            <div class="form-group">
                <small><b>THEME OF EVENT</b></small>
                <input class="form-control" type="text" name="theme" value="<?php if (isset($theme)) {echo $theme;} ?>">
            </div>
            
            <div class="form-group">
                <small><b>BIBLE VERSE FOR THE EVENT. * Optional *<br>Use this format[Mathew 15 vs 1 - 23]</b></small>
                <input class="form-control" type="text" name="bibleVerse" value="<?php if (isset($bibleverse)) {echo $bibleverse;} ?>">
            </div>
            
            <div class="form-group">
                <small><b>DATE OF EVENT</b></small>
                <div class="row">
                    <div class="col-md-4">
                        <?php 
                            echo '<select name="day" class="form-control">';
                                    echo '<option value="" selected>Day of event</option>';
                                for ($x = 1; $x < 32; $x++) {
                                    echo '<option value="'.$x.'">'.$x.'</option>';
                                }  
                            echo '</select>';
                        ?>
                    </div>
                        <div class="col-md-4">
                            <select name="month" class="form-control">
                                <option value="" selected>Month of event</option>
                                <option value="JAN" >January</option>
                                <option value="FEB" >Febuary</option>
                                <option value="MARCH" >March</option>
                                <option value="APRIL" >April</option>
                                <option value="MAY" >May</option>
                                <option value="JUNE" >June</option>
                                <option value="JULY" >July</option>
                                <option value="AUG" >August</option>
                                <option value="SEPT" >September</option>
                                <option value="OCT" >October</option>
                                <option value="NOV" >November</option>
                                <option value="DEC" >December</option>
                            </select>
                        </div>
                    <div class="col-md-4">
                        <?php 
                            echo '<select name="year" class="form-control">';
                                    echo '<option value="" selected>Year of event</option>';
                                for ($x = 2017; $x < 2030; $x++) {
                                    echo '<option value="'.$x.'">'.$x.'</option>';
                                }  
                            echo '</select>';
                        ?>
                    </div>
                </div>
            </div>
            
            
            <div class="form-group">
                <small><b>TIME OF EVENT</b></small>
                <div class="row">
                    <div class="col-md-6">
                        <b><small>It should be in this format "8:00 am"</small></b>
                        <div class="form-group">
                            <input class="form-control" type="text" name="startTime" value="<?php if (isset($startTime)) {echo $startTime;} ?>" placeholder="Start Time only">
                        </div>
                    </div>                    
                </div>
            </div>
            
            
            <div class="form-group">
                <small><b>UPLOAD IMAGE EVENT</b></small><br>
                <small>Note: The image must be less than 500kb, it must be only in JPEG OR JPG format, the width must be 1200px wide and the height can be any size</small>
                <div id="InsertImage"></div>
                <input class="form-control" type="file" name="img"  onchange="PreviewImg(event)">
            </div>
            
            <div class="form-group">
                <small><b>WRITE ABOUT THIS EVENT</b></small><br>
                <small>The write up for this event should be written and formatted in microsoft office before pasting in the box below</small>
                <textarea class="form-control" name="writeup" rows="10" style="font-size:16px;"><?php if (isset($writeup)) {echo $writeup;} ?></textarea>
            </div>
            
            <div class="form-group">
                <small>Note: If you're not sure of the information you're providing please don't click the CREATE EVENT button below.</small>
                <button type="submit" class="btn btn-info btn-lg btn-block" name="newEvent">CREATE EVENT</button>
            </div>
        </div>
    </form>
    
</div>