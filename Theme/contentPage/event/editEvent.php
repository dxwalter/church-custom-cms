<?php
    
    require_once ('../scripts/editEvent.php');

    if (isset($_GET["eventId"])) {
        $eventId = $_GET["eventId"];
        if ($eventId) {
            $eventDetails = $mainFunc -> eventDetails($eventId, $db);
            
            $title = $eventDetails["eventtitle"];
            $theme = $eventDetails["eventheme"];
            $bibleverse = $eventDetails["bibleverse"];
            $startTime = $eventDetails["eventTime"];
            $writeup = $eventDetails["writeup"];
            
        } else {
            header('location: index.php?pageLet=event');
        }
    } else header('location: index.php?pageLet=event');

?>
<div class="col-lg-9 main-chart">
       <!-- This is where the main content falls in -->
        <h4><i class="fa fa-angle-right"></i> Edit this event</h4>
        <hr>
    
        <?php

            if(isset($createEvent->err["1"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[1]."<br></div>";
            }else if(isset($createEvent->err["2"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[2]."<br></div>";
            }else if(isset($createEvent->err["3"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[3]."<br></div>";
            }else if(isset($createEvent->err["4"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[4]."<br></div>";
            }else if(isset($createEvent->err["5"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[5]."<br></div>";
            }else if(isset($createEvent->err["6"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[6]."<br></div>";
            }else if(isset($createEvent->err["7"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[7]."<br></div>";
            }else if(isset($createEvent->err["8"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[8]."<br></div>";
            }else if(isset($createEvent->err["9"])){
                echo "<div class=\"alert alert-success\"><strong>SUCCESS:</strong> ".$createEvent->err[9]."<br></div>";
            }

        ?>
    
    
        <form action="index.php?pageLet=editEvent&eventId=<?php echo $eventId; ?>" method="post" enctype="multipart/form-data">
       <!-- This is where the main content falls in -->
        <div class="showback" >
            <div class="form-group">
                <small><b>TITLE OF EVENT</b></small>
                <input class="form-control" type="text" name="title" value="<?php if (isset($title)) {echo $title;} ?>">
            </div>
            
            <div class="form-group">
                <small><b>THEME OF EVENT</b></small>
                <input class="form-control" type="text" name="theme" value="<?php if (isset($theme)) {echo $theme;} ?>">
            </div>
            
            <div class="form-group">
                <small><b>BIBLE VERSE FOR THE EVENT. * Optional *<br>Use this format[Mathew 15 vs 1 - 23]</b></small>
                <input class="form-control" type="text" name="bibleVerse" value="<?php if (isset($bibleverse)) {echo $bibleverse;} ?>">
            </div>
            
            <div class="form-group">
                <small><b>DATE OF EVENT</b></small>
                <div class="row">
                    <div class="col-md-4">
                        <?php 
                            echo '<select name="day" class="form-control">';
                                    echo '<option value="" selected>Day of event</option>';
                                for ($x = 1; $x < 32; $x++) {
                                    echo '<option value="'.$x.'">'.$x.'</option>';
                                }  
                            echo '</select>';
                        ?>
                    </div>
                        <div class="col-md-4">
                            <select name="month" class="form-control">
                                <option value="" selected>Month of event</option>
                                <option value="JAN" >January</option>
                                <option value="FEB" >Febuary</option>
                                <option value="MARCH" >March</option>
                                <option value="APRIL" >April</option>
                                <option value="MAY" >May</option>
                                <option value="JUNE" >June</option>
                                <option value="JULY" >July</option>
                                <option value="AUG" >August</option>
                                <option value="SEPT" >September</option>
                                <option value="OCT" >October</option>
                                <option value="NOV" >November</option>
                                <option value="DEC" >December</option>
                            </select>
                        </div>
                    <div class="col-md-4">
                        <?php 
                            echo '<select name="year" class="form-control">';
                                    echo '<option value="" selected>Year of event</option>';
                                for ($x = 2017; $x < 2030; $x++) {
                                    echo '<option value="'.$x.'">'.$x.'</option>';
                                }  
                            echo '</select>';
                        ?>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <small><b>TIME OF EVENT</b></small>
                <div class="row">
                    <div class="col-md-6">
                        <b><small>It should be in this format "8:00 am"</small></b>
                        <div class="form-group">
                            <input class="form-control" type="text" name="startTime" value="<?php if (isset($startTime)) {echo $startTime;} ?>" placeholder="Start Time only">
                        </div>
                    </div>                    
                </div>
            </div>
            
            <div class="form-group">
                <small><b>UPLOAD IMAGE EVENT</b></small><br>
                <small>Note: The image must be less than 500kb, it must be only in JPEG OR JPG format, the width must be 1200px wide and the height can be any size</small>
                <br>
                <small>If you upload a new image it change the previously uploaded image</small>
                <div id="InsertImage"></div>
                <input class="form-control" type="file" name="img"  onchange="PreviewImg(event)">
            </div>
            
            <div class="form-group">
                <small><b>WRITE ABOUT THIS EVENT</b></small><br>
                <small>The write up for this event should be written and formatted in microsoft office before pasting in the box below</small>
                <textarea class="form-control" name="writeup" rows="10" style="font-size:16px;"><?php if (isset($writeup)) {echo $writeup;} ?></textarea>
            </div>
            
            <div class="form-group">
                <small>Note: If you're not sure of the information you're providing please don't click the CREATE EVENT button below.</small>
                <button type="submit" class="btn btn-info btn-lg btn-block" name="newEvent">Submit Changes</button>
            </div>
        </div>
    </form>
    
</div>

<script type="text/javascript">
function PreviewImg(event){
    var scr = URL.createObjectURL(event.target.files[0]);
    $('#InsertImage').html("<div class=\"newsFeedImg\"><img src=\"" + scr + "\"></div>");
}
</script>