<?php



	if (isset($_GET["id"])) {
		$messageId = $_GET["id"];
		
		$query = $db -> prepare("SELECT * FROM sermon WHERE id = ? LIMIT 1");
		$query -> execute(array($messageId));
		
		if ($query -> rowCount()) {
			
			while ($row = $query -> fetch(PDO::FETCH_ASSOC)) {
				$title = $row["title"];
				$datePreached = $row["datepreached"];
				$preacher = $row["preacher"];
				$bibleverse = $row["bibleverse"];
				$bibleText = $row["bibletext"];
				$word = $row["message"];
			}
		}
	}
?>


<h4><i class="fa fa-angle-right"></i> Edit This Message</h4>
<hr>
<div class="col-md-8">
    <form action="index.php?pageLet=fromPulpit&type=edit&id=<?php echo $messageId; ?>" method="post">
       <!-- This is where the main content falls in -->
        <div class="showback" >
            <div class="form-group">
                <small><b>TITLE OF MESSAGE</b></small>
                <input class="form-control" type="text" name="title" value="<?php if (isset($title)) {echo $title;} ?>">
            </div>

            <div class="form-group">
                <small><b>DATE PREACHED</b></small><br>
                <small>It should be in this format 15th May, 2017</small>
                <input class="form-control" type="text" name="datePreached" value="<?php if (isset($datePreached)) {echo $datePreached;} ?>">
            </div>
            
            <div class="form-group">
                <small><b>Preachers Name</b></small><br>
                <input class="form-control" type="text" name="author" value="<?php if (isset($preacher)) {echo $preacher;} ?>">
            </div>
            
            <div class="form-group">
                <small><b>BIBLE VERSE.<br>Use this format "Mathew 15 vs 1 - 23"</b></small>
                <input class="form-control" type="text" name="bibleVerse" value="<?php if (isset($bibleverse)) {echo $bibleverse;} ?>">
            </div>

            <div class="form-group">
                <small><b>BIBLE TEXT <br> This will the text from of the bible verse above</b></small>
                <textarea class="form-control" rows="2.5" name="bibleText"><?php if (isset($bibleText)) {echo $bibleText;} ?></textarea>
            </div>

            <div class="form-group">
                <small><b>THE WORD</b></small><br>
                <small>The write up for this event should be written and formatted in microsoft office before pasting in the box below</small>
                <textarea class="form-control" name="word" rows="10" style="font-size:16px;"><?php if (isset($word)) {echo $word;} ?></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-info btn-lg btn-block" name="editMessage">Upload Message</button>
            </div>
        </div>
    </form>
</div>