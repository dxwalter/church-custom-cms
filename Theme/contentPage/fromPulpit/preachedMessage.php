<div class="row">
    
    
    <div class="col-lg-12 main-chart">
        <h3><i class="fa fa-angle-right"></i> Upload Messages</h3>
        <?php require_once ('../scripts/uploadMessage.php'); ?>
		<?php require_once ('../scripts/editMessageFromPulpit.php'); ?>
       <aside class="col-lg-12 mt">
          <div id="external-events">
              <div class="btn-group btn-group-justified">
                  <div class="btn-group">
                    <a href="index.php?pageLet=fromPulpit&amp;type=add" class="btn btn-default">Add New Message</a>
                  </div>
                  <div class="btn-group">
                    <a href="index.php?pageLet=fromPulpit&amp;type=view" class="btn btn-default">View All Messages</a>
                  </div>
    <!--
                  <div class="btn-group">
                    <a href="index.php?pageLet=fromPulpit&amp;type=""" class="btn btn-default">Right</a>
                  </div>
    -->
            </div>
          </div>
           
           <!-- This part holds all the contents -->
           <div>
            <?php
                if (isset($_GET["type"])) {
                    $action = $_GET["type"];
                    
                    if ($action == "add") {
                        require_once ('../contentPage/fromPulpit/createMessageForm.php');
                    } else if ($action == "view") {
                       
                        echo '
                        <h4><i class="fa fa-angle-right"></i> List of messages</h4>
                        <hr>';
                        
                        require_once ('../scripts/messageList.php');
                        $lastId = 0;
                        $callMessage = new messageList($lastId, $db);
                        
                    } else if ($action == "edit") {
                        require_once ('../contentPage/fromPulpit/editMessage.php');
                    }
                }   
            ?>
           </div>
           
      </aside> 
    </div>
    
    
</div>