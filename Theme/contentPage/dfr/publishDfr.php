<div class="col-lg-12 main-chart">
<!-- This is where the main content falls in -->
    <h4><i class="fa fa-angle-right"></i> Publish DFR for Today
        <br>
        <small>List of DFR messages yet to be published</small>
    </h4>
    <hr>
<?php
    require_once ('../scripts/publishDfr.php');
    $lastId = 0;
    $publishDfr = new publishDfr($lastId, $db);    
?>
</div>