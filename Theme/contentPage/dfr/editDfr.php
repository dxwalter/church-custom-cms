<?php

    if (isset($_GET["id"])) {
        $id = $_GET["id"];
        $dfrData = $mainFunc -> dfrDetails ($id, $db);
    }

?>
<div class="col-lg-12 main-chart">
       <!-- This is where the main content falls in -->
        <h4><i class="fa fa-angle-right"></i> Edit Daily Faith Recharge (<?php echo $dfrData["title"]; ?>)</h4>
        <hr>

        <form action="index.php?pageLet=dfr&type=editDfr&id=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
       <!-- This is where the main content falls in -->
        <div class="showback" >
            <div class="form-group">
                <small><b>TITLE OF DFR</b></small>
                <input class="form-control" type="text" name="title" value="<?php if (isset($title)) {echo $title;} else { echo $dfrData["title"]; } ?>">
            </div>


            <div class="form-group">
                <small><b>CHARGING TERMINAL.<br>Use this format[Mathew 15 vs 1 - 23]</b></small>
                <input class="form-control" type="text" name="bibleVerse" value="<?php if (isset($bibleverse)) {echo $bibleverse;} else {echo $dfrData["terminal"];} ?>">
            </div>

            <div class="form-group">
                <small><b>Bible Text <br> This will the text from of the charging terminal above</b></small>
                <textarea class="form-control" rows="2.5" name="bibleText"><?php if (isset($bibleText)) {echo $bibleText;} else {echo $dfrData["bibletext"];} ?></textarea>
            </div>

            <div class="form-group">
                <small><b>THE WORD</b></small><br>
                <small>The write up for this event should be written and formatted in microsoft office before pasting in the box below</small>
                <textarea class="form-control" name="word" rows="10" style="font-size:16px;"><?php if (isset($word)) {echo $word;} else {echo $dfrData["word"];} ?></textarea>
            </div>

            <div class="form-group">
                <small><b>BLESSINGS</b></small><br>
                <small>The write up for this event should be written and formatted in microsoft office before pasting in the box below</small>
                <textarea class="form-control" name="blessing" rows="2.5" style="font-size:16px;"><?php if (isset($blessing)) {echo $blessing;} else {echo $dfrData["blessing"];} ?></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-info btn-lg btn-block" name="editDfr">SAVE CHANGES</button>
            </div>
        </div>
    </form>

</div>
