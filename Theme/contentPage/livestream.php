<div class="row">
    <div class="col-lg-8 main-chart">
    <form action="index.php?pageLet=livestream" method="post">
       <!-- This is where the main content falls in -->
        <h3><i class="fa fa-angle-right"></i> Upload Livestream Iframe</h3>
        <hr>
        <?php require_once('../scripts/livestreamClass.php'); ?>
        <div class="showback" >
            
            <div class="form-group">
                <small><b>Paste Iframe.</b></small>
                <textarea name="iframeText" rows="3" class="form-control"></textarea>
            </div>
            
            
            <div class="form-group">
                <button type="submit" class="btn btn-info btn-lg btn-block" name="iframeBtn">Upload</button>
            </div>
        </div>
    </form>
    </div>

</div>