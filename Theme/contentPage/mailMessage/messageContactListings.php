<div class="row">
    
    <div class="col-lg-12 main-chart">
        <?php require_once ('../scripts/emailUiClass.php'); ?>
        <?php require_once ('../scripts/sendGeneralMessage.php'); ?>
    <h4  style="float:left"><i class="fa fa-angle-right"></i> Unread messages</h4>
        <div style="float:right">
            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#sendMessage">Send Message</button>
        </div>
    <hr style="clear:both;">
    <?php require_once ('../scripts/unreadMessageContactListing.php'); ?>
    
    <h4><i class="fa fa-angle-right"></i> Contact Listing</h4>
    <hr>
    <?php require_once ('../scripts/recentMessageListing.php'); ?>
    </div>

</div>

<div class="modal fade" id="sendMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Send Message</h4>
      </div>
        <form action="" method="post">
      <div class="modal-body">
        
        <div class="form-group">
                <small><b>Message Subject</b></small>
                <input class="form-control" type="text" name="subject" value="<?php if (isset($subject)) {echo $subject;} ?>">
        </div>
        
        <div class="form-group">
            <small><b>Name OF Sender</b></small>
            <input class="form-control" type="text" name="name" value="<?php if (isset($senderName)) {echo $senderName;} ?>">
        </div>
          
        <div class="form-group">
            <small><b>Recipients' Email Address</b></small>
            <input class="form-control" type="text" name="email" value="<?php if (isset($email)) {echo $email;} ?>">
        </div>
          
        <div class="form-group">
            <small><b>Type Your Message</b></small>
            <textarea class="form-control" name="writeup" rows="10" style="font-size:16px;"><?php if (isset($writeup)) {echo $writeup;} ?></textarea>
        </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="generalMessage">Send Message</button>
      </div>
    </form>
    </div>
  </div>
</div>