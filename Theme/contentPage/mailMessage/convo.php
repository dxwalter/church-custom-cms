<div class="row">
    <div class="col-lg-12 main-chart">
        
<?php

    if (isset($_GET["convoId"])) {
        $convoId = $_GET["convoId"];
        
    } else header('location: index.php?pageLet=contactList');
    
    $contactDetails = $mainFunc -> contactDetails($convoId, $db);
    if (!$contactDetails) {
        echo '<div class="alert alert-info">This user\'s account doesn\'t exist</div>';
    }

?>

       <!-- This is where the main content falls in -->
        <h3><i class="fa fa-angle-right"></i> Conversation with <?php echo $contactDetails["name"]; ?></h3>
        <hr>
        
        <?php require_once('../scripts/replyMessage.php'); ?>
        
        <div class="showback" style="padding:5px;">
            <h4><i class="fa fa-angle-right"></i> Details of <?php echo $contactDetails["name"]; ?></h4>
            <h5 style="padding:3px;font-size:16px;"><b>Name</b>: <?php echo $contactDetails["name"]; ?></h5>
            <h5 style="padding:3px;font-size:16px;"><b>Contact</b>: <?php echo $contactDetails["contact"]; ?></h5>
      	</div>
        <br>
        
        <?php 
        
            require_once ('../scripts/callMailMessage.php');
            $callMessages = new callMailMessage ($convoId, $db);
            $callMessages -> setAllToRead($convoId, $db);
        
        ?>
    

        <div class="col-lg-12" id="messageBox" style="clear:both;">
            <div class="showback">
            <h4>Reply Message</h4>
            
            <form action="<?php echo $_SERVER["PHP_SELF"].'?pageLet=convo&convoId='.$convoId.''; ?>#messageBox" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="subject" class="form-control" placeholder="Subject Of Message">
                </div>
                
                <div class="form-group">
                    <textarea class="form-control" rows="10" placeholder="Type your Message" name="message"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-block btn-lg" name="replyMessage">Send Message</button>
                </div>
            </form>
        </div>
        </div>
        
    </div>
        
</div>