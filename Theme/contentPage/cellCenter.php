<div class="row">
    <div class="col-lg-9 main-chart">
       <!-- This is where the main content falls in -->
        <h3><i class="fa fa-angle-right"></i> Cell Centers</h3>
        <hr>
        
        <a href="">
            <div class="col-md-6 col-sm-6 mb">
                <div class="white-panel pn donut-chart">
                    <div class="row" style="padding:60px 10px 10px 10px;color:#999;">
                        <div><span class="glyphicon glyphicon-plus-sign" style="font-size:40px;"></span></div>
                        <h3>ADD A NEW CELL CENTER</h3>
                    </div>
                </div><!-- --/grey-panel ---->
            </div>
        </a>
        
        <!--  This is to show previous programs   -->
        <a href="">
            <div class="col-md-6 col-sm-6 mb">
                <div class="white-panel pn donut-chart">
                    <div class="row" style="padding:60px 15px 15px 15px;color:#999;">
                        <div><span class="glyphicon glyphicon-home" style="font-size:40px;"></span></div>
                        <h3>SHOW ALL CELL CENTERS</h3>
                    </div>
                </div><!-- --/grey-panel ---->
            </div>
        </a>
        
    </div>
        <!-- This is the notification area --> 
        <?php require_once('inc/notification.php'); ?>
</div>