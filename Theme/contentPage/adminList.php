<div class="row">
    <div class="col-lg-9 main-chart">
       <!-- This is where the main content falls in -->
        <h3><i class="fa fa-angle-right"></i> LIST OF ADMINISTRATORS</h3>
        <hr>
        <?php require_once('../scripts/setNewTask.php'); ?>
        <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <?php 
                                require_once ('../scripts/listOfAdministrators.php');
                                $callAmins = new adminAccountSetup($userId, $db);
                            ?>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div>
        
        
        
    </div>
    
    
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="ajaxModal" class="modal fade">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Assign Task</h4>
                  </div>
                  <div class="modal-body" id="assignTaskVisuals">


                  </div>
                  <div class="modal-footer" style="clear: both;border:0px;">
                      <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                  </div>
              </div>
          </div>
    </div>

    
        <!-- This is the notification area --> 
        <?php //require_once('inc/notification.php'); ?>
</div>