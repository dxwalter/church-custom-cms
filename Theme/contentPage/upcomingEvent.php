<div class="row">
    
    <div class="col-lg-9 main-chart">
       <!-- This is where the main content falls in -->
        <h3><i class="fa fa-angle-right"></i> Add an upcoming program</h3>
        <hr>
        <?php require_once('../scripts/createNewEventClass.php'); ?>
        <?php

            if(isset($createEvent->err["1"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[1]."<br></div>";
            }else if(isset($createEvent->err["2"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[2]."<br></div>";
            }else if(isset($createEvent->err["3"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[3]."<br></div>";
            }else if(isset($createEvent->err["4"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[4]."<br></div>";
            }else if(isset($createEvent->err["5"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[5]."<br></div>";
            }else if(isset($createEvent->err["6"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[6]."<br></div>";
            }else if(isset($createEvent->err["7"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[7]."<br></div>";
            }else if(isset($createEvent->err["8"])){
                echo "<div class=\"alert alert-danger\"><strong>ERROR:</strong> ".$createEvent->err[8]."<br></div>";
            }else if(isset($createEvent->err["9"])){
                echo "<div class=\"alert alert-success\"><strong>SUCCESS:</strong> ".$createEvent->err[9]."<br></div>";
            }

        ?>
        <!--  This is to add a new program      -->
          <div id="external-events">
              <div class="btn-group btn-group-justified" style="display: flex; justify-content: flex-end;">
                  <div class="btn-grou">
                    <a href="index.php?pageLet=event&state=addNewEvent" class="btn btn-primary btn-lg">Add A New Event</a>
                  </div>
                  <div class="btn-grou" style="margin-left: 8px;">
                    <a href="index.php?pageLet=event" class="btn btn-default btn-lg">View All Events</a>
                  </div>
    
            </div>
          </div>
    <?php
    
        if (isset($_GET["state"])) {
            $state = $_GET["state"];
            
            if ($state == "addNewEvent") {
                // include event form
                require_once ('../contentPage/event/createEventInclude.php');
            } else if ($state == "addOldEventPhoto") {
                // include program listing
                require_once ('../contentPage/event/addEventPhotoInclude.php');
            }
        } else {
            require_once ('../contentPage/event/recentlyAddedEvents.php');
        }
    
    ?>
    
    </div>
</div>
<script type="text/javascript">
function PreviewImg(event){
    var scr = URL.createObjectURL(event.target.files[0]);
    $('#InsertImage').html("<div class=\"newsFeedImg\"><img src=\"" + scr + "\"></div>");
}
</script>