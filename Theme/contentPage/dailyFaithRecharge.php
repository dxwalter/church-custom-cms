<div class="row">
    <div class="col-lg-9 main-chart">
       <!-- This is where the main content falls in -->
        <h3><i class="fa fa-angle-right"></i> Daily Faith Recharge</h3>
        <hr>
        <!-- CREATE DFR -->
        <div class="col-md-12 col-sm-12">
        <?php
            
            require_once ('../scripts/createDfr.php');
            require_once('../scripts/editDfr.php');
            require_once ('../scripts/listOfDfr.php');
        
        ?>
        </div>
          <div id="external-events">
              <div class="btn-group btn-group-justified">
                  <div class="btn-group">
                    <a href="index.php?pageLet=dfr&amp;type=new" class="btn btn-default">ADD NEW DFR</a>
                  </div>
                  <div class="btn-group">
                    <a href="index.php?pageLet=dfr&amp;type=publish" class="btn btn-default">PUBLISH DFR FOR TODAY</a>
                  </div>
    
                  <div class="btn-group">
                    <a href="index.php?pageLet=dfr&amp;type=allView" class="btn btn-default">View all DFR</a>
                  </div>
    
            </div>
          </div>
        
        <!-- this is a list of previously uploaded messages -->
        <?php
            
        if (isset($_GET["type"])) {
            $type = $_GET["type"];
            
            if ($type == "new") {
                // this is to include for create
                require_once ('../contentPage/dfr/createDfrFormInclude.php');
            } else if ($type == "publish") {
                // this is to call the list of 
                require_once ('../contentPage/dfr/publishDfr.php');
            } else if ($type == "allView") {
                require_once ('../contentPage/dfr/allViewDFR.php');
                
            } else if ($type == "editDfr") {
                require_once ('../contentPage/dfr/editDfr.php');
            } else {
                header('location: index.php?pageLet=dfr');
            }
            
        } else {
            require_once ('../contentPage/dfr/allViewDFR.php');
        }
        
        ?>
        
    </div>
        <!-- This is the notification area --> 
</div>