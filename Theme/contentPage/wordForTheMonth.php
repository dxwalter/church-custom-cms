<div class="row">
    <div class="col-lg-8 main-chart">
    <form action="index.php?pageLet=wordMonth" method="post" enctype="multipart/form-data">
       <!-- This is where the main content falls in -->
        <h3><i class="fa fa-angle-right"></i> Update word for the month</h3>
        <hr>
        <?php require_once('../scripts/wordFortheMonthClass.php'); ?>
        <div class="showback" >
            <div class="form-group">
                <small><b>Word for the month</b></small>
                <input class="form-control" type="text" name="word">
            </div>
            
            <div class="form-group">
                <small><b>Bible Verse for the month.<br>Use this format[Mathew 15 vs 1 - 23]</b></small>
                <input class="form-control" type="text" name="bibleVerse">
            </div>
            
            <div class="form-group">
                <small><b>Bible Text.</b></small>
                <textarea name="bibletext" rows="3" class="form-control"></textarea>
            </div>
            
            
            
            <div class="form-group">
                <small><b>Write up</b></small>
                <textarea class="form-control" name="writeup" rows="10" style="font-size:16px;"></textarea>
            </div>
            
            <div class="form-group">
                <button type="submit" class="btn btn-info btn-lg btn-block" name="wordMonth">Submit</button>
            </div>
        </div>
    </form>
    </div>

</div>