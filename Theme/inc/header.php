<?php 
require_once('../config/carrier.php');
require_once('../scripts/mainQueryFile.php');
require_once('../scripts/mainFunctionFile.php');

$mainFunc = new mainFunctionFile();
//$generalDb = new dbQuery();

    if (!isset($_COOKIE["_FK"]) || empty($userId)) {
        header('location: SignOut.php');
    } else {
    	setcookie('_FK', ''.$userId.'', time()+1800, "/");
    }


    if (isset($_COOKIE["_LV"])) {
        $level = $_COOKIE["_LV"];
        if ($level == false) {
            $level = $mainFunc -> userLevel($userId, $db);
        }
    } else {
        // retrieve level
        $level = $mainFunc -> userLevel($userId, $db);
    }
// this is to update last seen
$mainFunc -> updatelastSeen($userId, $db);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="robots" content="noindex, nofollow">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>Welcome -- Rehoboth Center</title>
    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="../assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="../assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="../assets/css/style.css" rel="stylesheet">
    <link href="../assets/css/style-responsive.css" rel="stylesheet">

    <script src="../assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.php" class="logo"><b>REHOBOTH</b></a>
            <!--logo end-->
          
            <?php require_once('../inc/navDropdown.php'); ?>
          
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="SignOut.php">Logout</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- *******************************************************  ***************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="#"><img src="../assets/img/mainLogo.png" class="img-circle" width="80"></a></p>
              	  <h5 class="centered"><?php echo $mainFunc -> adminBio ($userId, $db)["firstname"]." ".$mainFunc -> adminBio ($userId, $db)["lastname"];  ?></h5>
              	  	
                  <?php
                    
                    if ($level == "h") {
                       require_once ('../inc/levelMenu/majorAdminNavbar.php'); 
                    } else {
                        require_once ('../inc/levelMenu/minorAdminNavbar.php');
                    }
                  
                  ?>
                  
                  <li class="">
                    <a class="" href="index.php?pageLet=livestream">
                        <span>Livestream</span>
                    </a>
                </li>      

                  <!--<li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>UI Elements</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="general.php">General</a></li>
                          <li><a  href="buttons.php">Buttons</a></li>
                          <li><a  href="panels.php">Panels</a></li>
                      </ul>
                  </li> -->
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->