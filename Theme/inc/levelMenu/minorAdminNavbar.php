<?php

class callTaskNav extends mainFunctionFile{
    
	function testimonyCountUnconfirmed ($db) {
		
		$count = $this -> unconfirmedTestimonies ($db);
		
		if ($count) {
			return '('.$count.')';
		}
	}
	
    function __construct ($adminId, $db) {
        
        $mytask = $this -> callMyTask ($adminId, $db);
        $mytaskId = $mytask["adminid"];
        
        if ($mytaskId == $adminId) {
            
            $wordforthemonth = $mytask["wordforthemonth"];
            $upcomingProgram = $mytask["upcomingprogram"];
            $dfr = $mytask["dfr"];
            $testimonies = $mytask["testimonies"];
            $cellcenters = $mytask["cellcenters"];
            
            if ($wordforthemonth == true) {
                echo '
                    <li class="">
                      <a class="" href="index.php?pageLet=wordMonth">
                          <i class="fa fa-dashboard"></i>
                          <span>Word for the month</span>
                      </a>
                    </li>
                ';
            }
            
            if ($upcomingProgram == true) {
                echo '
                    <li class="">
                      <a class="" href="index.php?pageLet=event">
                          <i class="fa fa-dashboard"></i>
                          <span>Event Management</span>
                      </a>
                    </li>
                ';
            }
            
            if ($dfr) {
                echo '
                    <li class="sub-menu">
                        <a href="javascript:;" >
                              <i class="fa fa-desktop"></i>
                              <span>Sermons</span>
                        </a>
                        <ul class="sub">
                        <li><a class="" href="index.php?pageLet=fromPulpit&type=view">Service Messages</a></li>
                        <li><a class="" href="index.php?pageLet=dfr">Daily Faith Recharge</a></li>
                        </ul>
                    </li>
                ';
            }
            
            if ($testimonies) {
                echo '
                    <li class="">
                      <a class="" href="index.php?pageLet=testimony">
                          <i class="fa fa-dashboard"></i>
                          <span>Testimonies '.$this -> testimonyCountUnconfirmed ($db).'</span>
                      </a>
                    </li>
                ';
            }
            
            
            
        }
        
    }
    
}

$adminId = $userId;

$callNav = new callTaskNav ($adminId, $db);

?>