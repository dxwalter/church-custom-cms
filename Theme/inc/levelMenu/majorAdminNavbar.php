<?php $multiFunction = new mainFunctionFile; ?>
<!-- This is for the church admin -->
<li class="">
  <a class="" href="index.php?pageLet=dashboard">
      <span>Dashboard</span>
  </a>
</li>
                  
<li class="">
  <a class="" href="index.php?pageLet=wordMonth">
      <span>Word for the month</span>
  </a>
</li>         


<li class="">
  <a class="" href="index.php?pageLet=event">
      <span>Event Management
        <?php 
          $count = $multiFunction -> unconfirmedEvents($db);
          if ($count) {
              echo "($count)";
          }
        ?>      
        </span>
  </a>
</li>

<li class="sub-menu">
<a href="javascript:;" >
    <span>Sermons</span>
</a>
<ul class="sub">
    <li><a class="" href="index.php?pageLet=fromPulpit&type=view">Service Messages</a></li>
    <li><a class="" href="index.php?pageLet=dfr">Daily Faith Recharge</a></li>
</ul>
</li>

<li class="">
  <a class="" href="index.php?pageLet=testimony">
      <span>Testimonies
	  <?php
		$testimonyCount = $multiFunction -> unconfirmedTestimonies ($db);
		
		if ($testimonyCount) {
			echo '('.$testimonyCount.')';
		}
	  ?>
	  </span>
  </a>
</li>

<!-- <li class="">
  <a class="" href="index.php?pageLet=recentActivies">
      <span>Recent Activities</span>
  </a>
</li> -->

<li class="">
  <a class="" href="index.php?pageLet=adminQl">
      <span>Admin List</span>
  </a>
</li>