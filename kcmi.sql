-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2018 at 11:53 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kcmi`
--

-- --------------------------------------------------------

--
-- Table structure for table `activitylog`
--

CREATE TABLE IF NOT EXISTS `activitylog` (
  `id` bigint(20) NOT NULL,
  `adminid` varchar(70) NOT NULL,
  `message` text NOT NULL,
  `time` time NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activitylog`
--

INSERT INTO `activitylog` (`id`, `adminid`, `message`, `time`, `date`) VALUES
(1, '356a192b7913b04c54574d18c28d46e6395428ab', 'Daniel Walter created a new event COMMUNION SERVICE on this day December 30, 2017, 12:33 pm. Click here to view event <a href="" target="_blank"></a> to see event', '12:33:43', '2017-12-30'),
(2, '356a192b7913b04c54574d18c28d46e6395428ab', 'Daniel Walter updated the word for this month which is "<b>This is the latest word</b>" on this day "<b>December 30, 2017, 4:46 pm</b>". The bible verse choosen was "<b>Mathew 15 vs 1 - 23</b>".', '16:46:40', '2017-12-30'),
(3, '356a192b7913b04c54574d18c28d46e6395428ab', 'Daniel Walter updated the word for this month which is "<b>This is the latest word</b>" on this day "<b>December 30, 2017, 4:46 pm</b>". The bible verse choosen was "<b>Mathew 15 vs 1 - 23</b>".', '16:46:43', '2017-12-30'),
(4, '356a192b7913b04c54574d18c28d46e6395428ab', 'Daniel Walter updated the word for this month which is "<b>This is the latest word</b>" on this day "<b>December 30, 2017, 4:46 pm</b>". The bible verse choosen was "<b>Mathew 15 vs 1 - 23</b>".', '16:46:47', '2017-12-30'),
(5, '356a192b7913b04c54574d18c28d46e6395428ab', 'Daniel Walter updated the word for this month which is "<b>This is the latest word</b>" on this day "<b>December 30, 2017, 4:46 pm</b>". The bible verse choosen was "<b>Mathew 15 vs 1 - 23</b>".', '16:46:50', '2017-12-30'),
(6, '356a192b7913b04c54574d18c28d46e6395428ab', 'Daniel Walter created a new event KGM LEADERSHIP SUMMIT 2018 on this day April 28, 2018, 2:24 pm. Click here to view event <a href="" target="_blank"></a> to see event', '14:24:47', '2018-04-28');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` tinyint(4) NOT NULL,
  `mainid` varchar(70) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `tempPass` varchar(40) DEFAULT NULL,
  `valkey` int(20) NOT NULL,
  `level` varchar(3) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0 means unactivated account, 1 means activated account',
  `date` date NOT NULL,
  `time` int(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `mainid`, `firstname`, `lastname`, `email`, `password`, `tempPass`, `valkey`, `level`, `status`, `date`, `time`) VALUES
(1, '356a192b7913b04c54574d18c28d46e6395428ab', 'Daniel', 'Walter', 'gt@gmail.com', '25GQDfQrHqR72', NULL, 2522143, 'h', 1, '2017-12-28', 61027),
(2, 'da4b9237bacccdf19c0760cab7aec4a8359010b0', 'Daniel', 'Walter', 'admin1@gmail.com', '48XoMcf1b2W/.', NULL, 481040, '0', 1, '2017-12-30', 123116);

-- --------------------------------------------------------

--
-- Table structure for table `contentreadcheck`
--

CREATE TABLE IF NOT EXISTS `contentreadcheck` (
  `id` int(11) NOT NULL,
  `contentid` int(11) NOT NULL,
  `contenttype` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `dateadded` date NOT NULL,
  `timeadded` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contentreadcheck`
--

INSERT INTO `contentreadcheck` (`id`, `contentid`, `contenttype`, `address`, `dateadded`, `timeadded`) VALUES
(1, 13, 'pulpitmessage', '::1', '2017-12-24', '01:28:51'),
(2, 1, 'pulpitmessage', '::1', '2017-12-30', '16:59:27'),
(3, 1, 'dfr', '::1', '2017-12-30', '17:01:33'),
(4, 3, 'testimony', '::1', '2017-12-30', '17:03:13'),
(5, 1, 'event', '::1', '2017-12-30', '17:04:52'),
(6, 2, 'event', '::1', '2018-04-28', '15:26:02');

-- --------------------------------------------------------

--
-- Table structure for table `conversationbinder`
--

CREATE TABLE IF NOT EXISTS `conversationbinder` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `dateadded` date NOT NULL,
  `timeadded` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conversationbinder`
--

INSERT INTO `conversationbinder` (`id`, `name`, `contact`, `dateadded`, `timeadded`) VALUES
(1, 'Mathew John', 'realdanielwalter@gmail.com', '2018-03-08', '17:49:32');

-- --------------------------------------------------------

--
-- Table structure for table `conversationmessage`
--

CREATE TABLE IF NOT EXISTS `conversationmessage` (
  `id` int(11) NOT NULL,
  `sender` varchar(30) NOT NULL,
  `receiver` varchar(30) NOT NULL,
  `message` text NOT NULL,
  `seen` int(1) NOT NULL DEFAULT '0',
  `dateadded` date NOT NULL,
  `timeadded` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conversationmessage`
--

INSERT INTO `conversationmessage` (`id`, `sender`, `receiver`, `message`, `seen`, `dateadded`, `timeadded`) VALUES
(1, '1', 'admin', 'Our Locations\r\nRivers State, Nigeria\r\n\r\nOkwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria\r\n+234 808 458 3102\r\npastor@kcmi-rcc.com ', 1, '2018-03-08', 1520527772),
(2, 'admin', '1', 'Our Locations Rivers State, Nigeria Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria +234 808 458 3102 pastor@kcmi-rcc.com ', 1, '2018-03-13', 1520955173);

-- --------------------------------------------------------

--
-- Table structure for table `dfr`
--

CREATE TABLE IF NOT EXISTS `dfr` (
  `id` bigint(20) NOT NULL,
  `writer` varchar(70) NOT NULL,
  `title` varchar(300) NOT NULL,
  `terminal` varchar(200) NOT NULL,
  `bibletext` text NOT NULL,
  `word` text NOT NULL,
  `blessing` mediumtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT 'This means this dfr has been published before and it''s the DFR for the day (today)',
  `publish` int(1) DEFAULT '0' COMMENT 'This means this dfr has been published before',
  `publishdate` varchar(40) NOT NULL DEFAULT '0',
  `publishtime` time NOT NULL DEFAULT '00:00:00',
  `dateadded` date NOT NULL,
  `timeadded` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dfr`
--

INSERT INTO `dfr` (`id`, `writer`, `title`, `terminal`, `bibletext`, `word`, `blessing`, `status`, `publish`, `publishdate`, `publishtime`, `dateadded`, `timeadded`) VALUES
(1, '356a192b7913b04c54574d18c28d46e6395428ab', 'Communion Service', 'This is thehe', 'localhost/rest/main/pages/sin localhost/rest/main/pages/sin localhost/rest/main/pages/sin localhost/rest/main/pages/sin localhost/rest/main/pages/sin localhost/rest/main/pages/sin localhost/rest/main/pages/sin', 'localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now.localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now.localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now.localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now.', 'localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now.', 1, 1, 'Saturday 30th December, 2017', '16:35:34', '2017-12-30', '16:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` bigint(20) NOT NULL,
  `eventcreator` varchar(70) NOT NULL COMMENT 'This is the person that created the event',
  `eventtitle` varchar(200) NOT NULL,
  `eventheme` varchar(200) NOT NULL,
  `bibleverse` varchar(70) NOT NULL,
  `day` int(2) NOT NULL,
  `month` varchar(5) NOT NULL,
  `year` int(4) NOT NULL,
  `eventTime` varchar(20) NOT NULL,
  `image` varchar(500) NOT NULL,
  `writeup` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '1 means the event will hold',
  `nextevent` int(1) NOT NULL DEFAULT '0',
  `dateadded` date NOT NULL,
  `timeadded` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `eventcreator`, `eventtitle`, `eventheme`, `bibleverse`, `day`, `month`, `year`, `eventTime`, `image`, `writeup`, `status`, `nextevent`, `dateadded`, `timeadded`) VALUES
(1, '356a192b7913b04c54574d18c28d46e6395428ab', 'Communion Service', 'Let The Praise Go Up', 'Mathew 15 vs 1 - 23', 14, 'JULY', 2022, '8:00 AM', 'Communion_Service_14_JULY_2022.jpg', 'Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. v Oops! These are all the events we have for now.  v Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. v Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now.', 1, 0, '2017-12-30', '12:33:43'),
(2, '356a192b7913b04c54574d18c28d46e6395428ab', 'KGM Leadership Summit 2018', 'Decently & In Order', 'This is cool', 23, 'MAY', 2018, '5:30pm', 'KGM_Leadership_Summit_2018_23_MAY_2018.jpg', 'The MDL components are created with CSS, JavaScript, and HTML. You can use the components to construct web pages and web apps that are attractive, consistent, and functional. Pages developed with MDL will adhere to modern web design principles like browser portability, device independence, and graceful degradation. The MDL components are created with CSS, JavaScript, and HTML. You can use the components to construct web pages and web apps that are attractive, consistent, and functional. Pages developed with MDL will adhere to modern web design principles like browser portability, device independence, and graceful degradation. The MDL components are created with CSS, JavaScript, and HTML. You can use the components to construct web pages and web apps that are attractive, consistent, and functional. Pages developed with MDL will adhere to modern web design principles like browser portability, device independence, and graceful degradation. The MDL components are created with CSS, JavaScript, and HTML. You can use the components to construct web pages and web apps that are attractive, consistent, and functional. Pages developed with MDL will adhere to modern web design principles like browser portability, device independence, and graceful degradation.', 1, 1, '2018-04-28', '14:24:46');

-- --------------------------------------------------------

--
-- Table structure for table `eventgallery`
--

CREATE TABLE IF NOT EXISTS `eventgallery` (
  `id` int(11) NOT NULL,
  `eventid` int(11) NOT NULL,
  `imagepath` varchar(150) NOT NULL,
  `dateadded` date NOT NULL,
  `timeadded` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lastseen`
--

CREATE TABLE IF NOT EXISTS `lastseen` (
  `id` int(11) NOT NULL,
  `adminid` varchar(70) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lastseen`
--

INSERT INTO `lastseen` (`id`, `adminid`, `date`, `time`) VALUES
(1, '356a192b7913b04c54574d18c28d46e6395428ab', '2018-04-28', '14:24:53'),
(2, 'da4b9237bacccdf19c0760cab7aec4a8359010b0', '2018-04-28', '13:19:27');

-- --------------------------------------------------------

--
-- Table structure for table `sermon`
--

CREATE TABLE IF NOT EXISTS `sermon` (
  `id` bigint(20) NOT NULL,
  `title` varchar(150) NOT NULL,
  `bibleverse` varchar(100) NOT NULL,
  `bibletext` text NOT NULL,
  `message` text NOT NULL,
  `preacher` varchar(100) NOT NULL,
  `datepreached` varchar(100) NOT NULL,
  `uploader` varchar(70) NOT NULL,
  `dateadded` date NOT NULL,
  `timeadded` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sermon`
--

INSERT INTO `sermon` (`id`, `title`, `bibleverse`, `bibletext`, `message`, `preacher`, `datepreached`, `uploader`, `dateadded`, `timeadded`) VALUES
(1, 'Favour on every side', 'Mathew 15 vs 1 - 23', 'Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. ', 'Oops! These are all the events we have for now.Oops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. ', 'Apostle P. F. Aikins', '15th May, 2017 edit', '356a192b7913b04c54574d18c28d46e6395428ab', '2017-12-30', '16:32:59');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` bigint(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `dfr` int(1) NOT NULL DEFAULT '0',
  `sermon` int(1) NOT NULL DEFAULT '0',
  `newsletter` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `email`, `dfr`, `sermon`, `newsletter`) VALUES
(1, 'realdanielwalter@gmail.com', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `adminid` varchar(70) NOT NULL,
  `wordforthemonth` int(1) NOT NULL DEFAULT '0',
  `upcomingprogram` int(1) NOT NULL DEFAULT '0',
  `gallery` int(1) NOT NULL DEFAULT '0',
  `dfr` int(1) NOT NULL DEFAULT '0',
  `testimonies` int(1) NOT NULL DEFAULT '0',
  `cellcenters` int(1) NOT NULL DEFAULT '0',
  `message` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='if task is set to 1 then the admin is eligible to do that task';

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`adminid`, `wordforthemonth`, `upcomingprogram`, `gallery`, `dfr`, `testimonies`, `cellcenters`, `message`) VALUES
('356a192b7913b04c54574d18c28d46e6395428ab', 0, 0, 0, 0, 0, 0, 0),
('da4b9237bacccdf19c0760cab7aec4a8359010b0', 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `testimony`
--

CREATE TABLE IF NOT EXISTS `testimony` (
  `id` bigint(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `title` varchar(150) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `location` varchar(70) NOT NULL,
  `tag` varchar(100) DEFAULT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `validator` varchar(70) DEFAULT NULL,
  `timeadded` time NOT NULL,
  `dateadded` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimony`
--

INSERT INTO `testimony` (`id`, `name`, `title`, `contact`, `location`, `tag`, `message`, `status`, `validator`, `timeadded`, `dateadded`) VALUES
(1, 'Mathew John', 'Favour on every side', 'daniwaliponzi@gmail.com', 'Port harcourt, Nigeria', 'God''s Grace', 'localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now.                ', 1, '356a192b7913b04c54574d18c28d46e6395428ab', '16:36:21', '30th December, 2017'),
(2, 'Mathew John', 'Favour on every side', 'daniwaliponzi@gmail.com', 'Port harcourt, Nigeria', 'Blessings', '                localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now.                ', 1, '356a192b7913b04c54574d18c28d46e6395428ab', '16:40:38', '30th December, 2017'),
(3, 'Mathew John', 'Favour on every side', 'daniwaliponzi@gmail.com', 'Port harcourt, Nigeria', 'Blessings', 'ocalhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. localhost/rest/main/pages/sinlocalhost/rest/main/pages/sin localhost/rest/main/pages/sinOops! These are all the events we have for now.Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now. Oops! These are all the events we have for now.                ', 1, '356a192b7913b04c54574d18c28d46e6395428ab', '16:43:47', '30th December, 2017');

-- --------------------------------------------------------

--
-- Table structure for table `wordforthemonth`
--

CREATE TABLE IF NOT EXISTS `wordforthemonth` (
  `id` bigint(20) NOT NULL,
  `updater` varchar(70) NOT NULL COMMENT 'The person that updated word for the month',
  `word` varchar(100) NOT NULL,
  `bibleverse` varchar(40) NOT NULL,
  `bibletext` text NOT NULL,
  `writeup` text NOT NULL,
  `time` time NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wordforthemonth`
--

INSERT INTO `wordforthemonth` (`id`, `updater`, `word`, `bibleverse`, `bibletext`, `writeup`, `time`, `date`) VALUES
(1, '356a192b7913b04c54574d18c28d46e6395428ab', 'This is the latest word', 'Mathew 15 vs 1 - 23', 'Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. ', 'Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. ', '16:46:40', '2017-12-30'),
(2, '356a192b7913b04c54574d18c28d46e6395428ab', 'This is the latest word', 'Mathew 15 vs 1 - 23', 'Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. ', 'Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. ', '16:46:43', '2017-12-30'),
(3, '356a192b7913b04c54574d18c28d46e6395428ab', 'This is the latest word', 'Mathew 15 vs 1 - 23', 'Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. ', 'Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. ', '16:46:47', '2017-12-30'),
(4, '356a192b7913b04c54574d18c28d46e6395428ab', 'This is the latest word', 'Mathew 15 vs 1 - 23', 'Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. ', 'Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. Modals are built with HTML, CSS, and JavaScript. Theyâ€™re positioned over everything else in the document and remove scroll from the <body> so that modal content scrolls instead. ', '16:46:50', '2017-12-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activitylog`
--
ALTER TABLE `activitylog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contentreadcheck`
--
ALTER TABLE `contentreadcheck`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversationbinder`
--
ALTER TABLE `conversationbinder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversationmessage`
--
ALTER TABLE `conversationmessage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dfr`
--
ALTER TABLE `dfr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventgallery`
--
ALTER TABLE `eventgallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lastseen`
--
ALTER TABLE `lastseen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sermon`
--
ALTER TABLE `sermon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimony`
--
ALTER TABLE `testimony`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wordforthemonth`
--
ALTER TABLE `wordforthemonth`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activitylog`
--
ALTER TABLE `activitylog`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contentreadcheck`
--
ALTER TABLE `contentreadcheck`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `conversationbinder`
--
ALTER TABLE `conversationbinder`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `conversationmessage`
--
ALTER TABLE `conversationmessage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dfr`
--
ALTER TABLE `dfr`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `eventgallery`
--
ALTER TABLE `eventgallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lastseen`
--
ALTER TABLE `lastseen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sermon`
--
ALTER TABLE `sermon`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `testimony`
--
ALTER TABLE `testimony`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wordforthemonth`
--
ALTER TABLE `wordforthemonth`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
