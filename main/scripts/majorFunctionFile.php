<?php

class general {
    
    function cutText ($textString, $length) {
        if(strlen($textString) <= $length){
            return str_replace("&nbsp;", "", strip_tags(stripcslashes($textString)));
        }else{
            $y=substr($textString, 0, $length) . '...';
            return str_replace("&nbsp;", "", strip_tags(stripcslashes($y)));
        }
    }
    
    function selectSingleWord ($id, $db) {
        $query = $db -> prepare("SELECT * FROM wordforthemonth WHERE id = ? LIMIT 1");
        $query -> execute(array($id));
        
        if ($query -> rowCount()) {
            return $query -> fetch(PDO::FETCH_ASSOC);
        } else {
            header('location: 404.php');
        }
    }
    
    function singleDfrDetails ($id, $db) {
        $query = $db -> prepare("SELECT * FROM dfr WHERE id = ? LIMIT 1");
        $query -> execute(array($id));
        
        if ($query -> rowCount()) {
          return $query -> fetch(PDO::FETCH_ASSOC);
        } else {
            header('location: 404.php');
        }
    }
    
   function eventDetails ($eventId, $db) {
        $query = $db -> prepare("SELECT * FROM event WHERE id = ? LIMIT 1");
        $query -> execute(array($eventId));
        
        if ($query) {
            return $query -> fetch(PDO::FETCH_ASSOC);
        }
    }
    
    function testimonyDetails($testimonyId, $db) {
        $query = $db -> prepare("SELECT * FROM testimony WHERE id = ? LIMIT 1");
        $query -> execute(array($testimonyId));
        
        if ($query -> rowCount()) {
            
            return $query -> fetch(PDO::FETCH_ASSOC);
            
        } else {
            header('location: 404.php');
        }
    }
    
    function singleMessage($id, $db) {
        
        $query = $db -> prepare("SELECT * FROM sermon WHERE id = ? LIMIT 1");
        $query -> execute(array($id));
        
        if ($query -> rowCount()) {
          return $query -> fetch(PDO::FETCH_ASSOC);
        } else {
            header('location: 404.php');
        }
    }
    
}

?>