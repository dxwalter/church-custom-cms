<?php

class callDfrMessageList extends general {
    
    
    function dfrOutput ($id, $title, $publishdate, $word) {
        
        return '
                <li class="sermon-item format-standard">
                <div class="row">
                <div class="col-md-12">
                    <h3><a href="singleDfr.php?ijn='.$id.'">'.$title.'</a></h3>
                    <span class="meta-data"><i class="fa fa-calendar"></i>'.$publishdate.'</span>
                    <p>'.$word.'</p>
                    <a href="singleDfr.php?ijn='.$id.'" class="basic-link">Continue reading </a>
                </div>
                </div>
                </li>
        ';
        
    }
    
    function __construct ($lastId, $db) {
        
        if ($lastId) {
            $query = $db -> prepare("SELECT * FROM dfr WHERE id < ?AND publish = ? ORDER by id DESC LIMIT 7");
            $query -> execute(array($lastId, 1));
        } else {
            $query = $db -> prepare("SELECT * FROM dfr WHERE publish = ? ORDER by id DESC LIMIT 7");
            $query -> execute(array(1));
        }
        
        if ($query -> rowCount()) {
            
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                $id = $row -> id;
                $title = $row -> title;
                $publishdate = $row -> publishdate;
                $word = $this -> cutText ($row -> word, 400);
                
                echo $this -> dfrOutput ($id, $title, $publishdate, $word);
            }
            
            if ($query -> rowCount() > 6) {
                echo '
                    <div id="moreDfr'.$id.'">
                        <div class="col-md-push-6">
                            <nav aria-label="...">
                              <ul class="pager">
                                <li><button id="moreDfr" value="'.$id.'" data-request="moreDfr" type="button" class="btn btn-default btn-transparent event-tickets event-register-button btn-lg">Load More DFR\'S</button></li>
                              </ul>
                            </nav>
                        </div>
                    </div>
                ';
            } else {
                echo '
                    <div class="alert alert-info"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oops!</strong> That\'s all for now</div>
                ';
            }
            
        } else {
            echo '<div class="alert alert-info"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oops!</strong> That\'s all for now</div>';
        }
        
    }
    
}

?>