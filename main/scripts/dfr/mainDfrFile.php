<?php


class mainDfrFile extends general{
    
    public $sliderDfrId;
    public $sliderDfrTitle;
    public $sliderDfrMessage;
    
    function previousDRF($eventSliderId, $db) {
        $query = $db -> prepare("SELECT id FROM dfr WHERE id < ? AND publish = ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array($eventSliderId, 1));
        if ($query ->  rowCount()) {
            $result = $query -> fetch()[0];
            return '<li><a href="singleDfr.php?ijn='.$result.'"><span aria-hidden="true">&larr;</span> Previous</a></li>';
        }
    }
    
    function nextDRF($eventSliderId, $db) {
        $query = $db -> prepare("SELECT id FROM dfr WHERE id > ? AND publish = ? ORDER BY id ASC LIMIT 1");
        $query -> execute(array($eventSliderId, 1));
        if ($query ->  rowCount()) {
            $result = $query -> fetch()[0];
            return '<li><a href="singleDfr.php?ijn='.$result.'"> Next <span aria-hidden="true">&rarr;</span></a></li>';
        }
    }
    
    public function sliderDfr ($db) {
        
        $query = $db -> prepare("SELECT * FROM dfr WHERE status = ? AND publish = ? LIMIT 1");
        $query -> execute(array(1, 1));
        
        if ($query-> rowCount()) {
            while ($row = $query -> fetch(PDO::FETCH_ASSOC)) {
                
                
                $length = 200;
                $this -> sliderDfrId = $row["id"];
                $this -> sliderDfrTitle = $row["title"];
                $this -> sliderDfrMessage = $this -> cutText ($row["word"], $length);
            }
        }
        
    }
    
    function __construct($db) {
        
    }
    
}

$callDfrMainFile = new mainDFrFile($db);

?>