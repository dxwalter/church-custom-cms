<?php 

class wordFortheMonth extends general {
    
    public $wordTitle;
    public $bibleVerse;
    public $writeUp;
    public $wordId;
    
    function prevButton($id, $db) {
        $query = $db -> prepare("SELECT id FROM wordforthemonth WHERE id < ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array($id));
        
        $result = $query -> fetch()[0];
        
        if ($result) {
            return '<li class="previous"><a href="wordForTheMonth.php?ijn='.$result.'"><span aria-hidden="true">&larr;</span> Previous</a></li>';
        }
    }

    
    function nextButton($id, $db) {
        $query = $db -> prepare("SELECT id FROM wordforthemonth WHERE id > ? LIMIT 1");
        $query -> execute(array($id));
        
        $result = $query -> fetch()[0];
        
        if ($result) {
            return '<li class="previous"><a href="wordForTheMonth.php?ijn='.$result.'"><span aria-hidden="true">&larr;</span> Next</a></li>';
        }
    }
    
    function sliderWord () {
        $length = 200;
        $setUp = $this -> cutText ($this -> writeUp, $length);
        return $setUp;
    }
    
    function __construct ($db) {
        
        $query = $db -> prepare("SELECT * FROM wordforthemonth ORDER BY id DESC LIMIT 1");
        $query -> execute();
        
        if ($query -> rowCount()) {
            
            while ($row = $query -> fetch(PDO::FETCH_ASSOC)) {
                $this -> wordId = $row["id"];
                $this -> wordTitle = $row["word"];
                $this -> bibleVerse = $row["bibleverse"];
                $this -> writeUp = $row["writeup"];
            }
            
        }
    }
    
}

$callWordForTheMonth = new wordFortheMonth ($db);


?>