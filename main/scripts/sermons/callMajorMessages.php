<?php

class callMultipleSermons extends sermons {
    
    public $bufferOne;
    public $bufferTwo;
    
    function messageTileView ($id, $title, $datePreached, $message) {
        
        return '
            
            <li class="col-md-4 col-sm-6 sermon-item grid-item format-standard">
                <div class="grid-item-inner">
                    <!--<a href="sermonSingle.php?ijn='.$id.'" class="media-box">
                        <img src="../images/img_sermon4.jpg" alt="">
                    </a>-->
                    <div class="grid-content">
                        <span class="sermon-series-date">'.$datePreached.'</span>
                        <h3><a href="sermonSingle.php?ijn='.$id.'">'.$title.'</a></h3>
                        <p>'.$message.' </p>
                        <a href="sermonSingle.php?ijn='.$id.'" class="btn btn-primary">View series <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </li>
        
        ';
        
    }
    
    function __construct ($lastId, $db) {
        
        if ($lastId) {
            $query = $db -> prepare("SELECT * FROM sermon WHERE id < ? ORDER BY id DESC LIMIT 6");
            $query -> execute(array($lastId));
        } else {
            $query = $db -> prepare("SELECT * FROM sermon ORDER BY id DESC LIMIT 6");
            $query -> execute();
        }
        
        
        if ($query -> rowCount()) {
            
            $x = 0;
            
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                $datePreached = $row -> datepreached;
                $id = $row -> id;
                $title = $row -> title;
                $message = $this -> cutText ($row -> message, 350);
                
                $outPut = $this -> messageTileView ($id, $title, $datePreached, $message);
                
                if ($x < 3) {
                    $this -> bufferOne .= $outPut;
                    $x = $x + 1;
                } else if ($x >= 3) {
                    $this -> bufferTwo .= $outPut;
                    $x = $x + 1;
                }
            }
            
            
            $rowOne = '<ul class="isotope-grid">'.$this -> bufferOne.'</ul>';
            $rowTwo = '<ul class="isotope-grid">'.$this -> bufferTwo.'</ul>';
            
            echo $rowOne.$rowTwo;
            
            if ($query -> rowCount() > 5) {
                
                echo '
                    <div id="moreMessage'.$id.'">
                        <div class="col-md-push-6">
                            <nav aria-label="...">
                              <ul class="pager">
                                <li><button id="morePulpitSermons" value="'.$id.'" data-request="moreSermon" type="button" class="btn btn-default btn-transparent event-tickets event-register-button btn-lg">Load More Messages</button></li>
                              </ul>
                            </nav>
                        </div>
                    </div>
                ';
                
            }
            
            
        } else {
            echo '<div class="alert alert-info fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oops!</strong> Sermons are yet to be uploaded. <a href="testimonyList.php">Read The Testimonies Of Others</a>.</div>';
        }
        
        
    }
    
}

?>