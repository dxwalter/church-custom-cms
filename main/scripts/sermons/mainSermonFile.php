<?php

class sermons extends general {
    
    public $lastMessageTitle;
    public $lastMessageWord;
    public $lastMessageId;
    
    
    function singleMessagePrev($id, $db) {
        $query = $db -> prepare("SELECT id FROM sermon WHERE id < ?  ORDER BY id DESC LIMIT 1");
        $query -> execute(array($id));
        if ($query ->  rowCount()) {
            $result = $query -> fetch()[0];
            return '<li class="previous"><a href="sermonSingle.php?ijn='.$result.'"><span aria-hidden="true">&larr;</span> Previous</a></li>';
        }
    }
    
    function singleMessageNext($id, $db) {
        $query = $db -> prepare("SELECT id FROM sermon WHERE id > ?  ORDER BY id ASC LIMIT 1");
        $query -> execute(array($id));
        if ($query ->  rowCount()) {
            $result = $query -> fetch()[0];
            return '<li class="next"><a href="sermonSingle.php?ijn='.$result.'">Next <span aria-hidden="true">&rarr;</span></a></li>';
        }
    }
    
    
    function indexMessageListing ($db) {
        $query = $db -> prepare("SELECT * FROM sermon ORDER BY id DESC LIMIT 5");
        $query -> execute();
        
        if ($query -> rowCount()) {
            
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                $title = ucfirst($row -> title);
                $preacher = ucfirst($row -> preacher);
                $id = $row -> id;
                
                echo '
                    <li>
                        <a href="sermonSingle.php?ijn='.$id.'">
                        <strong class="post-title">'.$title.'</strong></a>
                            <div class="meta-data">by <a href="#">'.$preacher.'</a></div>
                    </li>
                ';
                
            }
        }
        
    }
    
    function lastUploadedSermon ($db) {
        
        $query = $db -> prepare("SELECT * FROM sermon ORDER BY id DESC LIMIT 1");
        $query -> execute();
        
        if ($query -> rowCount()) {
            
            while ($row = $query -> fetch(PDO::FETCH_ASSOC)) {
                $this -> lastMessageTitle = $row["title"];
                $this -> lastMessageWord = $this -> cutText ($row["message"], 200);
                $this -> lastMessageId = $row["id"];
            }
        }
        
    }
    
    function __construct ($db) {
        
        
        
        
    }
    
}


$callSermons = new sermons($db);

?>