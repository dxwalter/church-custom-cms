<?php

class indexGallery {
    
    
    function __construct ($db) {
        
        $query = $db -> prepare("SELECT id FROM eventgallery ORDER BY id DESC LIMIT 1");
        $query -> execute();
        
        $query = $db -> prepare("SELECT * FROM eventgallery ORDER BY id DESC LIMIT 5");
        $query -> execute();
        
        if ($query -> rowCount() > 0) {
            
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                $imagePath = $row -> imagepath;
                $newpath =  "../../Photouploads/gallery/".$imagePath;
                echo '<li class="format-image"><a href="memories.php" class="media-box"><img src="'.$newpath.'" alt=""></a></li>';
            }
            
        }
        
    }
    
}

?>