<?php 
    
class indexSlider {
    
    function __construct ($wordForThheMonthWriteUp, $wordFortheMonthTitle, $wordFortheMonthId, $eventSliderTitle, $eventSliderWord, $eventSliderImage, $eventSliderId, $dfrSliderId, $dfrSliderTitle, $dfrSliderMessage) {
        
        echo '<div class="hero-slider heroflex flexslider clearfix" data-autoplay="yes" data-pagination="no" data-arrows="yes" data-style="fade" data-speed="7000" data-pause="yes">
      	<ul class="slides">';
        
            // This is for Word for the month
        	echo '<li class="parallax" style="background-image:url(../images/KCMIPHOTO/index/apostle.jpg);">
            	<div class="flex-caption" data-appear-animation="fadeInRight" data-appear-animation-delay="500">
                	<strong style="font-size:30px;">HONOUR & DIGNITY</strong>
                	<p class="sm-dontShow"></p>
                    <p class="sm-Show" style="width:40%;"><a href="wordForTheMonth.php?ijn='.$wordFortheMonthId.'" class="btn btn-primary btn-sm">Learn More</a></p>
               	</div>
            </li>';
            
            // This is the latest event coming up
        	echo '<li class="parallax" style="background-image:url('.$eventSliderImage.');">
            	<div class="flex-caption" data-appear-animation="fadeInRight" data-appear-animation-delay="500">
                	<strong>'.$eventSliderTitle.'</strong>
                	<p class="sm-dontShow">'.$eventSliderWord.'</p>
                    <p class="sm-Show" style="width:40%;"><a href="singleEvent.php?ijn='.$eventSliderId.'" class="btn btn-primary btn-sm">Learn More</a></p>
               	</div>
            </li>';
        
        
            // This is for today's dfr    
            
            // echo '<li class="parallax" style="background-image:url(../images/slide5.jpg);">
            // 	<div class="flex-caption" data-appear-animation="fadeInRight" data-appear-animation-delay="500">
            //     	<strong>'.$dfrSliderTitle.'</strong>
            //     	<p class="sm-dontShow">'.$dfrSliderMessage.'</p>
            //         <p class="sm-Show" style="width:40%;"><a href="singleDfr.php?ijn='.$dfrSliderId.'" class="btn btn-primary btn-sm">Learn More</a></p>
            //    	</div>
            // </li>';
            
            
      	echo '</ul>
        <canvas id="canvas-animation"></canvas>
    </div>';
        
    }
    
}
    // Word For The Month
    $wordFortheMonthTitle = $callWordForTheMonth -> wordTitle;
    $wordForThheMonthWriteUp = $callWordForTheMonth -> sliderWord();
    $wordFortheMonthId = $callWordForTheMonth -> wordId;

    // Latest Event
    $callEventSlider = $events -> sliderEvent($db);
    $eventSliderId = $events -> sliderEventId;
    $eventSliderWord = $events -> sliderEventWriteUp;
    $eventSliderImage = $events -> sliderEventImage;
    $eventSliderTitle = $events -> sliderEventTitle;

    // Daily Faith Recharge
    $callDfrSlider = $callDfrMainFile -> sliderDfr ($db);
    $dfrSliderId = $callDfrMainFile -> sliderDfrId;
    $dfrSliderTitle = $callDfrMainFile -> sliderDfrTitle;
    $dfrSliderMessage = $callDfrMainFile -> sliderDfrMessage;

    $setUpSlider = new indexSlider ($wordForThheMonthWriteUp, $wordFortheMonthTitle, $wordFortheMonthId, $eventSliderTitle, $eventSliderWord, $eventSliderImage, $eventSliderId, $dfrSliderId, $dfrSliderTitle, $dfrSliderMessage);
?>
