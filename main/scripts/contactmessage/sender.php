<?php

class sendMessage extends general {
    
    function checkMessageId ($contact, $name, $db) {
        
        $query = $db -> prepare("SELECT id FROM conversationbinder WHERE contact = ? LIMIT 1");
        $query -> execute(array($contact));
        
        $result = $query -> fetch()[0];
        
        if ($result) {
            return $result;
        } else {
            $query = $db -> prepare("INSERT INTO conversationbinder(id, name, contact, dateadded, timeadded) VALUES (?, ?, ?, NOW(), NOW())");
            $query -> execute(array('', $name, $contact));
            
            if ($query) {
                return $db -> lastInsertId();
            }
        }
        
    }
    
    function sendMail ($name, $contact, $message, $db) {
        
        $messageId = $this -> checkMessageId ($contact, $name, $db);
        $currentTime = strtotime(date('H:i:s'));
        
        $query = $db -> prepare("INSERT INTO conversationmessage(id, sender, receiver, message, seen, dateadded, timeadded) VALUES(?, ?, ?, ?, ?, NOW(), ?)");
        
        $query -> execute(array("", $messageId, 'admin', $message, 0, $currentTime));
        
        if ($query) {
            echo '<div class="alert alert-success">Your message has been sent successfully.</div>';
        } else {
            echo '<div class="alert alert-danger">An error occured</div>';
        }
        
    }
    
    function __construct ($name, $contact, $message, $db) {
        
        if (!empty($name) && ctype_space($name) == false) {
            
            if (!empty($contact) && ctype_space($contact) == false) {
                
                if (!empty($message) && ctype_space($message) == false) {
                    
                    $this -> sendMail ($name, $contact, $message, $db);
                    
                } else {
                    echo '<div class="alert alert-info">Type your message</div>';
                }
                
            } else {
                echo '<div class="alert alert-info">Your email address or phone number is required</div>';
            }
            
        } else {
            echo '<div class="alert alert-info">Your names are required</div>';
        }
        
    }
    
}

if (isset($_POST["submitMessage"])) {
 
    $name = $_POST["names"];
    $contact = $_POST["contact"];
    $message = $_POST["message"];
    
    $sendMessage = new sendMessage ($name, $contact, $message, $db);
}

?>