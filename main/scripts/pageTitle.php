<?php

class pageInfoMeta extends general {
    
    function outputTitle ($text) {
        echo '<title>'.$text.'</title>';
    }
    
    function outputDescription ($text) {
        echo '<meta name="description" content="'.$text.'">';
    }
    
    function __pageTitle ($pageName, $getData, $db) {
        
        $pageName = strtolower($pageName);
        
        switch ($pageName) {
            
            case "/rest/main/pages/index.php":
                $this -> outputTitle ("Kingdom Covenant Ministries International -- Raising Kings To Build the Kingdom.");
                $this -> outputDescription("Welcome to Kingdom Covenant Ministries International, Rehoboth Christian Center. Our vision is to raise kings to build the kingdom of God.");
            break;
                
            case "/rest/main/pages/404.php":
                $this -> outputTitle("An error just occured but don't worry we're getting it fixed");
            break;
                
            case "/rest/main/pages/aboutdfr.php":
                $this -> outputTitle("Learn all you need to know about Daily Faith Recharge");
                $this -> outputDescription("Faith comes by hearing God's word. Through the Daily Faith Recharge, you will be energized, revitalized and inspired to face life's daily challenges with confidence and renewed hope making you live like a victor in Christ. Remain charged to take charge. Blessings.");
            break;
                
            case "/rest/main/pages/aboutus.php":
                $this -> outputTitle("Welcome to Kingdom Covenant Ministeries");
                $this -> outputDescription("All you need to know about the vision and purpose of Kingdom Covenant Ministries International");
            break;
                
            case "/rest/main/pages/contact.php":
                $this -> outputTitle("We would love to hear from you.");
                $this -> outputDescription("You can contact us by sending us a message or calling any of the numbers listed -- pastor@kcmi-rcc.com");
            break;
                
            case "/rest/main/pages/dfr.php":
                $this -> outputTitle("Daily Faith Recharge -- Build your faith through the word of God daily");
                $this -> outputDescription("The Gospel aimed at increasing your appetite for the word of God");
            break;
                
            case "/rest/main/pages/donate.php":
                $this -> outputTitle("Donate for the furtherance of the Gospel");
                $this -> outputDescription("Be a part of the furtherance of the Gospel. Your donations will go a long way in reaching the lives of people.");
            break;
                
            case "/rest/main/pages/eventpictures.php":
                $eventDetails = $this -> eventDetails ($getData, $db);
                $this -> outputTitle($eventDetails["eventtitle"]);
                $this -> outputDescription($eventDetails["writeup"]);
            break;
                
            case "/rest/main/pages/events.php":
                $year = getdate()["year"];
                $this -> outputTitle("Event listing for the year $year and previous years");
                $this -> outputDescription("Every year we hold series of events tailored towards uplifting souls. This pag carries the list of events from previous years and this year $year");
            break;
                
            case "/rest/main/pages/gmaplocation.php":
                $this -> outputTitle("Find us through our location.");
                $this -> outputDescription("Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Gym Port Harcourt, Rivers State, Nigeria.");
            break;
                
            case "/rest/main/pages/homecell.php":
                
                $this -> outputTitle("A place where we gather to fellowship in one accord");
                $this -> outputDescription("Find the closest home cell to you and be a part of the refreshing moments of fellowshiping");
            break;
                
            case "/rest/main/pages/maintestimony.php":
                $testimonyDetails = $this -> testimonyDetails($getData, $db);
                $this -> outputTitle($testimonyDetails["title"]." by ".$testimonyDetails["name"]);
                $this -> outputDescription($testimonyDetails["message"]);
            break;
                
            case "/rest/main/pages/memories.php":
                $this -> outputTitle("List of pictures from past events");
                $this -> outputDescription("When we hold events, we create memories that reminds us of where we are coming from.");
            break;
                
            case "/rest/main/pages/ourbelieve.php":
                $this -> outputTitle("What we believe and stand for");
                $this -> outputDescription("We believe in one God, the Creator of the Universe. We believe in the Trinity; Father, Son and the Holy Spirit. We believe in the Virgin Birth, death and resurrection of Jesus Christ; that Jesus rose in bodily form from the grave on the third day.");
            break;
                
            case "/rest/main/pages/sermons.php":
                $this -> outputTitle("The word of God that will change your life forever");
                $this -> outputDescription("The word of God as is sharper than any two edged sword has been shared bt the men of God to meet specific need(s) in your life");
            break;
            
            case "/rest/main/pages/sermonsingle.php":
                $sermonDetails = $this -> singleMessage($getData, $db);
                $this -> outputTitle($sermonDetails["title"]." by ".$sermonDetails["preacher"]);
                $this -> outputDescription($sermonDetails["message"]);
            break;
            
            case "/rest/main/pages/sharetestimony.php":
                $this -> outputTitle("Tell The world of the lord's Goodness");
                $this -> outputDescription("Share your testimonies as a means of increasing faith in the lives of others as they read it to get blessed");
            break;
            
            case "/rest/main/pages/singledfr.php":
                $dfrDetails = $this -> singleDfrDetails ($getData, $db);
                $this -> outputTitle($dfrDetails["title"]);
                $this -> outputDescription($dfrDetails["word"]);
            break;
                
            case "/rest/main/pages/singleevent.php":
                $eventDetails = $this -> eventDetails ($getData, $db);
                $this -> outputTitle($eventDetails["eventtitle"]);
                $this -> outputDescription($eventDetails["writeup"]);
            break;
                
            case "/rest/main/pages/testimonylist.php":
                $this -> outputTitle("Read testimonies of the goodness of the Lord");
                $this -> outputDescription("Testimonies shared by people to tell of the goodness of God in their lives");
            break;
                
            case "/rest/main/pages/wordforthemonth.php":
                $wofmonth = $this -> selectSingleWord ($getData, $db);
                $this -> outputTitle($wofmonth["word"]);
                $this -> outputDescription($wofmonth["writeup"]);
            break;
                
            case "/rest/main/pages/workinggroup.php":
                $this -> outputTitle("Working groups in Kingdom Covenant Ministries International");
                $this -> outputDescription("This are groups that take part in the areas of ministry that aid in the organisation of the church activities");
            break;
                
                
            default: 
                $this -> outputTitle ("Kingdom Covenant Ministries International -- Raising Kings To Build the Kingdom.");
                $this -> outputDescription("Welcome to Kingdom Covenant Ministries International, Rehoboth Christian Center. Our vision is to raise kings to build the kingdom of God.");
         
                
        }
        
    }
}

$callMeta = new pageInfoMeta ();

?>