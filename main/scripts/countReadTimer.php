<?php

require_once ('bootstrap.php');
require_once ('majorFunctionFile.php');


class setCounter extends general {
    
    function checkReadBefore($contentId, $contentType, $userIpAddress, $db) {
        
        $query = $db -> prepare("SELECT id FROM contentreadcheck WHERE address = ? AND contentid = ? AND contenttype = ? LIMIT 1");
        $query -> execute(array($userIpAddress, $contentId, $contentType));
        $result = $query -> fetch()[0];
        
        if ($result) {
            return false;
        } else return true;
        
    }
    
    function __construct ($contentId, $contentType, $userIpAddress, $db) {
        
        if ($contentId && $contentType) {
            
            // check if i have read it before
            $checkRead = $this -> checkReadBefore($contentId, $contentType, $userIpAddress, $db);
            if ($checkRead) {
                $query = $db -> prepare("INSERT INTO contentreadcheck(id, contentid, contenttype, address, dateadded, timeadded) VALUES (?, ?, ?, ?, NOW(), NOW())");
                $query -> execute(array("", $contentId, $contentType, $userIpAddress));
                if ($query) {
                    echo "This just got registered";
                } else echo "Wahala";
                
            } else echo "This has been read before";
            
        }
        
    }
    
}

if (isset($_POST["contentId"]) && isset($_POST["contentType"])) {
    
    $contentId = $_POST["contentId"];
    $contentType = $_POST["contentType"];
    $userIpAddress = $_SERVER["REMOTE_ADDR"];
    
    $countTimer = new setCounter($contentId, $contentType, $userIpAddress, $db);
    
}

?>