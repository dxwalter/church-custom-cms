<?php
require_once ('bootstrap.php');

class requestManager {
    
    function __construct ($request, $lastId, $db) {
        
        require_once ('majorFunctionFile.php');
        require_once ('sermons/mainSermonFile.php');
        require_once ('events/events.php');
        
        switch ($request) {
         
            case "moreSermon":
            require_once ('sermons/callMajorMessages.php');
            $callMessages = new callMultipleSermons($lastId, $db);
            break;
                
            case "moreDfr":
            require_once ('dfr/callDfrMajor.php');
            $callDfr = new callDfrMessageList ($lastId, $db);
            break;
            
            case "moreEventMemoryListing":
            require_once ('events/memoryListing.php');
            $callDfr = new callEventsList ($lastId, $db);
            break;
                
            default: echo '<div class="alert alert-info fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oops!</strong> An Error Occured</a>.</div>';
        }
        
    }
    
}

if (isset($_POST["lastId"]) && isset($_POST["actionType"])) {
    
    $lastId = $_POST["lastId"];
    $request = $_POST["actionType"];
    $callRequest = new requestManager($request, $lastId, $db);
}

?>