<?php

if (!isset($_POST["lastId"]) || $lastId == false) {
    require_once ('../scripts/majorFunctionFile.php');
    require_once ('../scripts/testimony/testimonyMainFile.php');   
}

class testimonyWithoutTag extends general {
    
    function messageTagDisplay ($id, $name, $title, $location, $tag, $message, $dateadded) {
        
        echo '
            <article class="single-post format-standard">
                <div class="title-row">
                    <h3><a href="mainTestimony.php?ijn='.$id.'">'.$title.'</a></h3>
                </div>
                <div class="meta-data">
                    <span><i class="fa fa-calendar"></i> '.$dateadded.' by <a href="#">'.$name.'</a>, '.$location.'</span> 
                </div>
                <div class="meta-data post-tags"><i class="fa fa-tags"></i> <a href="#">'.$tag.'</a></div>
                <div class="spacer-20"></div>
                <div class="post-content">'.$message.'
                    <p><a href="mainTestimony.php?ijn='.$id.'" class="btn btn-primary btn-sm">Continue Reading</a></p>
                </div>
                <div class="spacer-30"></div>
            </article>
        ';
        
    }
    
    function __construct ($lastId, $db) {
        
        if ($lastId) {
            $query = $db -> prepare("SELECT * FROM testimony WHERE status = ? AND id < ? ORDER BY id DESC LIMIT 7");
            $query -> execute(array(1, $lastId));
            
        } else {
            $query = $db -> prepare("SELECT * FROM testimony WHERE status = ? ORDER BY id DESC LIMIT 7");
            $query -> execute(array(1));
        }
        
        
        if ($query -> rowCount()) {
            
            $x = 0;
            
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                
                $id = $row -> id;
                $name = $row -> name;
                $title = $row -> title;
                $location = $row -> location;
                $tag = ucfirst(strtolower($row -> tag));
                $message = $this -> cutText($row -> message, 500);;
                $dateadded = $row -> dateadded;
                
                
                 $this -> messageTagDisplay($id, $name, $title, $location, $tag, $message, $dateadded);
            }
            
            if ($query -> rowCount() == 7) {
            
                echo '
                    <div id="moreTestimoniesDump'.$id.'">
                    <div class="spacer-30"></div>
                        <div class="col-md-push-6">
                            <nav aria-label="...">
                              <ul class="pager">
                                <li><button id="moreTestimonies" value="'.$id.'" data-tag-bool="false" data-tag="false" type="button" class="btn btn-default btn-transparent event-tickets event-register-button btn-lg">Load More Testimonies</button></li>
                              </ul>
                            </nav>
                        </div>
                    </div>
                ';  
                
            } else {
                echo '<div class="alert alert-info fade in"> That\'s all the testimonies for now. </div>';
            }
            
        } else {
            
            echo '<div class="alert alert-info fade in"> <strong>Oops!</strong> No result was found for now. </div>';
            
        }
        
    }
    
}

?>