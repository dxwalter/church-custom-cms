<?php

require_once ('../bootstrap.php');

class callMore {
    
    function __construct ($lastId, $tag, $tagCheck, $db) {
        
        require_once ('../majorFunctionFile.php');
        require_once ('testimonyMainFile.php');
        
        if ($tagCheck == "true") {
            require_once ('testimonyRetrievalbyTag.php');
            $callMore = new testimonyByTag($tag, $lastId, $db);
        } else if ($tagCheck == "false" && $tag == "false"){
            // call script without tag
            require_once ('testimonyRetrieval.php');
            $callTestimonies = new testimonyWithoutTag($lastId, $db);
        }
        
    }
}


if (isset($_POST["lastId"]) && isset($_POST["tag"]) && isset($_POST["tagCheck"])) {
    
    $lastId = $_POST["lastId"];
    $tag = $_POST["tag"];
    $tagCheck = $_POST["tagCheck"];
    
    $callMore = new callMore($lastId, $tag, $tagCheck, $db);
    
}

?>