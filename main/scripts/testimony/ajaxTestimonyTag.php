<?php
require_once ('../bootstrap.php');
require_once ('testimonyMainFile.php');

class callTag extends testimony{
    
    function __construct ($tag, $db) {
        
        if (empty($tag) || ctype_space($tag)) {
            
            echo '<p style="color:#d9534f;"><small><b>Type a testimony tag</b></small></p>';
        } else {
            
            $query = $db -> prepare("SELECT DISTINCT(tag) FROM testimony WHERE tag LIKE :searchOne ORDER BY id DESC");
            $query -> execute(array('searchOne' => '%'.$tag.'%'));
            
            if ($query -> rowCount()) {
                
                echo '<div class="list-group">';
                
                while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                    $retrievedTag = $row -> tag;
                    $tagCount = $this -> tagCount($retrievedTag, $db);
                    
                    echo '
                    <a href="testimonyList.php?tag='.$retrievedTag.'" class="list-group-item">'.ucfirst($retrievedTag).'
                        <span class="badge">'.$tagCount.'</span>
                    </a>';
                }
                
                echo '</div>';
                
            } else {
                echo '<p><small><b>No result was found for <span style="color:#d9534f;">'.strtoupper($tag).'</span></b></p>';
            }
            
        }
        
    }
    
}

if (isset($_POST["tag"])) {
    $tag = $_POST["tag"];
    $tagCall = new callTag($tag, $db);
}

?>