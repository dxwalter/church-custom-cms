<?php

class shareTestimony {
    
    
    function insertTestimony($names, $contact, $location, $title, $message, $db) {
        
        $today = getdate();
        $month = $today["month"];// month
        $day = $today["mday"];// day
        $year = $today["year"];// year
//        $dayOfWeek = $today["weekday"];
        
        if ($day == "1" || $day == "21" || $day == "31") {
            $day = $day."st";
        } else if ($day == "2" || $day == "22") {
            $day = $day."nd";
        } else if ($day == "3" || $day == "23") {
            $day = $day."rd";
        } else {
            $day = $day."th";
        }
        
        $dateFormat = $day." ".$month.", ".$year;
        
        $query = $db -> prepare("INSERT INTO testimony (id, name, title, contact, location, tag, message, status, validator, timeadded, dateadded) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?)");
        $query -> execute(array("", $names, $title, $contact, $location, "", $message, 0, "", $dateFormat));
        
        if ($query) {
            echo '<div class="alert alert-success fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hurray!</strong> You\'ve successfully shared your testimony. Thank you. </div>';
            
        } else {
            echo '<div class="alert alert-danger fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Snap!</strong> An error occured</div>';
        }
        
    }
    
    
    function __construct ($names, $contact, $location, $title, $message, $db) {
        
        if (empty($names) || ctype_space($names) || strlen($names) < 4) {
            
            echo '<div class="alert alert-info fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hello!</strong> Tell us what your name is and your name must be greater than 4 characters. </div>';
            
        } else {
            if (empty($contact) || ctype_space($contact)) {
                
                echo '<div class="alert alert-info fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hello!</strong> Your email address or phone number is also needed</div>';
                
            } else {
                if (empty($location) || ctype_space($location)) {
                    
                    echo '<div class="alert alert-info fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hello!</strong> Tell us where  you\'re sharing your testimony from</div>';
                    
                } else {
                    if (empty($title) || ctype_space($title)) {
                        
                        echo '<div class="alert alert-info fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hello!</strong> Give your testimony a title</div>';
                        
                    } else {
                        if (empty($message) || ctype_space($message)) {
                            
                            echo '<div class="alert alert-info fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hello!</strong> Type your testimony in the big box below</div>';
                            
                        } else {
                            
                            $this -> insertTestimony($names, $contact, $location, $title, $message, $db);
                            
                        }
                    }
                }
            }
        }
        
    }
    
}

if (isset($_POST["shareTestimony"])) {
    $names = $_POST["names"];
    $contact = $_POST["contact"];
    $location = $_POST["location"];
    $title = $_POST["title"];
    $message = $_POST["message"];
    
    $shareTestimony = new shareTestimony ($names, $contact, $location, $title, $message, $db);
}

?>