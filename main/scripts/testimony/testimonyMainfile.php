<?php


class testimony extends general {
    
    public $veryRecentId;
    public $veryRecentTag;
    public $veryRecentTitle;
    public $veryRecentDate;
    public $veryRecentTestimony;
    public $veryRecentName;
    
    
    function nextMainTestimony($testimonyId, $db) {
        $query = $db -> prepare("SELECT id FROM testimony WHERE id > ? LIMIT 1");
        $query -> execute(array($testimonyId));
        if ($query -> rowCount()) {
            $newId = $query -> fetch()[0];
            echo '<li><a href="mainTestimony.php?ijn='.$newId.'">Next</a></li>';
        }
    }
    
    function previousMainTestimony($testimonyId, $db) {
        $query = $db -> prepare("SELECT id FROM testimony WHERE id < ? LIMIT 1");
        $query -> execute(array($testimonyId));
        if ($query -> rowCount()) {
            $newId = $query -> fetch()[0];
            echo '<li><a href="mainTestimony.php?ijn='.$newId.'">Previous</a></li>';
        }
    }
    
    function testimonyDetails($testimonyId, $db) {
        $query = $db -> prepare("SELECT * FROM testimony WHERE id = ? LIMIT 1");
        $query -> execute(array($testimonyId));
        
        if ($query -> rowCount()) {
            
            return $query -> fetch(PDO::FETCH_ASSOC);
            
        } else {
            header('location: 404.php');
        }
    }
    
    function tagCount($retrievedTag, $db) {
        $query = $db -> prepare("SELECT * FROM testimony WHERE tag = ? AND status = ?");
        $query -> execute(array($retrievedTag, 1));
        
        if ($query -> rowCount()) {
            return $query -> rowCount();
        } else return "0";
    }
    
    function testimonyCount ($db) {
        $query = $db -> prepare("SELECT DISTINCT(tag) FROM testimony ORDER BY id DESC");
        $query -> execute();
        
        $queryTwo = $db -> prepare("SELECT * FROM testimony");
        $queryTwo -> execute();
        
        if ($queryTwo -> rowCount() > 99) {
            echo '
                <div class="widget sidebar-widget widget_categories">
                    <h3 class="widgettitle">Testimony Tags</h3>
                        <ul>
            ';
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                $tag = $row -> tag;
                echo '<li><a href="testimonyList.php?tag='.$tag.'">'.ucfirst(strtolower($tag)).'</a> ('.$this -> tagCount($tag, $db).')</li>';
            }
            
            echo '</ul></div>';
        }
    }
    
    
    function indexTestimonyList ($id, $db) {
        $query = $db -> prepare("SELECT * FROM testimony WHERE id < ? AND status = ? ORDER BY id DESC LIMIT 3");
        $query -> execute(array($id, 1));
        if ($query -> rowCount()) {
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                
                $id = $row -> id;
                $name = $row -> name;
                $testimony = $this -> cutText ($row -> message, 75);
                $tag = $row -> tag;
                $date = $row -> dateadded;
                $title = $row -> title;
                
                echo '
                    <li>
                        <a href="mainTestimony.php?ijn='.$id.'"><strong class="post-title">'.$title.'</strong></a>
                        <div class="meta-data">by <a href="#">'.$name.'</a> on '.$date.' tagged <a href="#">'.$tag.'</a></div>
                        <p>'.$testimony.'</p>
                        <p><a href="mainTestimony.php?ijn='.$id.'" class="basic-link">Continue reading</a></p>
                    </li>
                ';
                
            }   
        }
    }
    
    function veryRecent ($db) {
        
        $query = $db -> prepare("SELECT * FROM testimony WHERE status = ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array(1));
        
        if ($query -> rowCount()) {
            
            while ($row = $query -> fetch(PDO::FETCH_ASSOC)) {
                
                $this -> veryRecentTag = $row["tag"];
                $this -> veryRecentName = $row["name"];
                $this -> veryRecentId = $row["id"];
                $this -> veryRecentTestimony = $this -> cutText ($row["message"], 100);
                $this -> veryRecentTitle = $row["title"];
                $this -> veryRecentDate = $row["dateadded"];
                
            }
            
        }
        
    }
    
    
    function __construct ($db) {
        
    }
    
    
}

$mainTestimony = new testimony($db);

?>