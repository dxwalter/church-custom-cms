<?php

if (isset($_POST["email"])) {
    require_once ('../scripts/dbConfig.php');
    require_once ('../scripts/majorFunctionFile.php');
}

class unsubscribe extends general{
    
    
    function unsubscribeService($email, $type, $db) {
        
        $query = $db -> prepare("UPDATE subscription SET $type = ? WHERE email = ? LIMIT 1");
        $query -> execute(array(0, $email));
        
        if ($query) {
            echo '<div class="alert alert-success">Unsubscription successful</div>';
        } else {
            echo '<div class="alert alert-info">An error occurred</div>';
        }
        
    }
    
    public function subscribeDetails ($email, $db) {
        
        $query = $db -> prepare("SELECT * FROM subscription WHERE email = ? LIMIT 1");
        $query -> execute(array($email));
        
        
        if ($query -> rowCount()) {
            
            while ($row = $query -> fetch(PDO::FETCH_ASSOC)) {
                
                $email = $row["email"];
                $dfr = $row["dfr"];
                $sermon = $row["sermon"];
                $newsletter = $row["newsletter"];
                
                
                if ($dfr == true) {
                    echo '
                        <li class="event-list-item event-dynamic grid-item prayers celebrations isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                            <div class="event-list-item-info">
                                <div class="lined-info">
                                    <h3><a href="#">Daily Faith Recharge</a></h3>
                                </div>

                            </div>
                            <div class="event-list-item-actions" id="dfrunsub">
                                <button class="btn btn-default btn-transparent event-tickets event-register-button" value="'.$email.'" id="unsubscribe" data-unsub-type="dfr">Unsubscribe</button>

                            </div>
                        </li>
                    ';
                } else {
                    
                    echo '
                        <li class="event-list-item event-dynamic grid-item prayers celebrations isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                            <div class="event-list-item-info">
                                <div class="lined-info">
                                    <h3><a href="singleEvent.php?ijn=1">Daily Faith Recharge</a></h3>
                                </div>

                            </div>
                            <div class="event-list-item-actions">
                                <a href="aboutDfr.php#dfrSubscription" class="btn btn-default btn-transparent event-tickets event-register-button">Subscribe</a>

                            </div>
                        </li>
                    ';
                }
                
                if ($sermon == true) {
                    echo '
                        <li class="event-list-item event-dynamic grid-item prayers celebrations isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                            <div class="event-list-item-info">
                                <div class="lined-info">
                                    <h3><a href="#">Sermons</a></h3>
                                </div>

                            </div>
                            <div class="event-list-item-actions" id="sermonunsub">
                                <button class="btn btn-default btn-transparent event-tickets event-register-button" value="'.$email.'" id="unsubscribe" data-unsub-type="sermon">Unsubscribe</button>

                            </div>
                        </li>
                    ';
                } else {
                    
                    echo '
                        <li class="event-list-item event-dynamic grid-item prayers celebrations isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                            <div class="event-list-item-info">
                                <div class="lined-info">
                                    <h3><a href="#">Sermons</a></h3>
                                </div>

                            </div>
                            <div class="event-list-item-actions">
                                <a href="sermonSingle.php?in=1" class="btn btn-default btn-transparent event-tickets event-register-button">Subscribe</a>

                            </div>
                        </li>
                    ';
                }
                
                if ($newsletter == true) {
                    echo '
                        <li class="event-list-item event-dynamic grid-item prayers celebrations isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                            <div class="event-list-item-info">
                                <div class="lined-info">
                                    <h3><a href="#">Newsletter</a></h3>
                                </div>

                            </div>
                            <div class="event-list-item-actions" id="newsletterunsub">
                                <button class="btn btn-default btn-transparent event-tickets event-register-button" value="'.$email.'" id="unsubscribe" data-unsub-type="newsletter">Unsubscribe</button>

                            </div>
                        </li>
                    ';
                } else {
                    
                    echo '
                        <li class="event-list-item event-dynamic grid-item prayers celebrations isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                            <div class="event-list-item-info">
                                <div class="lined-info">
                                    <h3><a href="singleEvent.php?ijn=1">Newsletter</a></h3>
                                </div>

                            </div>
                            <div class="event-list-item-actions">
                                <a href="#newsletterEmailValue" class="btn btn-default btn-transparent event-tickets event-register-button">Subscribe</a>

                            </div>
                        </li>
                    ';
                }
                
            }
            
        } else {
            header('location: index.php');
        }
        
    }
    
}

if (isset($_POST["email"])) {
    $email = $_POST["email"];
    $type = $_POST["pType"];
    $callSub = new unsubscribe();   
    $callSub -> unsubscribeService($email, $type, $db);
} else {
    $callSub = new unsubscribe();   
}

?>