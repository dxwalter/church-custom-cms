<?php

class callEventPhotos extends events {
    
    public $previousMoveId;
    public $nextMoveId;
    public $eventId;
    public $db;
    
    
    function previousButton () {
        $query = $this -> db -> prepare("SELECT id FROM eventgallery WHERE eventid = ? AND id > ? LIMIT 1");
        $query -> execute(array($this -> eventId, $this -> previousMoveId));
        $result = $query -> fetch()[0];
        
        if ($result) {
            return '<li><a href="eventPictures.php?ijn='.$this -> eventId.'&move=prev&moveId='.$this -> previousMoveId.'">Previous</a></li>';
        }
    }
    
    function nextButton () {
        $query = $this -> db -> prepare("SELECT id FROM eventgallery WHERE eventid = ? AND id < ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array($this -> eventId, $this -> nextMoveId));
        $result = $query -> fetch()[0];
        
        if ($result) {
            return '<li><a href="eventPictures.php?ijn='.$this -> eventId.'&move=next&moveId='.$this -> nextMoveId.'">Next</a></li>';
        }
    }
    
    function __construct ($id, $type, $lastId, $db) {
        
        $this -> eventId = $id;
        $this -> db = $db;
        
        
        if ($type == 'none') {
            $query = $db -> prepare("SELECT * FROM eventgallery WHERE eventid = ? ORDER BY id DESC LIMIT 6");
            $query -> execute(array($id));
            $x = 0;
        } else if ($type == 'prev') {
            $query = $db -> prepare("SELECT * FROM eventgallery WHERE eventid = ?  AND id >= ? ORDER BY id ASC LIMIT 6");
            $query -> execute(array($id, $lastId));
            $x = 0;
        } else if ($type == 'next') {
            $query = $db -> prepare("SELECT * FROM eventgallery WHERE eventid = ?  AND id < ? ORDER BY id DESC LIMIT 6");
            $query -> execute(array($id, $lastId));
            $x = 1;
        }
        
        
        if ($query == false) {
            echo '<li class="col-md-12 col-sm-12 grid-item format-image"><div class="alert alert-standard fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oh snap!</strong> Pictures are yet to be uploaded for this event. </div></li>';
        } else {
            
            $eventDetails = $this -> eventDetails ($id, $db);
            
            if ($query -> rowCount()) {

                while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                    $photoId = $row -> id;
                    $imagePath = $row -> imagepath;
                    $newpath = "../../Photouploads/gallery/".$imagePath;
                    
                    if (($type == 'none' || $type == 'prev') && $x == 0) {
                        $this -> previousMoveId = $photoId;
                        $x = $x + 1;
                    }

                    echo '
                        <li class="col-md-4 col-sm-4 grid-item format-image">
                            <div class="grid-item-inner"> <a href="'.$newpath.'" data-rel="prettyPhoto" class="media-box"> <img src="'.$newpath.'" alt=""> </a> </div>
                        </li>
                    ';
                }
                
                if ($type == 'none') {
                    $this -> nextMoveId = $photoId;
                    
                } else if ($type == 'prev') {
                    $this -> nextMoveId = $lastId;

                } else if ($type == 'next') {
                    $this -> previousMoveId = $lastId;
                    $this -> nextMoveId = $photoId;
                }

                if ($query -> rowCount() > 5) {

                    echo '
                        
                            <li class=" grid-item format-image" style="width:100%;height:auto;">
                                <div class="spacer-50"></div>
                                <div class="col-md-push-3">
                                    <nav aria-label="...">
                                      <ul class="pager">
                                        '.$this -> previousButton().'
                                        '.$this -> nextButton().'
                                      </ul>
                                    </nav>
                                </div>
                            </li>
                        
                    ';

                } else {
                    echo '<li class="col-md-12 col-sm-12 grid-item format-image"><div class="alert alert-standard fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oh snap!</strong> That\'s all we have for <strong>'.$eventDetails["eventtitle"].'</strong></div></li>'; 
                }

            } else {
                echo '<li class="col-md-12 col-sm-12 grid-item format-image"><div class="alert alert-standard fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oh snap!</strong> That\'s all we have for <strong>'.$eventDetails["eventtitle"].'</strong></div></li>';
            }
            
        }
        
    }
    
}

?>