<?php

require_once ('../scripts/majorFunctionFile.php');
require_once ('../scripts/events/events.php');

class eventListing extends events {
    
    public $nextEventId;
    public $previousEventId;
    
    function previousEventCheck ($previousListingId, $db) {
        $query = $db -> prepare("SELECT id FROM event WHERE id < ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array($previousListingId));
        
        if ($query) {
            $previousId = $query -> fetch()[0];
            if ($previousId) {
                return '<li><a href="events.php?prev='.$previousListingId.'">Previous</a></li>';
            }
        }
    }
    
    function nextEventCheck ($nextListingId, $db) {
        $query = $db -> prepare("SELECT id FROM event WHERE id > ? LIMIT 1");
        $query -> execute(array($nextListingId));
        
        if ($query) {
            $nextId = $query -> fetch()[0];
            if ($nextId) {
                return '<li><a href="events.php?next='.$nextListingId.'">Next</a></li>';
            }
        }
    }
    
    function eventListingPagination ($nextButton, $previousButton, $db) { 
        //we're using the next upcoming event id to get the previous id of the event before the next upcoming event
         echo '
            <div class="col-md-push-6">
                <nav aria-label="...">
                  <ul class="pager">
                    '.$this -> previousEventCheck ($previousButton, $db).'
                    '.$this -> nextEventCheck ($nextButton, $db).'
                  </ul>
                </nav>
            </div> 
        ';   
    }
    
    function  listingUI ($id, $eventTitle, $day, $month, $year, $eventTime, $nextevent) {
        
        echo '
            <li class="event-list-item event-dynamic grid-item prayers celebrations">
                <div class="event-list-item-date">
                    <span class="event-date">
                        <span class="event-day">'.$day.'</span>
                        <span class="event-month">'.$month.', '.$year.'</span>
                    </span>
                </div>
                <div class="event-list-item-info">
                    <div class="lined-info">
                        <h3><a href="singleEvent.php?ijn='.$id.'">'.$eventTitle.'</a></h3>
                    </div>
                    <div class="lined-info">
                        <span class="meta-data"><i class="fa fa-clock-o"></i> Event starts at <span class="event-time">'.$eventTime.'</span></span>
                    </div>
                    <div class="lined-info event-location">
                        <span class="meta-data"><i class="fa fa-map-marker"></i> <span class="event-location-address"></span><a href="contact.php">
                         Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Gym Port Harcourt, Rivers State, Nigeria </a>
                        </span>
                    </div>
                </div>
                <div class="event-list-item-actions">';
                    if ($nextevent) {
                        echo '<a href="#" class="btn btn-default btn-transparent event-tickets event-register-button">Upcoming Event</a>';
                    }
        
        
                echo  '<ul class="action-buttons">
                        <li title="Share event">
                        <a href="http://www.kcmi-rcc.com/singleEvent.php?ijn='.$id.'" data-trigger="focus" data-placement="top" data-content="" data-toggle="popover" data-original-title="Share Event" class="event-share-link event-title">
                        <i class="icon-share"></i>
                        </a>
                        </li>
                        <li title="Get directions" class="hidden-xs"><a href="gmapLocation.php" class="cover-overlay-trigger event-direction-link"><i class="icon-compass"></i></a></li>
                        <li title="Contact event manager"><a href="contact.php" data-toggle="modal" data-target="#Econtact" class="event-contact-link"><i class="icon-mail"></i></a></li>
                    </ul>
                </div>
            </li>
        ';
        
    }
    
    function previousQuery ($lastId, $db) {
        
    }
    
    function __construct ($lastId, $direction, $db) {
        
        if ($direction == 'prev') {
            $query = $db -> prepare("SELECT * FROM event WHERE id < ? AND status = ? ORDER BY id DESC LIMIT 3");
            $query -> execute(array($lastId, 1));   
            
        } else if ($direction == 'next') {
            
            $query = $db -> prepare("SELECT * FROM event WHERE id > ? AND status = ? ORDER BY id ASC LIMIT 3");
            $query -> execute(array($lastId, 1));
            
        } else if ($direction == 'none') {
            $queryCurrentId = $db -> prepare("SELECT id FROM event WHERE status = ? AND nextevent = ? LIMIT 1");
            $queryCurrentId -> execute(array(1, 1));
            
            if ($queryCurrentId) {
                $currentEventId = $queryCurrentId -> fetch()[0];
            }
//            $this -> nextEventId = $currentEventId;
            
            $query = $db -> prepare("SELECT * FROM event WHERE status = ? AND id >= ? ORDER BY id ASC LIMIT 3");
            $query -> execute(array(1, $currentEventId));
            
        } else {
            header('location: 404.php');
        }
        
        
        if ($query) {
        
            if ($query -> rowCount()) {
                
                $x = 0;
                
                while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                    $id = $row -> id;
                    $eventTitle = $row -> eventtitle;
                    $day = $row -> day;
                    $month = $row -> month;
                    $year = $row -> year;
                    $eventTime = $row -> eventTime;
                    $nextevent = $row -> nextevent;
                    
                    $this -> listingUI ($id, $eventTitle, $day, $month, $year, $eventTime, $nextevent);
                    
                }
                
                if ($direction == "prev") {
                    $this -> previousEventId = $id;
                    $this -> nextEventId = $lastId - 1;
                } else if ($direction == "next") {
                    $this -> previousEventId = $lastId;
                    $this -> nextEventId = $id;
                } else if ($direction == "none") {
                    $this -> previousEventId = $currentEventId;
                    $this -> nextEventId = $id;
                }
                
                
                
                if ($query -> rowCount() == 3) {
                    
                    
                    
                } else {
                    echo '<li class="event-list-item event-dynamic grid-item prayers celebrations"><div class="alert alert-info fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oops!</strong> These are all the events we have for now.</div></li>';
                }
                
            } else {
                echo '<li class="event-list-item event-dynamic grid-item prayers celebrations"><div class="alert alert-info fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oops!</strong> These are all the events we have for now.</div></li>';
            }
            
        } else {
            echo '<div class="alert alert-info fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oops!</strong> An error occured. Please refresh this page.</div>';
        }
    }
    
}

?>