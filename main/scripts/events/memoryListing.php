<?php

class callEventsList extends events {
    public $outputCount;
    public $bufferOne;
    public $bufferTwo;
    
    function outputData($eventId, $eventTitle, $ImageName) {
        
        return '
            <a href="eventPictures.php?ijn='.$eventId.'" target="_blank">
                <div class="col-md-4 col-sm-4">
                    <div class="grid-item staff-item format-standard">
                        <div class="grid-item-inner">
                            <img src="../../Photouploads/events/'.$ImageName.'" alt="">
                            <div class="grid-content">
                                <h3>'.$eventTitle.'</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        ';
    }
    
    function __construct ($lastId, $db) {
        
        if ($lastId) {
            $query = $db -> prepare("SELECT * FROM event WHERE id < ? ORDER BY id DESC LIMIT 6");
            $query -> execute(array($lastId));
        } else {
            $query = $db -> prepare("SELECT * FROM event ORDER BY id DESC LIMIT 6");
            $query -> execute();
        }
        
        if ($query -> rowCount() > 0) {
            
            $x = 0;
            
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                $eventId = $row -> id;
                $eventTitle = $row -> eventtitle;
                $ImageName = $row -> image;
                
                $countImage = $this -> eventImageCount($eventId, $db);
                if ($countImage) {
                    $this -> outputCount = $this -> outputCount + 1;
                    $outPut = $this -> outputData($eventId, $eventTitle, $ImageName);
                        
                    if ($x < 3) {
                        $this -> bufferOne .= $outPut;
                        $x = $x + 1;
                    } else if ($x >= 3) {
                        $this -> bufferTwo .= $outPut;
                        $x = $x + 1;
                    }   
                }
            }
            
            $rowOne = '<div class="row">'.$this -> bufferOne.'</div>';
            $rowTwo = '<div class="row">'.$this -> bufferTwo.'</div>';
            
            if ($query -> rowCount() > 5 && $this -> outputCount < 6) {
                $lastId = $eventId;
                $callImage = new callEventsList($lastId, $db);
            } else {
                echo $rowOne.$rowTwo;
                
                echo '
                    <div id="moreMessage'.$eventId.'">
                    <div class="spacer-50"></div>
                        <div class="col-md-push-6">
                            <nav aria-label="...">
                              <ul class="pager">
                                <li><button class="btn btn-default btn-transparent event-tickets event-register-button" type="button" value="'.$eventId.'" id="morePulpitSermons" data-request="moreEventMemoryListing">Load More Events</button></li>
                              </ul>
                            </nav>
                        </div>
                    </div>
                ';
            }
            
        } else {
            echo '<div class="alert alert-standard fade in"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oh snap!</strong> That\'s all we have for now Or no event has been uploaded. </div>';
        }
    }
}

?>