<?php

class events extends general {
    
    public $sliderEventId;
    public $sliderEventImage;
    public $sliderEventTitle;
    public $sliderEventWriteUp;
    public $sliderEventDay;
    public $sliderEventMonth;
    public $sliderEventYear;
    public $sliderEventTime;
    
    function eventTimeDate ($day, $month, $year) {
        $monthArrange = array(
            'jan' => "January",
            'feb' => "Febuary",
            'march' => "March",
            'april' => "April",
            'may' => "May",
            'june' => "June",
            'july' => "July",
            'aug' => "August",
            'sept' => "September",
            'oct' => "October",
            'nov' => "November",
            'dec' => "December"
        );
        
        if ($day == "1" || $day == "21" || $day == "31") {
            $day = $day."st";
        } else if ($day == "2" || $day == "22") {
            $day = $day."nd";
        } else if ($day == "3" || $day == "23") {
            $day = $day."rd";
        } else {
            $day = $day."th";
        }
        
        $dateFormat = $day." ".$monthArrange["$month"].", ".$year;
        return $dateFormat;
    }
    
    function eventImageCount($eventId, $db) {
        $query = $db -> prepare("SELECT * FROM eventgallery WHERE eventid = ?");
        $query -> execute(array($eventId));
        
        $count = $query -> rowCount();
        if ($count) {
            return $count;
        } else return false;
    }
    
    function prevEvent ($id, $db) {
        $query = $db -> prepare("SELECT id FROM event WHERE id < ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array($id));
        $result = $query -> fetch()[0];
        if ($result) {
            return '<li class="previous"><a href="singleEvent.php?ijn='.$result.'"><span aria-hidden="true">&larr;</span> Previous</a></li>';
        }
    }
    
    
    function nextEvent ($id, $db) {
        $query = $db -> prepare("SELECT id FROM event WHERE id > ? ORDER BY id ASC LIMIT 1");
        $query -> execute(array($id));
        $result = $query -> fetch()[0];
        if ($result) {
            return '<li class="next"><a href="singleEvent.php?ijn='.$result.'">Next <span aria-hidden="true">&rarr;</span></a></li>';
        }
    }
    
    
    function indexEventList($recentId, $db) {
        
        $query = $db -> prepare("SELECT * FROM event WHERE status = ? AND id > ? ORDER BY id ASC LIMIT 3");
        $query -> execute(array(1, $recentId));
        
        if ($query -> rowCount()) {
            
            while ($row = $query -> fetch(PDO::FETCH_OBJ)) {
                $eventTitle = $row -> eventtitle;
                $eventDay = $row -> day;
                $eventMonth = $row -> month;
                $eventYear = $row -> year;
                $eventId = $row -> id;
                $eventTime = $row -> eventTime;
                
                echo '<div class="event-list-item event-dynamic">';
                
                    echo '
                        <div class="event-list-item-date">
                            <span class="event-date">
                                <span class="event-day">'.$eventDay.'</span><span class="event-month">'.$eventMonth.', '.$eventYear.'</span>
                            </span>
                        </div>
                    ';
                    
                    echo '<div class="event-list-item-info">';
                
                        echo '
                            <div class="lined-info">
                                <h4><a href="singleEvent.php?ijn='.$eventId.'" class="event-title">'.$eventTitle.'</a></h4>
                            </div>
                        ';
                
                        echo '
                            <div class="lined-info">
                                <span class="meta-data"><i class="fa fa-clock-o"></i><!-- Sunday, --><span class="event-time">'.$eventTime.'</span></span>
                            </div>
                        ';
                
                        echo '
                            <div class="lined-info event-location">
                                <span class="meta-data"><i class="fa fa-map-marker"></i> <span class="event-location-address">KCMI, Port Harcourt Nigeria</span></span>
                            </div>
                        ';
                        
                    echo '</div>';
                
                    echo '
                        <div class="event-list-item-actions">
                            <a href="/singleEvent.php?ijn='.$eventId.'" class="btn btn-default btn-transparent event-tickets event-register-button">Read More</a>
                            <!-- This is the part that holds share buttons -->
                        </div>
                    ';
                
                echo '</div>';
            }
            
        }
        
    }
    
    
    function sliderEvent ($db) {
        
        $query = $db -> prepare("SELECT * FROM event WHERE status = ? AND nextEvent = ? ORDER BY id DESC LIMIT 1");
        $query -> execute(array(1, 1));
        
        if ($query -> rowCount()) {
            
            while ($row = $query -> fetch(PDO::FETCH_ASSOC)) {
                
                $length = 200;
                $this -> sliderEventId = $row["id"];
                $this -> sliderEventTitle = $row["eventtitle"];
                $this -> sliderEventWriteUp = $this -> cutText ($row["writeup"], $length);
                $this -> sliderEventImage = '../../Photouploads/events/'.$row["image"];
                $this -> sliderEventDay = $row["day"];
                $this -> sliderEventMonth = $row["month"];
                $this -> sliderEventYear = $row["year"];
                $this -> sliderEventTime = $row["eventTime"];
            }
        }
        
    }
    
    function singleEventDetails ($id, $db) {
        $query = $db -> prepare("SELECT * FROM event WHERE id = ? LIMIT 1");
        $query -> execute(array($id));
        if ($query) {
            if ($query -> rowCount()) {
                return $query -> fetch(PDO::FETCH_ASSOC);
            } else header('location: 404.php');
        } else header('location: 404.php');
    }
    
    function __construct ($db) {
        
        
    }
    
}

$events = new events($db);

?>