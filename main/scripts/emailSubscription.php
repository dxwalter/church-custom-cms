<?php

require_once ('bootstrap.php');
require_once ('majorFunctionFile.php');

class subscribe extends general {
    
    function subscriptionFailure () {
        echo '<div class="alert alert-info"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oops!</strong> An error occured</div>';
    }
    
    function subscriptionSuccess () {
        echo '<div class="alert alert-success"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hurray!</strong> Subscription successful</div>';
    }
    
    function insertEmailDb ($email, $db, $task) {
        if ($task == "sermon") {
            $query = $db -> prepare("INSERT INTO subscription (id, email, sermon) VALUES(?, ?, ?)");
        } else if ($task == "dfr") {
            $query = $db -> prepare("INSERT INTO subscription (id, email, dfr) VALUES(?, ?, ?)");
        } else if ($task == "newsletter") {
            $query = $db -> prepare("INSERT INTO subscription (id, email, newsletter) VALUES(?, ?, ?)");
        }
        
        $query -> execute(array("", $email, 1));
        if ($query) {
            $this -> subscriptionSuccess ();
        } else $this -> subscriptionFailure (); 
    }
    
    function updateEmailDb ($emailId, $db, $task)  {
        $query = $db -> prepare("UPDATE subscription SET $task = ? WHERE id = ? LIMIT 1");
        $query -> execute(array (1, $emailId));
        if ($query) {
            $this -> subscriptionSuccess ();
        } else $this -> subscriptionFailure ();
    }
    
    function emailCheck ($email, $db) {
        
        $query = $db -> prepare("SELECT id FROM subscription WHERE email = ? LIMIT 1");
        $query -> execute(array($email));
        
        $result = $query -> fetch()[0];
        
        if ($query -> rowCount()) {
            return $result;
        } else return false;
        
    }
    
    function sermonSubscription ($email, $db) {
        
        $emailId = $this -> emailCheck($email, $db);
        if ($emailId) {
            // update
            $this -> updateEmailDb ($emailId, $db, 'sermon');
        } else {
            $this -> insertEmailDb ($email, $db, 'sermon');
        }
        
    }
    
    function newsLetterSubscription ($email, $db) {
        $emailId = $this -> emailCheck($email, $db);
        if ($emailId) {
            //  update
            $this -> updateEmailDb ($emailId, $db, 'newsletter');
        } else {
            $this -> insertEmailDb ($email, $db, 'newsletter');
        }
    }

    function dfrSubscription($email, $db) {
        $emailId = $this -> emailCheck($email, $db);
        if ($emailId) {
            //update
            $this -> updateEmailDb($emailId, $db, 'dfr');
        } else {
            $this -> insertEmailDb($email, $db, 'dfr');
        }
    }
    
    function __construct ($email, $action, $db) {
        
        if ($action == "sermonSubscription") {
            
            $this -> sermonSubscription ($email, $db);
        } else if ($action == "dfrSubscription") {
            $this -> dfrSubscription($email, $db);
        } else if ($action == "newsletterSubscription") {
            $this -> newsLetterSubscription ($email, $db);
        }
        
    }
    
}

if (isset($_POST["email"]) && isset($_POST["actionType"])) {
    $email = $_POST["email"];
    $action = $_POST["actionType"];
    
    if ($email == true && $email && strpos($email, '@') == true) {
        $subscribe = new subscribe($email, $action, $db);
    } else {
        echo '<b style="color:red">Enter a valid email address</b>';
    }
    
} else {
    echo '<div class="alert alert-info"> <a class="close" data-dismiss="alert" href="#">×</a> <strong>Oops!</strong> An error occured</div>';
}

?>