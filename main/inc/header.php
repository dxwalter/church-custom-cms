<?php require_once ('../scripts/bootstrap.php'); ?>
<?php require_once ('../scripts/pageTitle.php'); ?>
<!DOCTYPE HTML>
<html class="no-js">
<head>
<!-- Basic Page Needs ================================================== -->
<?php 
    $pageName = $_SERVER["SCRIPT_NAME"]; 
    if (isset($_GET["ijn"])) {
        $getData = $_GET["ijn"];
    } else {
        $getData = "";
    }
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php $callMeta -> __pageTitle($pageName, $getData, $db); ?>
<?php require_once ('../scripts/pageMeta/keywords.php'); ?>
<meta name="author" content="Kingdom Covenant Ministries International">
<!-- Mobile Specific Metas
  ================================================== -->
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="format-detection" content="telephone=yes">
<!-- CSS
  ================================================== -->
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css">
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link href="../vendor/prettyphoto/css/prettyPhoto.css" rel="stylesheet" type="text/css">
<link href="../vendor/mediaelement/mediaelementplayer.css" rel="stylesheet" type="text/css">
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
<link href="../css/custom.css" rel="stylesheet" type="text/css"><!-- CUSTOM STYLESHEET FOR STYLING -->
<!-- Color Style-->
<link class="alt" href="../colors/color1.css" rel="stylesheet" type="text/css">
<link href="../style-switcher/css/style-switcher.css" rel="stylesheet" type="text/css"> 
<!-- SCRIPTS
  ================================================== -->
<script src="../js/modernizr.js"></script><!-- Modernizr -->
</head>
<body class="home home-style2 footer-dark">
<!--[if lt IE 7]>
	<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
<![endif]-->
<div class="body">
    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
	<!-- Start Site Header -->
	<header class="site-header">
    	<div class="container for-navi">
        	<div class="site-logo">
            <h1>
                <a href="index.php">
                    <span class="logo-icon"><img src="../images/logo-home.png"></span>
                    <span class="logo-text">
                        <span> 
                            Kingdom Covenant 
                        </span>
                            <br>
                            Ministries International
                        </span>
                    <span class="logo-tagline"></span>
                </a>
            </h1>
            </div>
            <!-- Main Navigation -->
            <nav class="main-navigation" role="navigation">
                <ul class="sf-menu">
                    <li><a href="index.php">Home</a>  </li>
                    <li><a href="javascript:void(0)">The Word</a>
                    	<ul class="dropdown">
                            <li><a href="sermons.php">Sermons</a>
<!--
                                <ul class="dropdown">
                                    <li><a href="sermons-series.html">Video Sermons</a></li>
                                    <li><a href="sermons-list.html">Audio Sermons</a></li>
                                </ul>
-->
                            </li>
                            <li><a href="dfr.php">Daily Faith Recharge</a> </li>
                       	</ul>
                   	</li>
                    
                    <li><a href="javascript:void(0)">Testimonies</a>
                    	<ul class="dropdown">
                            <li><a href="shareTestimony.php">Share your testimony</a></li>
                            <li><a href="testimonyList.php">Read Testimonies</a> </li>
                       	</ul>
                   	</li>
                    
                    <li><a href="events.php">Events</a></li>
                    <li><a href="memories.php">Memories</a></li>
                    <li><a href="javascript:void(0)">About us</a>
                  		<ul class="dropdown">
                    		<li><a href="aboutUs.php">Overview</a></li>
<!--                    		<li><a href="newMember.php">New Here?</a></li>-->
                    		<li><a href="donate.php">Donate now</a></li>
                   		</ul>
                    </li>
                	<li><a href="contact.php">Contact Us</a></li>
                </ul>
            </nav>       
            <a href="#" class="visible-sm visible-xs" id="menu-toggle"><i class="fa fa-bars"></i></a>
    	</div>
	</header>