    <footer class="site-footer">
       	<div class="container">
    		<div class="site-footer-top">
            	<div class="row">
                	<div class="col-md-4 widget footer_widget text_widget">
                        <h4>About our church</h4>
                        <hr class="sm">
                        <p> In Kingdom Covenant Ministries International, Rehoboth Christian Center, Our vision is to raise kings to build the kingdom of God. <a href="ourBelieve.php">Read More...</a> 
                        </p>
                    </div>
                	<div class="col-md-4 widget footer_widget text_widget">
                    	<h4>Daily Faith Recharge</h4>
                        <hr class="sm">
                      	<p> 
                            To fan the flames of our spirit through audio recorded and inspired word of God in nugget form. <a href="aboutDfr.php">Read More...</a> 
                        </p>
            		</div>
                    
                	<div class="col-md-4 widget footer_widget newsletter_widget">
                        <h4>News Letter Subscription</h4>
                        <hr class="sm">
                        <p>Subscribe to our monthly newsletter in order to receive the latest new &amp; articles. We promise we won't spam your inbox!</p>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Email Address" id="newsletterEmailValue" autocomplete="off">
                            <span class="input-group-btn">
                            <button id="subscribe" data-sub-type="newsletterSubscription" class="btn btn-primary" type="button" data-formValue="newsletterEmailValue">Subscribe!</button>
                            </span>
                        </div>
                        <div id="newsletterSubscription"></div>
            		</div>
                    
                    <div class="col-md-4 widget footer_widget newsletter_widget">
                        <h4>Like Our Facebook Page</h4>
                        <hr class="sm">
                        <p><div class="fb-like" data-href="https://www.facebook.com/kcmiworldwide" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="true"></div></p>
            		</div>
                
                
                    <div class="col-md-4 widget footer_widget newsletter_widget">
                        <h4>Follow Us On twitter</h4>
                        <hr class="sm">
                        <p><a href="https://twitter.com/kcmiworldwide?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @kcmiworldwide</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></p>
                        <p><a href="https://twitter.com/intent/tweet?screen_name=kcmiworldwide&ref_src=twsrc%5Etfw" class="twitter-mention-button" data-show-count="false">Tweet to @kcmiworldwide</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></p>
            		</div>
               	</div>
            
        		<!-- Quick Info -->
        		<div class="quick-info">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <h4><i class="fa fa-clock-o"></i> Service Times</h4>
                            <p>Sundays @ 8:00 am<br></p>
                            <p>Thursdays @ 5:45 pm<br></p>
                            <P><a href="gmapLocation.php">View Venue</a></P>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h4><i class="fa fa-map-marker"></i> Our Location</h4>
                            <p>Kingdom Covenant Ministries International
                                Okwuruola Wonodi Close
                                Off Rumuola/Stadium Link Road,
                                By Charlies Fitness Center
                                Port Harcourt, Rivers State, Nigeria
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h4><i class="fa fa-envelope"></i> Contact Info</h4>
                            <p>
                                <a href="tel:+2348084583102">+234 808 458 3102</a>
                                <br>
                                <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>
                                <br>
                                <a href="contact.php">Send us a message</a>
                                </p>
                        </div>
                        
                    </div>
            	</div>
        		<div class="site-footer-bottom">
            		<div class="row">
                		<div class="col-md-6 col-sm-6 copyrights-coll">
        					&copy; 2018 KCMI. All Rights Reserved
            			</div>
                		<div class="col-md-6 col-sm-6 copyrights-colr">
        					<nav class="footer-nav" role="navigation">
                        		<ul>
                            		<li><a href="index.php">Home</a></li>
                                    <li><a href="aboutUs.php">About Us</a></li>
                            		<li><a href="donate.php">Donate now</a></li>
                            		<li><a href="contact.php">Contact</a></li>
                            	</ul>
                        	</nav>
            			</div>
                   	</div>
               	</div>
           	</div>
        </div>
    </footer>
    <!-- End site footer -->
  	<a id="back-to-top"><i class="fa fa-angle-double-up"></i></a>  
</div>
<script src="../js/jquery-2.0.0.min.js"></script> <!-- Jquery Library Call -->
<script src="../js/ajaxFile.js"></script>
<script src="../vendor/prettyphoto/js/prettyphoto.js"></script> <!-- PrettyPhoto Plugin -->
<script src="../js/helper-plugins.js"></script> <!-- Helper Plugins -->
<script src="../js/bootstrap.js"></script> <!-- UI -->
<script src="../js/init.js"></script>
<script src="../js/home.js"></script>
<script src="../vendor/flexslider/js/jquery.flexslider.js"></script> <!-- FlexSlider -->
<script src="../vendor/countdown/js/jquery.countdown.min.js"></script> <!-- Jquery Timer -->
<script src="../vendor/mediaelement/mediaelement-and-player.min.js"></script> <!-- MediaElements --> 
<script src="../style-switcher/js/jquery_cookie.js"></script>
<script src="../style-switcher/js/script.js"></script>
<!-- Style Switcher Start -->
<!-- -->
</body>

</html>
<?php ob_end_flush(); ?>