$(document).ready(function () {
   
    // This is for other ajax request of same type
    $(document).on('click', '#morePulpitSermons', function () {
        var lastId = $(this).val();
        var actionType = $(this).attr('data-request');
        
        $("#moreMessage" + lastId).html("<div align=\"center\" class=\"loader\" style=\"width:40px;height:40px;margin:0 auto;margin-top:10px;margin-bottom:10px;\"></div>");
        var send = {'lastId' : lastId, 'actionType':actionType};
        
        $.ajax
           ({
               type: "POST",
               url: "../scripts/ajaxRequestManager.php",
               data: send,
               cache: true,
               success: function(html)
            { 
                $("#moreMessage" + lastId).empty();
                $("#moreMessage" + lastId).html(html);
            }

           });
        
        
    });
    
    
    // This is to retrieve tagged testimonies
    $(document).on('click', '#moreTestimonies', function () {
        var lastId = $(this).val();
        var tag = $(this).attr('data-tag');
        var tagCheck = $(this).attr('data-tag-bool');
        
        
        $("#moreTestimoniesDump" + lastId).html("<div align=\"center\" class=\"loader\" style=\"width:40px;height:40px;margin:0 auto;margin-top:10px;margin-bottom:10px;\"></div>");
        
        var send = {'lastId' : lastId, 'tag':tag, 'tagCheck':tagCheck};
        
        $.ajax
           ({
               type: "POST",
               url: "../scripts/testimony/ajaxTestimonyRequestManager.php",
               data: send,
               cache: true,
               success: function(html)
            { 
                $("#moreTestimoniesDump" + lastId).empty();
                $("#moreTestimoniesDump" + lastId).html(html);
            }

           });
        
        
    });
    
    
    // This is to retreive testimony tags
    $(document).on('keyup', '#tagForm', function () {
        var tag = $(this).val();
        
        $("#tagDump").html("<div align=\"center\" class=\"loader\" style=\"width:30px;height:30px;margin:0 auto;margin-top:10px;margin-bottom:10px;\"></div>");
        var send = {'tag' : tag};
        
        $.ajax
           ({
               type: "POST",
               url: "../scripts/testimony/ajaxTestimonyTag.php",
               data: send,
               cache: true,
               success: function(html)
            { 
                $("#tagDump").empty();
                $("#tagDump").html(html);
            }

           });
        
        
    });
    
    
    // This to load more DFR's   
    $(document).on('click', '#moreDfr', function () {
        var lastId = $(this).val();
        var actionType = $(this).attr('data-request');
        
        $("#moreDfr" + lastId).html("<div align=\"center\" class=\"loader\" style=\"width:40px;height:40px;margin:0 auto;margin-top:10px;margin-bottom:10px;\"></div>");
        var send = {'lastId' : lastId, 'actionType':actionType};
        
        $.ajax
           ({
               type: "POST",
               url: "../scripts/ajaxRequestManager.php",
               data: send,
               cache: true,
               success: function(html)
            { 
                $("#moreDfr" + lastId).empty();
                $("#moreDfr" + lastId).html(html);
            }

           });
    });
    
    
        
    // This to load more event photos  
    $(document).on('click', '#moreEventPhoto', function () {
        var lastId = $(this).val();
        var eventId = $(this).attr('data-event');
        
        $("#moreEventPhoto").html("Loading...").attr('disabled', true);
        
        var send = {'lastId' : lastId, 'eventid':eventId};
        
        $.ajax
           ({
               type: "POST",
               url: "../scripts/events/eventPhotoListing.php",
               data: send,
               cache: true,
               success: function(html)
            { 
                $("#drop" + lastId).empty();
                $("#drop" + lastId).html(html);
            }

           });
    });
    
    
        // This to unsubscribe from our programs  
    $(document).on('click', '#unsubscribe', function () {
        var email = $(this).val();
        var pType = $(this).attr('data-unsub-type');
        
        $("#" + pType + "unsub").empty().html("<div align=\"center\" class=\"loader\" style=\"width:30px;height:30px;margin:0 auto;margin-top:10px;margin-bottom:10px;\"></div>");
        
        var send = {'email' : email, 'pType': pType};
        
        $.ajax
           ({
               type: "POST",
               url: "../scripts/unsubscribe.php",
               data: send,
               cache: true,
               success: function(html)
            { 
                $("#" + pType + "unsub").empty();
                $("#" + pType + "unsub").html(html);
            }

           });
    });
    
    //This is for email subscription
    $(document).on('click', '#subscribe', function () {
        
        var formId = $(this).attr('data-formValue');
        var email = $("#" + formId).val();
        var actionType = $(this).attr('data-sub-type');
        
        $("#" + actionType).html("<div class=\"loader\" style=\"width:20px;height:20px;margin-top:10px;margin-bottom:10px;\"></div>");
        
        if (email.length == false) {
            $("#" + actionType).empty();
            $("#" + actionType).html("<b style=\"color:red\">Your Email address is required</b>");
        } else {
        
            var send = {'email' : email, 'actionType':actionType};

            $.ajax ({
                   type: "POST",
                   url: "../scripts/emailSubscription.php",
                   data: send,
                   cache: true,
                   success: function(html)
                { 
                    $("#" + actionType).empty();
                    $("#" + actionType).html(html);
                }

               });
        }
        
    });
    
});