<?php require_once ('../inc/header.php'); ?>
<?php require_once ('../scripts/testimony/testimonyMainFile.php'); ?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix">
        <div class="page-header-overlay" style="background-image:url(../images/ph4.jpg);"></div>
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
                <?php 
                    if (isset($_GET["tag"])) {
                        $urlTag = $_GET["tag"];
                        echo '<h2>testimonies tagged '.$urlTag.'</h2>';
                    } else echo '<h2>List Of Shared testimonies</h2>';
                ?>
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.html">Home</a></li>
            	<li class="active">
                    <?php 
                        
                        if (isset($_GET["tag"])) {
                            echo 'List of testimonies Tagged \' '.ucfirst(strtolower($_GET["tag"]))." '";
                        } else echo 'List of shared Testimonies';
                    
                    ?>
                </li>
          	</ol>
        </div>
    </div>

<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <form method="get" action="testimonyList.php">
                            <div class="widget sidebar-widget search-form-widget">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="tag" placeholder="Search for testimonies using tags" id="tagForm" autocomplete="off">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-search fa-lg"></i></button>
                                    </span>
                                </div>
                                <div id="tagDump"></div>
                            </div>
                        </form>
                        <div class="widget sidebar-widget">
                            <h3 class="widgettitle">Testimony Tags</h3>
                          	<div class="tag-cloud">
                            	<a href="testimonyList.php?tag=Daily Faith Recharge">Daily Faith Recharge</a>
                            	<a href="testimonyList.php?tag=faith">Faith</a>
                            	<a href="testimonyList.php?tag=love">Love</a>
                            	<a href="testimonyList.php?tag=promotion">Promotions</a>
                            	<a href="testimonyList.php?tag=blessing">Blessing</a>
                            	<a href="testimonyList.php?tag=employment">Employment</a>
                            	<a href="testimonyList.php?tag=provision">Provision</a>
                                <a href="testimonyList.php?tag=baptism">Baptism</a>
                                <a href="testimonyList.php?tag=protection">Protection</a>
                                <a href="testimonyList.php?tag=healing">Healing</a>
                                <a href="testimonyList.php?tag=God's grace">God's Grace</a>
                                <a href="testimonyList.php?tag=mercy">God's Mercy</a>
                          	</div>
                        </div>
                        <div class="spacer-30"></div>
                    </div>
                    
                	<div class="col-md-8">
                        
                        <?php
                    
                        if (isset($_GET["tag"])) {
                            
                            $tag = htmlentities(htmlspecialchars($_GET["tag"]));
                            
                            if ($tag) {
                                require_once ('../scripts/testimony/testimonyRetrievalbyTag.php');
                                $lastId = "";
                                $callTestimonies = new testimonyByTag ($tag, $lastId, $db);
                            } else {
                                require_once ('../scripts/testimony/testimonyRetrieval.php');
                                $lastId = "";
                                $callTestimonies = new testimonyWithoutTag($lastId, $db);
                            }
                            
                        } else {
                            require_once ('../scripts/testimony/testimonyRetrieval.php');
                            $lastId = "";
                            $callTestimonies = new testimonyWithoutTag($lastId, $db);
                        }
                    
                    ?>
                            
                        
                    
                        <div class="spacer-50"></div>
                        <?php
                        
                            echo $mainTestimony -> testimonyCount ($db);
                        
                        ?>
                        
                    </div>
                </div>
         	</div>
        </div>
   	</div>

<?php require_once ('../inc/footer.php'); ?>