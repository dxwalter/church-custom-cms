<?php include('../inc/header.php'); ?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/slide7.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>About Daily Faith Recharge</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active">About Daily Faith Recharge</li>
          	</ol>
        </div>
    </div>

    <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
               	<h3>Faith comes by hearing God's word. Through the <span style="color:#3bafda">Daily Faith Recharge</span>, you will be energized, revitalized and inspired to face life's daily challenges with confidence and renewed hope making you live like a victor in Christ. Remain charged to take charge. Blessings.</h3>
              	<hr class="sm">
                <h4>THE MISSION</h4>
               	<p>To make available spiritual resource material that will charge up the faith of the children of God on a daily basis, preparing them to face each working day with confidence and boldness through mobile format and other social media platforms.</p>
                
               	<p>To communicate a nugget of God's word, in a non-judgemental or condemnatory manner with a view to stir up doctrinal difference.</p>
               	
                <p>To increase our appetite for God's word and to stimulate our prayer lives.</p>
               	
                <p>Glorifying Christ and upholding His word through the use of modern technology.</p>
                
                <div class="spacer-30"></div>
                
                <hr class="sm">
                <h4>THE VISION</h4>
                
                <p>Based on <span style="color:#3bafda;font-weight:bold;">Hebrews 10: 38 - 39 and Romans 10: 17;</span> therefore, our VISION is;
                    <article>
                        <h4>
                            TO FAN THE FLAMES OF OUR SPIRIT THROUGH AUDIO RECORDED AND INSPIRED WORD OF GOD IN NUGGET FORM.<!-- This will be used as this page title -->
                        </h4>
                    </article>
                </p>
                
                
                <div class="spacer-50"></div>
                
                <h2>THE BACK STORY</h2>
                <P>
                    The vision of <b style="color:#3bafda;font-weight:bold;">DAILY FAITH RECHARGE</b> first came to me in June 2015, when I concluded my daily morning prayers,<br>
                    I picked up my phone to check for the text message that might have come in overnight. I found the battery of the phone so weak that, the phone just went blank when I attempted to access my messages.<br>
                    I needed to charge the phone.<br>
                    At that instant, the vision dropped in my spirit as how people charge their phones on daily basis but never get their faith charged up. I imagined how often the faith of people gone flat in the face of the challenges of the day!<br>
                    I felt the lord would have me do something. While charging the phone, I held the phone in my hands and recorded the first of what we now know today as the 
                    <span style="font-weight:bold;color:#3bafda;">DAILY FAITH RECHARGE</span>. I sent it to a brother,  and then forwarded it to another brother,  and then forwarded it to another brother and a sister in the UK. Within minutes, I got messages from these three people as  how that word blessed them. I sat back and began to forward to a few more people.
                    <br>
                    Interestingly, they all felt the message was just specifically for them. I thought I would do it the next day and that would be it. But by the following day, I was geting messagees from people who received it from those I sent the first one to and wanted me to send to them. That was scary.
                    <br>
                    I said to myself; I would do it for the week and forget all about it. By the end of that week, I felt the lord would have me continue the following week.
                    <br>
                    Then I got a message from Pastor Don Kester. He had just listened to two of the previous ones. He asked me if he could share it. He went on to encourage me on this vison on what He called "<span style="font-weight:bold;color:#3bafda;">THE DAILY CAPSULE; A DAILY DEVOTIONAL</span>" and suggested we got it recorded on CD "FOR BUSY BEES WHO MAY SLOT IT IN THE PLAYERS OF THEIR CARS." Then again, he came up with the idea of transcribing it into text format for publication. That's how the devotional was birthed.
                    <br>
                    <div class="spacer-20"></div>
                        <div class="col-md-4">
                            <img src="../images/KCMIPHOTO/dfrCover.jpg" alt="Cover page for DFR">
                            <br>
                            <br>
                            <h4>The web version is derived from the transcribed text format stated above.</h4>
                        </div>
                    <div class="spacer-20"></div>
                    <div class="col-md-12">
                        <h4>
                            At this juncture, the vision became settled in my spirit.<br>
                            We hope you're blessed by whatever format you receive of the daily faith recharge.
                        </h4>
                    </div>
                    <div class="spacer-30"></div>
                    <div class="col-md-8">
                        <div class="widget sidebar-widget widget_sermon_speakers" style="border:0px;margin:0px;">
                        <?php  ?>
                        <ul>
                        <li>
                        <img src="../images/speaker1.jpg" alt="" width="100" height="100">
                            <h5>&nbsp;</h5>
                        <span class="meta-data"><h3>Philemon Frank AIKINS.</h3></span>
                        </li>
                        </ul>
                        </div>
                    </div>
                    
                </P>
                
            
                <div class="spacer-50"></div>
                <div class="row">
                	<div class="col-md-4 col-sm-4">
                    	<div class="icon-box icon-box-style1">
                        	<div class="icon-box-head">
<!--                            	<span class="ico"><i class="icon-mail"></i></span>-->
                            	<h3>Subscribe to Daily Faith Recharge</h3>
                                <div class="input-group">
                                <input type="text" class="form-control" placeholder="Email Address" id="dfrEmailValue" autocomplete="off">
                                <span class="input-group-btn">
                                <button id="subscribe" data-sub-type="dfrSubscription" class="btn btn-primary" type="button" data-formValue="dfrEmailValue">Subscribe!</button>
                                </span>
                            </div>
                            <div id="dfrSubscription"></div>
                           	</div>
                            
            				<p>Subscribe to Daily Faith Recharge in order to receive the word of the lord every morning. We promise we won't spam your inbox!</p>
                       	</div>
                   	</div>
                    
                	<div class="col-md-4 col-sm-4">
                    	<div class="icon-box icon-box-style1">
                        	<div class="icon-box-head">
<!--                            	<span class="ico"><i class="icon-happy-drop"></i></span>-->
                            	<h3>Share Your Testimony</h3>
                                <p style="padding:5px;">&nbsp;</p>
                                <p><a href="shareTestimony.php"  class="btn btn-success" style="width:100%;">Share Your Testimony</a></p>
                           	</div>
            				<p>Share with the world that which the Lord has done for you through the DFR or any of the Gospel you've been reading.</p>
                       	</div>
                   	</div>
                	<div class="col-md-4 col-sm-4">
                    	<div class="icon-box icon-box-style1">
                        	<div class="icon-box-head">
<!--                            	<span class="ico"><i class="icon-currency"></i></span>-->
                            	<h3>Donate To DFR</h3>
                                <p style="padding:5px;">&nbsp;</p>
                                <p><a href="donate.php" class="btn btn-primary" style="width:100%;">Make your Donations</a></p>
                           	</div>
            				<p>Be a part of DFR. Your donations will go a long way in reaching the lives of people.</p>
                       	</div>
                   	</div>
              	</div>
                <div class="spacer-50"></div>
            </div>
        </div>
   	</div>
    <!-- End Body Content -->


<?php include('../inc/footer.php'); ?>