<?php require_once('../inc/header.php'); ?>

    <div class="page-header parallax clearfix">
    	<div id="contact-map"></div>
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>Find us on the map</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.html">Home</a></li>
            	<li class="active">Our Location on the map</li>
          	</ol>
        </div>
    </div>
    <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
                <div class="row">
                	<div class="col-md-4 col-sm-5">
                    	<h2>Our Locations</h2>
                    	<hr class="sm">
                    	<h4 class="short accent-color">Rivers State, Nigeria</h4>
                    	<p>
                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria 
                            
                            <br>
                            <a href="tel:+2348084583102">+234 808 458 3102</a>
                            <br>
                            <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>
                        </p>
                        
                        <hr class="fw cont">
                        
                        <h4 class="short accent-color">Umahia, Nigeria</h4>
                    	<p>
<!--                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria -->
<!--
                            
                            <br>
                            <a href="tel:+2348084583102">+234 808 458 3102</a>
                            <br>
-->
<!--                            <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>-->
                        </p>
                        
                        
                        <h4 class="short accent-color">Accra, Ghana</h4>
                    	<p>
<!--                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria -->
<!--
                            
                            <br>
                            <a href="tel:+2348084583102">+234 808 458 3102</a>
                            <br>
-->
<!--                            <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>-->
                        </p>
                       
                        
                        <h4 class="short accent-color">Kasoa, Ghana</h4>
                    	<p>
<!--                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria -->
<!--
                            
                            <br>
                            <a href="tel:+2348084583102">+234 808 458 3102</a>
                            <br>
-->
<!--                            <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>-->
                        </p>
                       
                        
                        <h4 class="short accent-color">Lome, Togo</h4>
                    	<p>
<!--                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria -->
<!--
                            
                            <br>
                            <a href="tel:+2348084583102">+234 808 458 3102</a>
                            <br>
-->
<!--                            <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>-->
                        </p>
                       
                        
                   	</div>
                    <div class="col-md-8 col-sm-6" id="contactDiv">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63614.525078939536!2d6.976607517731878!3d4.785846303778563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf79c75918f6481f9!2sKINGDOM+COVENANT+MINISTRIES+INTERNATIONAL!5e0!3m2!1sen!2sng!4v1514058881786" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
               	</div>
           	</div>
        </div>
   	</div>
    <!-- End Body Content -->
<div class="spacer-50"></div>


<?php require_once('../inc/footer.php'); ?>