<?php require_once ('../inc/header.php'); ?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/slide7.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>Tell The world of the lord's Goodness</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active">Share your testimony</li>
          	</ol>
        </div>
    </div>

        <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
                <div class="row">
                    
                	<div class="col-md-4 col-sm-5">
<!--
                    	<h3>Our Terms And Conditions</h3>
                    	<hr class="sm">
                        <h4 class="short accent-color">Toronto</h4>
                    	<p>777, path to God<br>1800-989-990<br><a href="mailto:ca@adorechurch.com">ca@adorechurch.com</a></p>
-->
                   	</div>
                    
                    <div class="col-md-8 col-sm-7">
                        <h3>Share Your Testimony</h3>
                    	<hr class="sm">
                        
                        <?php require_once ('../scripts/testimony/shareTestimony.php'); ?>
                        
                       	<form method="post" name="contactform" class="clearfix" action="shareTestimony.php">
                        	<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" id="fname" name="names"  class="form-control input-lg" placeholder="First and Last names" value="<?php if (isset($names)) {echo $names;} ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" id="contact" name="contact"  class="form-control input-lg" placeholder="Email Address or Phone number" value="<?php if (isset($contact)) {echo $contact;} ?>">
                                    </div>
                              	</div>
                           	</div>
                        	<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" id="email" name="location"  class="form-control input-lg" placeholder="What's your State and Country. Example state, Country" value="<?php if (isset($location)) {echo $location;} ?>">
                                    </div>
                                </div>
                          	</div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" id="email" name="title"  class="form-control input-lg" placeholder="Give Your Testimony a Title" value="<?php if (isset($title)) {echo $title;} ?>">
                                    </div>
                                </div>
                          	</div>
                            
                        	<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea cols="6" rows="10" id="comments" name="message" class="form-control input-lg" placeholder="Type Your Testimony" value="<?php if (isset($message)) {echo $message;} ?>">
                                        </textarea>
                                    </div>
                                </div>
                          	</div>
                        	<div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-lg btn-block" type="submit" name="shareTestimony">submit Testimony</button>
                                </div>
                          	</div>
                		</form>
                    </div>
               	</div>
           	</div>
        </div>
   	</div>
<div class="spacer-50"></div>
    <!-- End Body Content -->

<?php require_once ('../inc/footer.php'); ?>