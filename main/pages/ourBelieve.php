<?php include('../inc/header.php'); ?>

<?php

    $image = array('Our-Beliefs1.jpg', 'Our-Beliefs2.jpg', 'Our-Beliefs3.jpg');
    $path = '../images/KCMIPHOTO/ourbelieve/';

    $rand = rand(0, 2);
    $arrayImage = $image[$rand];
    $display = $path.$arrayImage;

?>
    <!-- Start Page Header -->
    <div class="page-header parallax clearfix">
    	<div class="page-header-overlay" style="background-image:url(<?php echo $display; ?>);"></div>
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>Our Beliefs</h2>
        </div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li><a href="aboutUs.php">About Us</a></li>
            	<li class="active">What We Be Believe</li>
          	</ol>
        </div>
    </div>

    <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container" style="text-align:center">
                <h3><span style="color:#3bafda">The Bible:</span> We believe that the sixty-six Canonical Books of the Bible were given by Divine Inspiration and that the whole Bible is the WORD OF GOD and that it is TRUE and infallible. Therefore we take the Bible as our final authority in all matters. </h3>
                
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3><span style="color:#3bafda">Depravity of man:</span> we believe that all men being the offspring of Adam have sinned and fallen short of the glory of God and that redemption is only by faith in the Lord Jesus </h3>
                
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe in one God, the Creator of the Universe. </h3>
                
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe in the Trinity; Father, Son and the Holy Spirit. </h3>
                
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe in the Virgin Birth, death and resurrection of Jesus Christ; that Jesus rose in bodily form from the grave on the third day. </h3>
                
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe in the imminent personal return of the Lord Jesus Christ –"The Second Coming".</h3>
                
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe that Salvation is only in Jesus Christ, who through His shed blood made a full, perfect and sufficient sacrifice for the cleansing of all sins of mankind: and that man and the whole world can only be reconciled to God through Jesus Christ.</h3>
                
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe in divine healing, signs, wonders and miracles through faith in the finished work of the Lord Jesus at Calvary.</h3>
                
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3> We believe in the five-fold ministry gifts of the Church, the gifts of the Holy Spirit, and the Baptism of the Holy Spirit, accompanied by the initial sign of speaking with other tongues. </h3>
                
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                
                <h3>We believe in righteousness and personal holiness – "by ye holy, for I am Holy. </h3>
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                
                <h3>We believe in righteousness and personal holiness – "by ye holy, for I am Holy. </h3>
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe that water baptism and Lord's supper are ordinances to be observed by the Church during this present age. However, they are not to be regarded as means of salvation. </h3>
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                
                <h3>We believe that every born-again, spirit-filled believer should maintain a consistent prayer life. This should be through both praying with understanding and praying in the spirit. </h3>
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                
                <h3>We believe that without faith. It is impossible to please God, because he that comes to God must believe that He is and the He is a rewarder of those that diligently seek Him. </h3>
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe that the true Church is composed of all those who are born again and that through this new birth, we are united together in the body of Christ, Jesus Christ being the Lord and Head of the Church. </h3>
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe in the Local Church as the assembly of the righteous together, for the preaching of the gospel and to world evangelization. </h3>
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe in tithes and offerings to do the work of the ministry and as an avenue of spiritual, financial and material blessings to the individual believer. </h3>
                <img src="../images/KCMIPHOTO/ourbelieve/breaker.png">
                <p></p>
                
                <h3>We believe in the bodily resurrection of the dead, of the believer to everlasting joy with the Lord and of the unbeliever to judgment and everlasting conscious punishment </h3>
                
                
                <div class="spacer-50"></div>
            </div>
        </div>
   	</div>
    <!-- End Body Content -->


<?php include('../inc/footer.php'); ?>