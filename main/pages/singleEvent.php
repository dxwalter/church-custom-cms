<?php require_once('../inc/header.php'); ?>
<?php require_once('../scripts/events/events.php'); ?>
<?php

if (isset($_GET["ijn"])) {
    $eventId = $_GET["ijn"];
    
    if ($eventId) {
        
        $eventDetails = $events -> singleEventDetails ($eventId, $db);
        $eventDate = $events -> eventTimeDate ($eventDetails["day"], strtolower($eventDetails["month"]), $eventDetails["year"]);
        
    } else header('location: 404.php');
    
} else header('location: 404.php');

?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix">
    	<div class="page-header-overlay" style="background-image:url(<?php echo 'http://localhost/rest/Photouploads/events/'.$eventDetails["image"]; ?>);"></div>
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2><?php echo $eventDetails["eventtitle"]; ?></h2>
      			<div class="header-event-time"><?php echo $eventDetails['day']." ".$eventDetails["month"].", ".$eventDetails["year"]; ?></div>
        </div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.html">Home</a></li>
            	<li><a href="events.php">List of events</a></li>
            	<li class="active"><?php echo $eventDetails["eventtitle"]; ?></li>
          	</ol>
        </div>
    </div>
    <!-- Start Body Content -->

  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="single-event-content event-list-item event-dynamic">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                        	<div class="event-details">
                            	<div class="event-details-left">
                           			<img src="<?php echo 'http://localhost/rest/Photouploads/events/'.$eventDetails["image"]; ?>" alt="" class="temp-thumbnail">
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                    	<p>
                                        
                                            <?php echo $eventDetails["writeup"]; ?>
                                            
                                        <?php
                                            $pictureCount = $events -> eventImageCount($eventId, $db);
                                            if ($pictureCount) {
                                                echo '<h4><a href="eventPictures.php?ijn='.$eventId.'"><b style="color:#3bafda">View pictures from '.$eventDetails["eventtitle"].'</b></a></h4>';
                                            }
                                        ?>
                                            
                                        <h4>
                                            <span style="color:#3bafda"><i class="icon-calendar fa-2x"></i></span><br>
                                            <?php echo $eventDate.' at '.$eventDetails["eventTime"];?>
                                        </h4>
                                            
                                        <h4>
                                            <span style="color:#3bafda"><i class="icon-compass fa-2x"></i></span><br>
                                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Gym Port Harcourt, Rivers State, Nigeria 
                                        </h4>
                                            
                                        
                                        </p>
                                    
                                    <div class="social-share-bar" style="border:0px;">
                                    <?php $link = 'http://www.kcmi-rcc.com/singleEvent.php?ijn='.$eventId.''; ?>
                                        <h4><i class="fa fa-share-alt"></i> Share This Event</h4>
                                        <ul class="social-icons-colored">
                                            <li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>"><i class="fa fa-facebook"></i></a></li>
                                            <li class="twitter"><a href="https://twitter.com/home?status=<?php echo $link; ?>"><i class="fa fa-twitter"></i></a></li>
                                            <li class="googleplus"><a href="https://plus.google.com/share?url=<?php echo $link; ?>"><i class="fa fa-google-plus"></i></a></li>
                                        </ul>
                                    </div>
                                    
                                    </div>
                               	</div>
                        </div>
                        </div>
                            <div class="col-md-7 col-sm-7">
                            <nav aria-label="...">
                              <ul class="pager">
                                <?php
                                  
                                  echo $events -> prevEvent ($eventId, $db);
                                  echo $events -> nextEvent ($eventId, $db);
                                ?>
                                </ul>
                            </nav>
                            </div>
                        <input type="text" id="readTimer" value="<?php echo $eventId ?>" data-read-type="event" hidden="true" class="hidden">
                </div>
           	</div>
        </div>
   	</div>
</div>

<div class="spacer-50"></div>
<?php require_once('../inc/footer.php'); ?>
<script src="../js/readTimer.js"></script>