<?php require_once ('../inc/header.php'); ?>
<?php require_once ('../scripts/sermons/mainSermonFile.php'); ?>
<?php require_once ('../scripts/sermons/callMajorMessages.php'); ?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/ph8.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>Sermon Series</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.html">Home</a></li>
            	<li class="active">Sermon series</li>
          	</ol>
        </div>
    </div>



<!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
                <div class="row">
                    
                        
                        <?php 
                        
                        $lastId = "";
                        $callMessages = new callMultipleSermons($lastId, $db);
                        
                        ?>
                        
                    
                </div>
         	</div>
        </div>
   	</div>

    <div class="spacer-50"></div>
    <!-- End Body Content -->


<?php require_once ('../inc/footer.php'); ?>