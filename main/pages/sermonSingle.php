<?php require_once ('../inc/header.php'); ?>
<?php 
    require_once ('../scripts/sermons/mainSermonFile.php'); 
    $callSemons = new sermons($db);

    if (isset($_GET["ijn"])) {
        $id = $_GET["ijn"];
        
        if ($id == false) {
            header('location: sermons.php');
        } else {
            $messageDetails = $callSemons -> singleMessage($id, $db);
        }
    } else {
        header('location: sermons.php');
    }
?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/baptism.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2><?php echo $messageDetails["title"]; ?></h2>
           	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li><a href="sermons.php">Sermon Series</a></li>
            	<li class="active"><?php echo $messageDetails["title"]; ?></li>
          	</ol>
        </div>
    </div>
    <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
                <div class="row">
                	<div class="col-md-8">
                    	<div class="sermon-media clearfix">
                            
                            <h3><b style="color:#3bafda">Bible Verse:</b> <?php echo $messageDetails["bibleverse"]; ?></h3>
                            
                            <blockquote>
                            <p style="font-size:17px;color:#000;font-family: clicker Script;line-height:20px;">
<!--                                <span style="font-size:30px;color:#3bafda">"</span>-->
                                    <?php echo $messageDetails["bibletext"]; ?>
<!--                                <span style="font-size:30px;color:#3bafda">"</span>-->
                            </p>
                            </blockquote>
                        	
                            <div class="sermon-media-right">
                                <p style="font-size:16px"><?php echo $messageDetails["message"]; ?></p>
                                
                                <div class="col-md-8">
                                    <div class="widget sidebar-widget widget_sermon_speakers" style="border:0px;margin:0px;">
                                    <h3>Sermon Speaker</h3>
                                    <?php  ?>
                                    <ul>
                                    <li>
                                    <?php 
                                        $rand = rand(1, 2);
                                        $image = $rand.".jpg";
                                    ?>
                                    <img src="../images/KCMIPHOTO/speakers/<?php echo $image; ?>" alt="Speaker's Photo" width="100" height="100">
                                    <h5>Apostle Frank Aikins</h5>
                                    <span class="meta-data">Event Speaker</span>
                                    </li>
                                    </ul>
                                    </div>
                                </div>
                                
                                <div class="spacer-10"></div>
                                <div class="social-share-bar" style="border:0px;margin-top:0px;">
                                    <h4><i class="fa fa-share-alt"></i> Share This Message</h4>
                                    <ul class="social-icons-colored">
                                        <?php $link = "http://www.kcmi-rcc.com/sermonSingle.php?ijn=$id"; ?>
                                        <li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>"><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="https://twitter.com/home?status=<?php echo $link; ?>"><i class="fa fa-twitter"></i></a></li>
                                        <li class="googleplus"><a href="https://plus.google.com/share?url=<?php echo $link; ?>"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                                
                                
                                <nav aria-label="...">
                                    <ul class="pager">
                                        <?php echo $callSemons -> singleMessagePrev($id, $db); ?>
                                        <?php echo $callSemons -> singleMessageNext($id, $db); ?>
                                    </ul>
                                </nav>
                                
                                
                                <div class="spacer-30"></div>
                          	</div>
                       	</div>
                    </div>
                    
                    
                    <div class="col-md-4">
                    	<div class="widget sidebar-widget donate_form_widget">
                        	<h3 class="widgettitle">Subscribe to our Messages</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Receive emails when new message transcript are posted.</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Email Address" id="sermonEmailValue" autocomplete="off">
                                            <span class="input-group-btn">
                                            <button id="subscribe" data-sub-type="sermonSubscription" class="btn btn-primary" type="button" data-formValue="sermonEmailValue">Subscribe!</button>
                                            </span>
                                        </div>
                                        <div id="sermonSubscription"></div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <input type="text" id="readTimer" value="<?php echo $id ?>" data-read-type="pulpitmessage" hidden="true" class="hidden">
                </div>
         	</div>
        </div>
   	</div>
    <!-- End Body Content -->


<?php require_once ('../inc/footer.php'); ?>
<script src="../js/readTimer.js"></script>