<?php require_once ('../inc/header.php'); ?>
<?php 
    require_once ('../scripts/wordForTheMonth/wordForTheMonthFile.php'); 
    

    if (isset($_GET["ijn"])) {
        $id = $_GET["ijn"];
        
        if ($id == false) {
            header('location: 404.php');
        } else {
            $wordDetails = $callWordForTheMonth ->  selectSingleWord ($id, $db);
            $previousButton = $callWordForTheMonth -> prevButton($id, $db);
            $nextButton = $callWordForTheMonth -> nextButton($id, $db);
        }
    } else {
        header('location: 404.php');
    }
?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/baptism.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2><?php echo $wordDetails["word"]; ?></h2>
           	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active"><?php echo $wordDetails["word"]; ?></li>
          	</ol>
        </div>
    </div>
    <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
                <div class="row">
                	<div class="col-md-8">
                    	<div class="sermon-media clearfix">
                            
                            <h3><b style="color:#3bafda">Bible Verse:</b> <?php echo $wordDetails["bibleverse"]; ?></h3>
                            
                            <blockquote>
                            <p style="font-size:17px;color:#000;font-family: clicker Script;line-height:20px;">
<!--                                <span style="font-size:30px;color:#3bafda">"</span>-->
                                    <?php echo $wordDetails["bibletext"]; ?>
<!--                                <span style="font-size:30px;color:#3bafda">"</span>-->
                            </p>
                            </blockquote>
                        	
                            <div class="sermon-media-right">
                                <article style="font-size:16px"><?php echo $wordDetails["writeup"]; ?></article>
                                
                                <div class="spacer-10"></div>
                                <div class="social-share-bar" style="border:0px;margin-top:0px;">
                                    <h4><i class="fa fa-share-alt"></i> Share This Message</h4>
                                    <ul class="social-icons-colored">
                                        <?php $link = "http://www.kcmi-rcc.com/wordForTheMonth.php?ijn=$id"; ?>
                                        <li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>"><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="https://twitter.com/home?status=<?php echo $link; ?>"><i class="fa fa-twitter"></i></a></li>
                                        <li class="googleplus"><a href="https://plus.google.com/share?url=<?php echo $link; ?>"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                                
                            <nav align="center">
                              <ul class="pager">
                                <?php
                                  echo $previousButton;
                                  echo $nextButton;
                                  ?>
                              </ul>
                            </nav>
                                
                                <div class="spacer-30"></div>
                          	</div>
                       	</div>
                    </div>
                    
                    
                </div>
         	</div>
        </div>
   	</div>
    <!-- End Body Content -->


<?php require_once ('../inc/footer.php'); ?>