<?php require_once ('../inc/header.php'); ?>
<?php require_once ('../scripts/wordForTheMonth/wordForTheMonthFile.php');?>
<?php require_once ('../scripts/events/events.php');?>
<?php require_once ('../scripts/dfr/mainDfrFile.php');?>
<?php require_once ('../scripts/sermons/mainSermonFile.php');?>
<?php require_once ('../scripts/testimony/testimonyMainfile.php');?>
<?php require_once ('../scripts/indexGallery.php'); ?>
<!-- This is the beginning index page slider -->

<?php require_once ('../scripts/indexSlider.php'); ?>

<!-- This is the end of the index page slider -->

<!-- This part ends the next service time stop watch  -->
<div class="upcoming-event-bar event-dynamic">
    <div class="container">
        <div class="row" style="padding-top:20px;"><!-- getdate(); function -->
            <div class="col-md-5 col-sm-5">
                
                <h2><a href="livestream.php" class="event-title">JOIN OUR LIVE SERVICES</a></h2>
            </div>
            <div class="col-md-5 col-sm-7">
                <div id="counter" class="counter clearfix" data-date="July 13, 2018" style="float:right">
                    <a href="https://live.kcmi-rcc.org" target="_blank" class="btn btn-primary btn-lg">Join us</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
<div class="upcoming-event-bar event-dynamic">
    <div class="container">
        <div class="row"> getdate(); function 
            <div class="col-md-5 col-sm-5">
                <span class="label label-primary">Upcoming Event</span>
                <h3><a href="single-event.html" class="event-title">Next Service Will Be In</a></h3>
            </div>
            <div class="col-md-5 col-sm-7">
                <div id="counter" class="counter clearfix" data-date="July 13, 2018">
                    <div class="timer-col"> <span id="days">250</span> <span class="timer-type">Days</span> </div>
                    <div class="timer-col"> <span id="hours">17</span> <span class="timer-type">Hours</span> </div>
                    <div class="timer-col"> <span id="minutes">48</span> <span class="timer-type">Minutes</span> </div>
                    <div class="timer-col"> <span id="seconds">53</span> <span class="timer-type">Seconds</span> </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->



<!-- This part ends the next service time stop watch  -->

<!-- This is the section that holds word for the month and it also holds Daily Faith recharge -->

<div class="lead-content clearfix">
    <div class="lead-content-wrapper">
    		<div class="container">
            	<div class="row">
                    
                    <div class="col-md-4 col-sm-4 featured-block">
                        <a href="wordForTheMonth.php?ijn=<?php echo $callWordForTheMonth -> wordId; ?>"> 
                            <h3>Word For The Month</h3>
                        </a>
                        <figure>
                        	<a href="wordForTheMonth.php?ijn=<?php echo $callWordForTheMonth -> wordId; ?>"><img src="../images/KCMIPHOTO/WORDfORTHEMONTH.jpg" alt="<?php echo $callWordForTheMonth -> wordTitle; ?>" width="100%"></a>
                    	</figure>
                        <h4>
                            <a href="wordForTheMonth.php?ijn=<?php echo $callWordForTheMonth -> wordId; ?>">    <?php echo $callWordForTheMonth -> wordTitle; ?>
                            </a>
                        </h4>
                   		<p>
                            <?php echo $callWordForTheMonth -> sliderWord (); ?>
                            <a href="wordForTheMonth.php?ijn=<?php echo $callWordForTheMonth -> wordId; ?>" class="btn btn-default sm">
                                Read More
                            </a>
                        </p>
                    </div>
                    
                    
                    
                    <div class="col-md-4 col-sm-4 featured-block">
                        <?php 
                            $callSermons -> lastUploadedSermon($db);
                            $messageTitle = $callSermons -> lastMessageTitle;
                            $messageBody = $callSermons -> lastMessageWord;
                            $messageId = $callSermons -> lastMessageId;
                        
                        ?>
                        <h3><a href="sermonSingle.php?ijn=<?php echo $messageId; ?>">From the Pulpit</a></h3>
                        <figure>
                        	<a href="sermonSingle.php?ijn=<?php echo $messageId; ?>"><img src="../images/KCMIPHOTO/SERMONS.jpg" alt="Our Community" width="100%"></a>
                    	</figure>
                        <h4><a href="sermonSingle.php?ijn=<?php echo $messageId; ?>"><?php echo $messageTitle; ?></a></h4>
                   		<p>
                            <?php echo $messageBody; ?>
                            <a href="sermonSingle.php?ijn=<?php echo $messageId; ?>" class="btn btn-default sm">
                                Read More
                            </a>
                        </p>
                    </div>
                    
                    
                    <div class="col-md-4 col-sm-4 featured-block">
                        
                        <a href="singleDfr.php?ijn=<?php echo $dfrSliderId; ?>">
                            <h3>Daily Faith Recharge</h3>
                        </a>
                        <figure>
                        	<a href="singleDfr.php?ijn=<?php echo $dfrSliderId; ?>">
                                <img src="../images/KCMIPHOTO/dfr.jpg" alt="DFR" width="100%;">
                            </a>
                    	</figure>
                        
                        <h4>
                            <a href="singleDfr.php?ijn=<?php echo $dfrSliderId; ?>">
                                <?php echo $dfrSliderTitle; ?>
                            </a>
                        </h4>
                        
                   		<p>
                            <?php echo $dfrSliderMessage;?>
                            <a href="singleDfr.php?ijn=<?php echo $dfrSliderId; ?>" class="btn btn-default sm">
                                Read More
                            </a>
                        </p>
                    </div>
                    
                    
                    
                </div>
        	</div>
        </div>
</div>

<!-- This is the end of the section that holds word for the month and it also holds Daily Faith recharge -->

<!-- This is the part that handles upcoming event -->
<div class="main" role="main">
    <div id="content" class="content full">
        	<div class="container">
            	<div class="row">
                    <div class="col-md-4 col-sm-5">
                        <section class="upcoming-event format-standard event-list-item event-dynamic">
                            <a href="singleEvent.php?ijn=<?php echo $eventSliderId; ?>" class="media-box">
                                <img src="<?php echo $eventSliderImage; ?>" alt="<?php echo $eventSliderTitle; ?>">
                            </a>
                            
                            <?php
                            
                                $recentEventData = new events($db);
                                $newData = $recentEventData -> sliderEvent ($db);
                                $day = $recentEventData -> sliderEventDay;
                                $month = $recentEventData -> sliderEventMonth;
                                $year = $recentEventData -> sliderEventYear;
                                $time = $recentEventData -> sliderEventTime;
                            
                            ?>
                            <div class="upcoming-event-content">
                                <span class="label label-primary upcoming-event-label">Next coming event</span>
                                <h3><a  href="singleEvent.php?ijn=<?php echo $eventSliderId; ?>" class="event-title"><?php echo $eventSliderTitle; ?></a></h3>
                                <span class="meta-data">On <span class="event-date"><?php echo $day.', '.$month." ".$year; ?></span> at <?php echo strtoupper($time); ?></span>
                                <br>
                              	<span class="meta-data event-location"> 
                                <span class="event-location-address">KCMI Port Harcourt Nigeria</span>
                                </span>
                            </div>
                            <div class="upcoming-event-footer">
                            	<a href="singleEvent.php?ijn=<?php echo $eventSliderId; ?>" class="pull-right btn btn-primary btn-sm event-register-button">See More</a>
                                <?php require_once('../scripts/socialMediaShare.php'); ?>
                            </div>
                        </section>
                   	</div>
                    <div class="col-md-8 col-sm-7">
                        <div class="element-block events-listing">
                            <div class="events-listing-header">
                                <a href="events.php" class="pull-right basic-link">View All Events</a>
                                <h3 class="element-title">Upcoming Events</h3>
                                <hr class="sm">
                            </div>
                            <div class="events-listing-content">
                                
                                <!-- This will be three resukt -->
                                <?php $recentEventData -> indexEventList($eventSliderId, $db); ?>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="fw">
                
                
                
                <div class="row">
                    <div class="col-md-8">
                        <h3>Testimony</h3>
                        <hr class="sm">
                        <div class="row">
                        	<div class="col-md-6">
                            	<div class="very-latest-post format-standard">
                                    
                                    <?php
                                    
                                        $mainTestimony -> veryRecent($db);
                                        $veryLatestId = $mainTestimony -> veryRecentId;
                                        $veryLatestTag = $mainTestimony -> veryRecentTag;
                                        $veryLatestName = $mainTestimony -> veryRecentName;
                                        $veryLatestDate = $mainTestimony -> veryRecentDate;
                                        $veryLatestTestimony = $mainTestimony -> veryRecentTestimony;
                                        $veryLatestTitle = $mainTestimony -> veryRecentTitle;
                                    
                                    ?>
                                	<div class="title-row">
                                		<h4>Very latest</h4>
                                    </div>
                                    <a href="mainTestimony.php?ijn=<?php echo $veryLatestId; ?>" class="media-box post-thumb">
                                        <img src="../images/event_img4.jpg" alt="">
                                    </a>
                                    <h3 class="post-title"><a href="mainTestimony.php?ijn=<?php echo $veryLatestId; ?>"><?php echo $veryLatestTitle; ?></a></h3>
                                    <div class="meta-data">by <a href="#"><?php echo $veryLatestName; ?></a> <?php echo $veryLatestDate; ?></div>
                                    <p><?php echo $veryLatestTestimony; ?></p>
                                    <p><a href="mainTestimony.php?ijn=<?php echo $veryLatestId; ?>" class="basic-link">Continue reading</a></p>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <ul class="blog-classic-listing">
                                    <?php $mainTestimony -> indexTestimonyList ($veryLatestId, $db); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4">
                    	<div class="widget latest_sermons_widget">
                        	<ul>
                            	<li class="most-recent-sermon clearfix">
                                    <h3>Recent Sermons</h3>
                                    <hr class="sm">
                                    <?php $callSermons -> indexMessageListing ($db); ?>
                                    
                                    <li style="float:right">
                                        <a href="sermons.php" class="btn btn-default btn-transparent event-tickets event-register-button btn-sm">More Messages</a>
                                    </li>
                           	</ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="spacer-50"></div>
<!-- This part ends upcoming programs  -->

<!-----------------------This is gallery ------------------------------------>
<div class="gallery-updates cols5 clearfix">
    	<ul>
            <?php $callPhoto = new indexGallery($db); ?>
      	</ul>
        <div class="gallery-updates-overlay">
        	<div class="container">
            	<i class="icon-multiple-image"></i>
                <h2>Updates from our gallery</h2>
            </div>
        </div>
    </div>

<?php require_once ('../inc/footer.php'); ?>