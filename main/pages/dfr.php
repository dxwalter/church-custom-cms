<?php require_once ('../inc/header.php'); ?>
<?php require_once ('../scripts/dfr/callDfrMajor.php'); ?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/baptism.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>Daily Faith Recharge</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->

    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active">Daily Faith Recharge</li>
          	</ol>
        </div>
    </div>


   <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full" style="padding:0px;">
        	<div class="container">
                <div class="row">
                	<div class="col-md-7 col-sm-7">
                        <ul class="sermons-list">
                            <?php 
                                $lastId = 0;
                                $callMessage = new callDfrMessageList($lastId, $db);
                            ?>
                        </ul>
                    </div>
                </div>
         	</div>
        </div>
   	</div>

    <div class="spacer-50"></div>
    <!-- End Body Content -->

<?php require_once('../inc/footer.php'); ?>