<?php require_once('../inc/header.php'); ?>
<?php require_once('../scripts/events/eventsListing.php'); ?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/ph7.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>Event Listing</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active">Upcoming Events</li>
          	</ol>
        </div>
    </div>


    <!-- Start Body Content -->
  	 <div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
            	<div class="row">
                    <div class="col-md-9 col-sm-9">
                        <div class="element-block events-listing">
                            <ul class="events-listing-content sort-destination isotope-events" data-sort-id="events">
                                <?php
                                    
                                    if (isset($_GET["prev"])) {
                                        
                                        $previousId = $_GET["prev"];
                                        if ($previousId) {
                                            $callEvents = new eventListing($previousId, 'prev', $db);
                                        } else {
                                            header('location: 404.php');
                                        }
                                        
                                    } else if (isset($_GET["next"])) {
                                        
                                        $nextId = $_GET["next"];
                                        if ($nextId) {
                                            $callEvents = new eventListing($nextId, 'next', $db);
                                        } else {
                                            header('location: 404.php');
                                        }
                                        
                                    } else {
                                        $lastId = 0;
                                        $callEvents = new eventListing($lastId, 'none', $db);    
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
              	</div>
           	</div>
        </div>
   	</div>

        <div class="spacer-50"></div>

        <?php
            $nextButton = $callEvents -> nextEventId;
            $previousButton = $callEvents -> previousEventId;
            $callEvents -> eventListingPagination ($nextButton, $previousButton, $db);
        ?>
    <!-- End Body Content -->   
<div class="spacer-50"></div>

<?php require_once('../inc/footer.php'); ?>