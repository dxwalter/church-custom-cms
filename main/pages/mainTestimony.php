<?php require_once('../inc/header.php'); ?>
<?php require_once ('../scripts/testimony/testimonyMainFile.php'); ?>

<?php 

    if (isset($_GET["ijn"])) {
        
        $testimonyId = $_GET["ijn"];
        if ($testimonyId) {
            $testimonyDetails = $mainTestimony -> testimonyDetails($testimonyId, $db);
            
        } else {
            header('location: 404.php');
        }
        
    } else {
        header('location: 404.php');
    }

?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/baptism.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2><?php echo $testimonyDetails["title"]; ?></h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->

    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active"><?php echo $testimonyDetails["title"]; ?></li>
          	</ol>
        </div>
    </div>


    
   <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
                <div class="row">
                	<div class="col-md-9 col-sm-9">
                        <div class="sermon-series-description">
                            
                            <div class="meta-data">
                            	<span><i class="fa fa-calendar"></i> <?php echo $testimonyDetails["dateadded"]; ?> by <a href="#"><?php echo $testimonyDetails["name"]; ?></a>, <?php echo $testimonyDetails["location"]; ?></span>
                            	<div class="meta-data post-tags"><i class="fa fa-tags"></i> 
                                    <a href="testimonyList.php?tag=<?php echo $testimonyDetails["tag"]; ?>" data-toggle="tooltip" data-placement="right" data-original-title="View other testimonies tagged <?php echo $testimonyDetails["tag"]; ?>"><?php echo $testimonyDetails["tag"]; ?></a>
                                </div>
                                <div class="spacer-20"></div>
                           	</div>
                             
                        	<p style="font-size:17px">
                                <?php echo $testimonyDetails["message"]; ?>
                            </p>
                            
                            
                            <div class="social-share-bar" style="border:0px;">
                                <?php $link = 'http://www.kcmi-rcc.com/mainTestimony.php?ijn='.$testimonyId.''; ?>
                                    <h4><i class="fa fa-share-alt"></i> Share This Testimony</h4>
                                    <ul class="social-icons-colored">
                                        <li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>"><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="https://twitter.com/home?status=<?php echo $link; ?>"><i class="fa fa-twitter"></i></a></li>
                                        <li class="googleplus"><a href="https://plus.google.com/share?url=<?php echo $link; ?>"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            
                            <div class="col-md-push-6">
                                <nav aria-label="...">
                                  <ul class="pager">
                                    <?php $mainTestimony -> previousMainTestimony($testimonyId, $db); ?>
                                    <?php $mainTestimony -> nextMainTestimony($testimonyId, $db); ?>
                                  </ul>
                                </nav>
                            </div>
                            
                        </div>
                    
                    </div>
                </div>
         	</div>
        </div>
   	</div>

    <div class="spacer-50"></div>
    <div class="upcoming-event-bar event-dynamic">
    <div class="container">
        <div class="col-md-4 col-sm-4">
            <p></p>
            <p><a href="shareTestimony.php"  class="btn btn-success btn-lg">Share Your Testimony</a></p>
        </div>
        <div class="col-md-4 col-sm-4">
            <p></p>
            <p><a href="donate.php" class="btn btn-default btn-lg">Make your Donations</a></p>
        </div>
    </div>
    </div>
    <!-- End Body Content -->



<input type="text" id="readTimer" value="<?php echo $testimonyId ?>" data-read-type="testimony" hidden="true" class="hidden">



<?php require_once('../inc/footer.php'); ?>
<script src="../js/readTimer.js"></script>