<?php require_once ('../inc/header.php'); ?>
<?php require_once ('../scripts/dfr/callDfrmajor.php'); ?>
<?php require_once '../scripts/livestream/callIframe.php' ?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/baptism.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>Live Worship</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->

    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active">Live Service</li>
          	</ol>
        </div>
    </div>


   <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full" style="padding:0px;">
        	<div class="container">
                <div class="row">
                	<div class="col-md-12 col-sm-12">
                        
                        <div class="sermon-media clearfix">
                            <div class="sermon-media-right">
                            	<div class="sermon-media-content">
                                    
                                    <div class="video-container fw-video sermon-tabs" id="vimeo-video">
                                        
                                        <!--tala  -->
                                        <?php  $callIframe = new callIframe($db); ?>
                                    </div>
                                    
                                    <div style="font-size: 11px;padding-top:10px;text-align:center;width:560px">
                                      <a href="https://facebook.com/kcmiworldwide" title="Watch kcmi">kcmi</a> on livestream.com.</div>
                                    </div>
                                
                                    <p style="font-size:16px"><h4>Our wonderful and refreshing services are streamed live so you can also be part of the awesome things God is doing in our midst. Always remember to keep a date with us.</h4></p>
                                
                                    <p>
                                
                                        <h3>Thursdays by 5:30 pm</h3>
                                        <h3>Sundays by 8:15 am</h3>
                                        <h4>And also during our <a href="events.php">events</a></h4>
                                
                                    </p>
                                
                                <div class="social-share-bar">
                                    <h4><i class="fa fa-share-alt"></i> Share</h4>
                                    <ul class="social-icons-colored">
                                        <li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=http://www.kcmi-rcc.com/pages/livestream.php"><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="https://twitter.com/home?status=http://www.kcmi-rcc.com/pages/livestream.php"><i class="fa fa-twitter"></i></a></li>
                                        
                                        <li class="googleplus"><a href="https://plus.google.com/share?url=http://www.kcmi-rcc.com/pages/livestream.php"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                          	</div>
                       	</div>
                        
                    </div>
                </div>
         	</div>
        </div>
   	</div>

    <div class="spacer-50"></div>
    <!-- End Body Content -->

<?php require_once('../inc/footer.php'); ?>