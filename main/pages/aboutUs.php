<?php require_once ('../inc/header.php'); ?>


    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/slide7.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>About our church</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active">About Us</li>
          	</ol>
        </div>
    </div>

    <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
               	<h2>Welcome to Kingdom Covenant Ministeries</h2>
              	<hr class="sm">
                <h4>Rehoboth Christian Center</h4>
               	<p style="font-size:16px;">Rev. Frank Aikins is the Founder and the Senior pastor of Rehoboth Christian Centre and the president of Kingdom Covenant Ministry International, Port Harcourt. His God given mandate is to Raise Kings to Build God's Kingdom. To this ends he ministers to the whole man; spirit, soul and body. To help them understand their authority as children of God and their responsibilities towards God's Kingdom. His passion is to bring hope to the hopeless, to train and equip them to become co-workers for the furthering of the Gospel of our Lord Jesus Christ, to this country and other parts of the world. <br>

                He is a well travelled and greatly sought for conference and revival meeting speaker round the world. With close to three decades of ministry under his belt, Pastor Frank has raised a big army of ministers who are doing great Kingdom exploits around the world. As an apostle, he plays a father's role over many young ministers. He is an author with some great books on the shelf.<br>

                Rev. Dr. Mrs Elfleda Aikins is the wife of Pastor Frank and the co-founder of Rehoboth Christian Centre. She is a Dental Surgeon by training and a minister of the Gospel of Christ by calling. Her vision is to see the vision of her husband fulfilled. Her passion within the ministry is towards homes and marriages.
                <br>
                A great teacher of the word, she combines her work in the surgery with a great number of speaking engagements in and outside Nigeria.

                Her annual 'Couples Retreat' program has had such great impact on many homes that it gets expanded every year. Apart from her biological three sons and a daughter , she is a mother and a mentor of very many, mainly wives of other pastors</p>
                                
                <div class="spacer-50"></div>
                <div class="row">
                    
                    <a href="ourBelieve.php">
                	<div class="col-md-4 col-sm-4">
                    	<div class="icon-box icon-box-style1">
                        	<div class="icon-box-head">
                            	<span class="ico"><i class="icon-happy-drop"></i></span>
                            	<h3>What We Believe In</h3>
                           	</div>
<!--            				<p>Be .</p>-->
                       	</div>
                   	</div>
                    </a>
                    
                    <a href="workingGroup.php">
                	<div class="col-md-4 col-sm-4">
                    	<div class="icon-box icon-box-style1">
                        	<div class="icon-box-head">
                            	<span class="ico"><i class="icon-heart"></i></span>
                            	<h3>Areas Of Ministries</h3>
                           	</div>
<!--            				<p>We hold yearly programmes to help the needy.</p>-->
                       	</div>
                   	</div>
                    </a>
                    
                    
                    <a href="homeCell.php">
                	<div class="col-md-4 col-sm-4">
                    	<div class="icon-box icon-box-style1">
                        	<div class="icon-box-head">
                            	<span class="ico"><i class="icon-home"></i></span>
                            	<h3>Home Cell Centers</h3>
                           	</div>
<!--            				<p>Every week day there is someone waiting and willing to talk and pray with you.</p>-->
                       	</div>
                   	</div>
                    </a>
                    
                    
              	</div>
<!--                <hr class="fw">-->
                <div class="spacer-50"></div>
            </div>
        </div>
   	</div>
    <!-- End Body Content -->


<?php require_once ('../inc/footer.php'); ?>