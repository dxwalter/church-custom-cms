<?php require_once ('../inc/header.php'); ?>
<?php require_once ('../scripts/events/events.php'); ?>
<?php require_once ('../scripts/events/eventPhotoListing.php'); ?>
<?php

    if (isset($_GET["ijn"])) {
        
        $id = $_GET["ijn"];
        if ($id == false) {
            header('location: 404.php');
        }
        
    } else header('location: 404.php');

$eventDetails = $events -> eventDetails ($id, $db);


?>

    <div class="page-header parallax clearfix" style="background-color:inherit;background-image:url(<?php echo "../../Photouploads/events/".$eventDetails["image"]."";  ?>);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2><?php echo $eventDetails["eventtitle"]; ?></h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active"><?php echo $eventDetails["eventtitle"]; ?></li>
          	</ol>
        </div>
    </div>


<div class="main" role="main">
    	<div id="content" class="content full">
      		<div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <ul class="sort-destination isotope-grid" data-sort-id="gallery">
                            <?php 
                                if (isset($_GET["move"]) && isset($_GET["moveId"])) {
                                    $move = $_GET["move"];
                                    $lastId = $_GET["moveId"];
                                    
                                } else {
                                    $move = 'none';
                                    $lastId = 0;
                                }
                                
                                $callPhotos = new callEventPhotos($id, $move, $lastId, $db);
                            ?>
                    	
                        </ul>
                    </div>
                    
                      
                    
           		</div>
         	</div>
        </div>
   	</div>
<div class="spacer-50"></div>

<?php require_once ('../inc/footer.php'); ?>