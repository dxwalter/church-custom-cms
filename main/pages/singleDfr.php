<?php require_once('../inc/header.php'); ?>
<?php require_once ('../scripts/dfr/mainDfrFile.php'); ?>
<?php

if (isset($_GET["ijn"])) {
    $dfrId = $_GET["ijn"];
    if ($dfrId) {
        
        $dfrDetails = $callDfrMainFile -> singleDfrDetails($dfrId, $db);
        
    } else {
        header('location: dfr.php');
    }
} else {
    header('location: dfr.php');
}

?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/baptism.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2><?php echo $dfrDetails["title"]; ?></h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->

    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active"><?php echo $dfrDetails["title"]; ?></li>
          	</ol>
        </div>
    </div>


    
   <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
                <div class="row">
                	<div class="col-md-9 col-sm-9">
                        <div class="sermon-series-description">
                    		<h2 style="text-transform:uppercase;">
<!--                                <b style="color:#3bafda">Title:</b> <?php //echo $dfrDetails["title"]; ?>-->
                            </h2>
                            
                            <h3><b style="color:#3bafda">CHARGING TERMINAL:</b> <?php echo $dfrDetails["terminal"]; ?></h3>
                            
                            <blockquote>
                            <p style="font-size:17px;color:#000;font-family: clicker Script;line-height:20px;">
<!--                                <span style="font-size:30px;color:#3bafda">"</span>-->
                                <?php echo $dfrDetails["bibletext"]; ?>
<!--                                <span style="font-size:30px;color:#3bafda">"</span>-->
                            </p>
                            </blockquote>
                            
                        	<div style="font-size:17px">
                                <?php echo $dfrDetails["word"]; ?>
                            </div>
                            
                            
                            <p>
                                <b style="color:#3bafda"><h3>The Blessings</h3></b> 
                                <span style="font-size:16px">
                                    <?php echo $dfrDetails["blessing"]; ?>
                                </span>
                            </p>
                            
                            <p>
                                <b style="color:#3bafda"><h3>Benediction</h3></b> 
                                <span style="font-size:16px">May Jehovah Rehoboth cause you to be fruitful in life in Jesus name. Amen!. Shalom!</span>
                            </p>
                            
                            <?php $eventSliderId = $dfrDetails["id"]; ?>
                            <?php require_once ('../scripts/socialMediaShare.php'); ?>
                            
                            <div class="col-md-6">
                                <p>Subscribe to Daily Faith Recharge in order to receive the word of the lord every morning. We promise we won't spam your inbox!</p>
                                <div class="input-group">
                                <input type="text" class="form-control" placeholder="Email Address" id="dfrEmailValue" autocomplete="off">
                                <span class="input-group-btn">
                                <button id="subscribe" data-sub-type="dfrSubscription" class="btn btn-primary" type="button" data-formValue="dfrEmailValue">Subscribe!</button>
                                </span>
                            </div>
                            <div id="dfrSubscription"></div>
                            </div>
                            
                            <div class="col-md-push-6">
                                <nav aria-label="...">
                                  <ul class="pager">
                                    <?php echo $callDfrMainFile -> previousDRF($eventSliderId, $db); ?>
                                    <?php echo $callDfrMainFile -> nextDRF($eventSliderId, $db); ?>
                                  </ul>
                                </nav>
                            </div>
                            
                        </div>
                    
                    </div>
                </div>
         	</div>
        </div>
   	</div>

    <div class="spacer-50"></div>
    <div class="upcoming-event-bar event-dynamic">
    <div class="container">
        <div class="col-md-4 col-sm-4">
            <p></p>
            <p><a href="aboutDfr.php"  class="btn btn-default btn-lg">Learn About Daily Faith Recharge</a></p>
        </div>
        <div class="col-md-4 col-sm-4">
            <p></p>
            <p><a href="https://give.kcmi-rcc.org" target="_blank" class="btn btn-default btn-lg">Make your Donations</a></p>
        </div>
        <div class="col-md-4 col-sm-4">
            <p></p>
            <p><a href="shareTestimony.php"  class="btn btn-success btn-lg">Share Your Testimony</a></p>
        </div>
    </div>
    </div>
    <!-- End Body Content -->


<input type="text" id="readTimer" value="<?php echo $dfrId ?>" data-read-type="dfr" hidden="true" class="hidden">




<?php require_once('../inc/footer.php'); ?>
<script src="../js/readTimer.js"></script>