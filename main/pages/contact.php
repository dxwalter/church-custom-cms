<?php require_once('../inc/header.php'); ?>

    <div class="page-header parallax clearfix">
    	<div id="contact-map"></div>
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>We would love to hear from you</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.html">Home</a></li>
            	<li class="active">Contact</li>
          	</ol>
        </div>
    </div>
    <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
                <div class="row">
                	<div class="col-md-4 col-sm-5">
                    	<h2>Our Locations</h2>
                    	<hr class="sm">
                    	<h4 class="short accent-color">Rivers State, Nigeria</h4>
                    	<p>
                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria 
                            
                            <br>
                            <a href="tel:+2348084583102">+234 808 458 3102</a>
                            <br>
                            <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>
                        </p>
                        
                        <hr class="fw cont">
                        
                        <h4 class="short accent-color">Umahia, Nigeria</h4>
                    	<p>
<!--                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria -->
<!--
                            
                            <br>
                            <a href="tel:+2348084583102">+234 808 458 3102</a>
                            <br>
-->
<!--                            <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>-->
                        </p>
                        
                        
                        <h4 class="short accent-color">Accra, Ghana</h4>
                    	<p>
<!--                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria -->
<!--
                            
                            <br>
                            <a href="tel:+2348084583102">+234 808 458 3102</a>
                            <br>
-->
<!--                            <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>-->
                        </p>
                       
                        
                        <h4 class="short accent-color">Kasoa, Ghana</h4>
                    	<p>
<!--                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria -->
<!--
                            
                            <br>
                            <a href="tel:+2348084583102">+234 808 458 3102</a>
                            <br>
-->
<!--                            <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>-->
                        </p>
                       
                        
                        <h4 class="short accent-color">Lome, Togo</h4>
                    	<p>
<!--                            Okwuruola Wonodi Close Off Rumuola/Stadium Link Road, By Charlies Fitness Center Port Harcourt, Rivers State, Nigeria -->
<!--
                            
                            <br>
                            <a href="tel:+2348084583102">+234 808 458 3102</a>
                            <br>
-->
<!--                            <a href="mailto:pastor@kcmi-rcc.com">pastor@kcmi-rcc.com</a>-->
                        </p>
                       
                        
                   	</div>
                    <div class="col-md-8 col-sm-7" id="contactDiv">
                        <?php require_once ('../scripts/contactmessage/sender.php') ?>
                       	<form method="post"  action="contact.php">
                        	<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" id="fname" name="names"  class="form-control input-lg" placeholder="First and Last names*">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="email" id="lname" name="contact"  class="form-control input-lg" placeholder="Email address or Phone number*">
                                    </div>
                              	</div>
                           	</div>
                        	<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea cols="6" rows="7" name="message" class="form-control input-lg" placeholder="Type your message"></textarea>
                                    </div>
                                </div>
                          	</div>
                        	<div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-lg btn-block" type="submit" name="submitMessage">Send Message</button>
                                </div>
                          	</div>
                		</form>
                    </div>
               	</div>
           	</div>
        </div>
   	</div>
    <!-- End Body Content -->
<div class="spacer-50"></div>


<?php require_once('../inc/footer.php'); ?>