<?php require_once ('../inc/header.php'); ?>
<?php require_once ('../scripts/events/events.php');?>
<?php require_once ('../scripts/events/memoryListing.php');?>

 <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/slide7.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>Our Stories</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active">Pictures of past events</li>
          	</ol>
        </div>
    </div>
    <!-- End of breadcrumbs -->


<!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Pictures of past events</h2>
                                <hr class="md">
                            </div>
                        </div>

                            <div class="spacer-50"></div>

                        
                            <?php 
                                $lastId = 0;
                                $callEventList = new callEventsList($lastId, $db);
                            ?>

                            
                        
                            <div class="spacer-50"></div>

                    </div>
                </div>
            </div>
    <!-- End Body Content -->


<?php require_once ('../inc/footer.php'); ?>