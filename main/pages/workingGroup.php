<?php require_once ('../inc/header.php'); ?>
<?php

$rand = rand(1, 3);
$image = $rand.".jpg"; 
?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/KCMIPHOTO/groups/<?php echo $image; ?>);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>Working Groups</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active">Working Groups</li>
          	</ol>
        </div>
    </div>

    <!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
               	<h2>Everyone has a role to play in building the temple</h2>
              	<hr class="lg">
                <div class="spacer-30"></div>
               	
                <div class="col-md-6">
    				<h2>Choir and Music Department</h2>
                    <p>Known as the <span style="color:">ARK BEARERS</span>, this department is solely responsible for training, developing and establishing a formidable choir for the Church. They appoint praise and worship conductors during Church worship services. They use all manners of instruments to the glory of God and for the edification of believers. They are permitted to wax records, tapes and organize carols, music concerts and other musical programmes aimed at the salvation of souls and the upliftment of our Lord Jesus Christ. This department keeps, maintains and protects all Church musical equipment. </p>
                  </div>
                
                <div class="col-md-6">
    				<h2>Family and Marriage Enrichment Department</h2>
                    <p>This group is made up of couples with several years of marriage experience. They form a glorious group made-up of different families. Their charge is to organize programmes and activities that will be to the benefit of married couples. To arrange their meetings on non-formal settings like homes, gardens, hotels, etc. They work towards strengthening family altars and to encourage families to rear up godly children. They organize family week, couples night and such other family based programmes. They are also in charge of pre-marital counseling for those who are intending to marry. </p>
                  </div>
                
                <div class="col-md-6">
    				<h2>Ushering Department</h2>
                    <p>This group organizes worshipers in orderly manner and in love. They also ensure that the records of weekly attendance are properly taken and recorded. Generally, they make the worship a stress free period, giving attention to the generality of activities, which include providing buckets for tithes/offerings and arrangement of the congregation for orderly giving of offerings.</p>
                  </div>
                
                <div class="col-md-6">
    				<h2>Outreach and Missions Department</h2>
                    <p>This group organizes external missions /evangelism and creates such awareness through programmes and seminars. They liaise and work with other Christian organizations and individuals in Nigeria and beyond who share similar vision with K.C.M.I. They equally survey potential mission fields, evangelism/outreach stations and crusade grounds to make situational reports on how to plant churches for the purpose of winning souls and following-up such.</p>
                  </div>
                
                <div class="col-md-6">
    				<h2>Care Group</h2>
                    <p>This department caters for the physical and material needs of the members of the church. They do this by identifying the needy amongst the brethren and assisting them as the Lord provides. They equally visit the bereaved, sick, Orphanages, Prisons, Hospitals, Remand Homes, those persecuted for their faith and the elderly, encouraging and strengthening them with the Word coupled with material and financial provisions where necessary. </p>
                  </div>
                
                <div class="col-md-6">
    				<h2>Media, Publication Department</h2>
                    <p>This group is solely responsible for the dissemination of information on church programmes and activities. They ensure that all information published regarding the Church is balanced and correct. They liaise with other Christian media organizations and avail the Church of tapes, books, articles and other publications. They are in charge of organizing radio and T.V. programmes, publishing magazines, tenets, as well as the arrangement of press media coverage for the activities of the Church. </p>
                </div>
                
                
                <div class="col-md-6">
    				<h2>Sound, Technical, and Maintenance Department</h2>
                    <p>This group ensures that ministry properties and equipment such as buildings, lighting, cameras, generators, water systems, sound systems, furniture, vehicles etc are in technically usable condition at all times. </p>
                </div>
                
                
                <div class="col-md-6">
    				<h2>Counseling and Discipleship Department</h2>
                    <p>This group plans systematic discipleship and training programmes, and facilities for members of the ministry. They are also involved in the training of new converts until Christ is formed in them and closely monitor their spiritual growth. And thereafter, recommend them for water baptism and approval for working group involvement. </p>
                </div>
                
                
                <div class="col-md-6">
    				<h2>Resource and Library Department</h2>
                    <p>The library/resource group procures Christian resource materials for sale and distribution to enhance the spiritual growth of members. They provide library services for the benefit of member </p>
                </div>
                
                
                <div class="col-md-6">
    				<h2>Intercessory and Deliverance Department</h2>
                    <p>This department has the responsibility to lead the Church in prayer sessions during services. They organize prayer seminars and conferences, approved by the Senior Pastor. It is within the scope of their calling and department to organize prayer vigils for Church outreaches, individual problems, national and such others as approved by the Church.</p>
                </div>
                
                
                <div class="col-md-6">
    				<h2>Children Department</h2>
                    <p>It is within their jurisdiction to organize the children in the Church and bring them up in the fear of the Lord. They plan and execute children programmes that will help and assist the growth of the children and take special care of the children during church services and provide them with refreshment.</p>
                </div>
                                
                <div class="col-md-6">
                    <h2>Welfare and Altar Nurse Department.</h2>
                    <p>This department caters for the feeding of guest Ministers during visits and programmes. They provide refreshments for Ministers during services. Generally they provide any other service/s relating to the welfare of Ministers as may be assigned. </p>
                </div>
                
                
                <div class="col-md-6">
                    <h2>Protocol and Security Department</h2>
                    <p>This department ensures the safety and security of life and property of Pastors and Guest Ministers of the Ministry. They also handle reception, transportation, accommodation and welfare of Pastors and Guest Ministers to the Ministry.</p>
                </div>
                
                <div class="spacer-50"></div>
                
<!--                <hr class="fw">-->
                <div class="spacer-50"></div>
            </div>
        </div>
   	</div>
    <!-- End Body Content -->


<?php require_once ('../inc/footer.php'); ?>