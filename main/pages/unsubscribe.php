<?php require_once('../inc/header.php'); ?>
<?php require_once('../scripts/unsubscribe.php'); ?>

    <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>Unsubscribe From Our Programs</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active">Email Unsubscription</li>
          	</ol>
        </div>
    </div>

    <?php
        if (isset($_GET["email"])) {
            $email = $_GET["email"];
            if ($email == false) {
                header('location: 404.php');    
            }
        } else {
            header('location: 404.php');
        }
    ?>
    <!-- Start Body Content -->
  	 <div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
            	<div class="row">
                    <div class="col-md-9 col-sm-9">
                        <div class="element-block events-listing">
                            <h2>Hello <?php echo $email; ?></h2>
                            <hr class="sm">
                            
                            <br>
                            <br>
                            
                            <ul class="events-listing-content sort-destination isotope-events isotope" data-sort-id="events" style="position: relative; overflow: hidden; height: 261px;">
                                
                                <?php

                                    $callSub -> subscribeDetails($email, $db);

                                ?>
                                
                            </ul>
                            <!-- unsubscribe -->
                            
                        </div>
                    </div>
              	</div>
           	</div>
        </div>
   	</div>

        <div class="spacer-50"></div>
    <!-- End Body Content -->   
<div class="spacer-50"></div>

<?php require_once('../inc/footer.php'); ?>