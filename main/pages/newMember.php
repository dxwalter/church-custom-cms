<?php require_once ('../inc/header.php'); ?>

 <!-- Start Page Header -->
    <div class="page-header parallax clearfix" style="background-image:url(../images/slide7.jpg);">
        <div class="title-subtitle-holder">
        	<div class="title-subtitle-holder-inner">
    			<h2>I'm New Here</h2>
        	</div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Breadcrumbs -->
    <div class="lgray-bg breadcrumb-cont">
    	<div class="container">
          	<ol class="breadcrumb">
            	<li><a href="index.php">Home</a></li>
            	<li class="active">New Here</li>
          	</ol>
        </div>
    </div>
    <!-- End of breadcrumbs -->


<!-- Start Body Content -->
  	<div class="main" role="main">
    	<div id="content" class="content full">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Get to know about Kingdom Covenant Ministries Int'l</h2>
                                <hr class="md">
                            </div>
                        </div>

                            <div class="spacer-100"></div>

                            <div class="row"> <!-- This row holds links -->

                                <a href="#" >
                                <div class="col-md-4 col-sm-4">
                                    <div class="grid-item staff-item format-standard media-box">
                                        <div class="grid-item-inner" >
<!--                                            <img src="../images/KCMIPHOTO/backdrop.jpg" alt="">-->
                                            
                                            <div class="grid-content">
                                                <p>about kcmi.</p>
                                            </div>
                                            <span class="zoom"><span class="icon"><i class="icon-eye"></i></span></span>
                                        </div>
                                    </div>
                                </div>
                                    
                                </a>


                                <a href="#">
                                <div class="col-md-4 col-sm-4">
                                    <div class="grid-item staff-item format-standard">
                                        <div class="grid-item-inner">
<!--                                            <img src="../images/KCMIPHOTO/backdrop.jpg" alt="">-->
                                            <div class="grid-content">
                                                <p>Working groups</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>


                                <a href="#">
                                <div class="col-md-4 col-sm-4">
                                    <div class="grid-item staff-item format-standard">
                                        <div class="grid-item-inner">
<!--                                            <img src="../images/KCMIPHOTO/backdrop.jpg" alt="">-->
                                            <div class="grid-content">
                                                <p>Home cell</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>

        </div>


                            <div class="row"> <!-- This row holds links -->

                                <a href="#">
                                <div class="col-md-4 col-sm-4">
                                    <div class="grid-item staff-item format-standard">
                                        <div class="grid-item-inner">
                                            <img src="../images/KCMIPHOTO/backdrop.jpg" alt="">
                                            <div class="grid-content">
                                                <p>Sermons.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>


                                <a href="#">
                                <div class="col-md-4 col-sm-4">
                                    <div class="grid-item staff-item format-standard">
                                        <div class="grid-item-inner">
                                            <img src="../images/KCMIPHOTO/backdrop.jpg" alt="">
                                            <div class="grid-content">
                                                <p>Daily faith recharge.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>


                                <a href="#">
                                <div class="col-md-4 col-sm-4">
                                    <div class="grid-item staff-item format-standard">
                                        <div class="grid-item-inner">
                                            <img src="../images/KCMIPHOTO/backdrop.jpg" alt="">
                                            <div class="grid-content">
                                                <p>Events.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>

        </div>

                            <div class="spacer-50"></div>

                            <div class="row">

                                <div class="col-md-12 col-sm-12">
                                    <h3>Common Questions</h3>
                                    <!-- Start Accordion -->
                                    <div class="accordion" id="accordionArea">
                                        <div class="accordion-group panel">
                                            <div class="accordion-heading accordionize"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionArea" href="#oneArea">What are our beliefs?<i class="fa fa-angle-down"></i> </a> </div>
                                            <div id="oneArea" class="accordion-body collapse">
                                                <div class="accordion-inner"> The Bible: We believe that the sixty-six Canonical Books of the Bible were given by Divine Inspiration and that the whole Bible is the WORD OF GOD and that it is TRUE and infallible. Therefore we take the Bible as our final authority in all matters. 
                                                <br>
                                                <br>
                                                Depravity of man: we believe that all men being the offspring of Adam have sinned and fallen short of the glory of God and that redemption is only by faith in the Lord Jesus Christ. 
                                                <br>
                                                <br>
                                                <a href="ourBelieve.php" class="btn btn-default btn-sm">Continue Reading</a></div>
                                            </div>
                                        </div>
                                        <div class="accordion-group panel">
                                            <div class="accordion-heading accordionize"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionArea" href="#twoArea"> Fusce dapibus, tellus ac cursus <i class="fa fa-angle-down"></i> </a> </div>
                                            <div id="twoArea" class="accordion-body collapse">
                                                <div class="accordion-inner"> Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet. Donec sed odio dui.</div>
                                            </div>
                                        </div>
                                        <div class="accordion-group panel">
                                            <div class="accordion-heading accordionize"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionArea" href="#threeArea"> Donec ullamcorper nulla non metus <i class="fa fa-angle-down"></i> </a> </div>
                                            <div id="threeArea" class="accordion-body collapse">
                                                <div class="accordion-inner"> Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet. Donec sed odio dui.</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
        </div>

                    </div>
                </div>
            </div>
    <!-- End Body Content -->


<?php require_once ('../inc/footer.php'); ?>